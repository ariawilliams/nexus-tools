from hue_plus import hue
from port import com_port
from time import sleep
import serial

ser = serial.Serial(com_port, 256000)

# One single fixed color - hue.fixed(ser, gui, channel, color)

# Breathing through a set of colors - hue.breathing(ser, gui, channel, colors, speed)
# hue.breathing(ser, 0, 0, ["FF0000", "FF1493", "FF4400", "FF8800", "BA55D3", "00FF00", "6B8E23", "FFFFFF"], 4)

# Fading through a set of colors - hue.fading(ser, gui, channel, colors, speed)
# hue.fading(ser, 0, 2, ["FF0000", "0000FF"], 4)
# hue.fading(ser, 0, 1, ["0000FF", "FF0000"], 4)

# A strip of color running - hue.marquee(ser, gui, channel, color, speed, size, direction)
# hue.marquee(ser, 0, 0, "FF00AA", 2, 0, 0)
# hue.marquee(ser, 0, 0, "FF0000", 2, 0, 0)

# A strip of color running (multiple colors) - hue.cover_marquee(ser, gui, channel, colors, speed, direction)
# hue.cover_marquee(ser, 0, 2, ["FF0000", "0000FF", "FF0000", "0000FF"], 4, 0)
# hue.cover_marquee(ser, 0, 1, ["0000FF", "FF0000", "0000FF", "FF0000"], 4, 1)

# Pulsing through a set of colors - hue.pulse(ser, gui, channel, color, speed)
# hue.pulse(ser, 0, 2, ["FF0000", "0000FF"], 4)
# hue.pulse(ser, 0, 1, ["0000FF", "FF0000"], 4)

# Two alternating colors - hue.alternating(ser, gui, channel, colors, speed, size, moving, direction)
hue.alternating(ser, 0, 0, ["FF0000", "0000FF"], 4, 3, 1, 0)

# A flickering color - hue.candlelight(ser, gui, channel, color)
# hue.candlelight(ser, 0, 0, "FF00AA")

# power(ser, channel, state)
# hue.power(ser, 0, "off")

# Pulsing through a set of colors - spectrum(ser, channel, speed, direction)
# hue.spectrum(ser, 2, 3, 0)
# hue.spectrum(ser, 1, 3, 1)

# Strips of light meeting in the center - hue.wings(ser, gui, channel, color, speed)
# hue.wings(ser, 0, 0, "FF00AA", 3) # follow
# sleep(5)
# hue.wings(ser, 0, 0, "FF4400", 3) # bits
# sleep(5)
# hue.wings(ser, 0, 0, "FF0000", 3) # raid
# sleep(5)
# hue.wings(ser, 0, 0, "6B8E23", 3) # host
# sleep(5)
# hue.wings(ser, 0, 0, "87CEFA", 3) # sub


