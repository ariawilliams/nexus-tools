from classes.config.NexusBotConfig import NexusBotConfig
from classes.core.NexusBotFunctions import NexusBotFunctions
from classes.core.NexusBotHelper import set_interval
from ssl import wrap_socket
from socket import socket
from threading import Thread
from time import sleep
import re
import sys


nbf = NexusBotFunctions()
bot_nick = "chatrelaybot"
oauth = NexusBotConfig().retrieve_oauth(bot_nick)
super_users = ["auroraexlux"]
accounts = sys.argv[1:]
accounts_list = []
status_list = []
debug_mode = False

for x, account in enumerate(accounts):
    super_users.append(account)
    accounts_list.append(account)
    status_list.append(account)

if len(accounts_list) < 2:
    print("Relay only functions for 2 or more accounts. Please specify at " \
        "least 2 accounts.\r\n")
    print("Accounts selected: %s\r\n" % " ".join(accounts_list))
    sys.exit(0)

if len(accounts_list) > 10:
    print("Relay only supports up to 10 accounts. Please specify less " \
        "accounts.")
    print("Accounts selected: %s\r\n" % " ".join(accounts_list))
    sys.exit(0)

while len(accounts_list) != 4:
    accounts_list.append(None)

s0 = socket()
s0 = wrap_socket(s0)
s1 = socket()
s1 = wrap_socket(s1)
s2 = socket()
s2 = wrap_socket(s2)
s3 = socket()
s3 = wrap_socket(s3)
s4 = socket()
s4 = wrap_socket(s4)
s5 = socket()
s5 = wrap_socket(s5)
s6 = socket()
s6 = wrap_socket(s6)
s7 = socket()
s7 = wrap_socket(s7)
s8 = socket()
s8 = wrap_socket(s8)
s9 = socket()
s9 = wrap_socket(s9)


def account_relay(account, sock):
    global status_list
    status_list = []
    real_account = nbf.get_display_name(account)
    sock.connect(("irc.chat.twitch.tv", 6697))
    sock.send("PASS {}\r\n".format(oauth).encode("utf-8"))
    sock.send("NICK {}\r\n".format(bot_nick).encode("utf-8"))
    sock.send("JOIN #{}\r\n".format(account).encode("utf-8"))

    active_accounts = [ x for x in accounts_list if x is not None ]
    if debug_mode:
        print("Connected to #%s // Relaying between #%s" % (real_account,
            " #".join(active_accounts)))

    chat_msg_prog = re.compile(
        r"^:\w+!\w+@\w+\.tmi\.twitch\.tv PRIVMSG #\w+ :")
    if account in status_list:
        nbf.send_channel(sock, account, "Chat relay initialized! Now " \
            "relaying between #%s!" % " #".join(active_accounts), debug_mode)

    while True:
        response = sock.recv().decode("utf-8")
        if response == "PING :tmi.twitch.tv\r\n":
            sock.send("PONG :tmi.twitch.tv\r\n".encode("utf-8"))
        elif chat_msg_prog.match(response) is not None:
            username = re.search(r"\w+", response).group(0)
            username = username.strip()
            message = chat_msg_prog.sub("", response)
            message = message.strip()

            if debug_mode:
                print("%s~%s" % (account, response))

            message_list = message.strip().split(" ")
            trigger = message_list[0]
            msg = " ".join(message_list[1:])
            values = {
                "user": username,
                "realuser": nbf.get_display_name(username),
                "msg": msg,
                "cmd": trigger
            }

            for i, msg_word in enumerate(message_list[1:]):
                values["msg%d" % i] = msg_word

            if account not in status_list and trigger == "!relayon" and \
                    username in super_users:
                status_list.append(account)
                nbf.send_channel(sock, account, "Chat relay enabled!",
                    debug_mode)
                for i, acct in enumerate(accounts_list):
                    if acct in status_list and acct is not None and \
                            acct != account:
                        content = (real_account, values["realuser"])
                        nbf.send_channel(eval(
                            "s%d" % accounts_list.index(acct)), acct,
                            "Chat relay enabled from #%s by %s!" % content,
                            debug_mode)

            if account in status_list and trigger == "!relayoff" and \
                    username in super_users:
                status_list.remove(account)
                nbf.send_channel(sock, account, "Chat relay disabled!",
                    debug_mode)
                for i, acct in enumerate(accounts_list):
                    if acct in status_list and acct is not None and \
                            acct != account:
                        content = (real_account, values["realuser"])
                        nbf.send_channel(eval(
                            "s%d" % accounts_list.index(acct)), acct,
                            "Chat relay disabled from #%s by %s!" % content,
                            debug_mode)

            if account in status_list and username != bot_nick:
                for i, acct in enumerate(accounts_list):
                    triggers_excluded = [ "!relayon", "!relayoff" ]
                    if acct in status_list and acct is not None and \
                            acct != account and \
                            trigger not in triggers_excluded:
                        content = (real_account, values["realuser"], message)
                        nbf.send_channel(eval(
                            "s%d" % accounts_list.index(acct)), acct,
                            "#%s %s: %s" % content, debug_mode)

        else:
            if debug_mode:
                print("%s~%s" % (account, response))
            pass

        sleep(0.5)


def main():
    try:
        if accounts_list[0] is not None:
            t0 = Thread(target = account_relay, args = (accounts_list[0],
                s0)).start()
    except IndexError:
        pass

    try:
        if accounts_list[1] is not None:
            t1 = Thread(target = account_relay, args = (accounts_list[1],
                s1)).start()
    except IndexError:
        pass

    try:
        if accounts_list[2] is not None:
            t2 = Thread(target = account_relay, args = (accounts_list[2],
                s2)).start()
    except IndexError:
        pass

    try:
        if accounts_list[3] is not None:
            t3 = Thread(target = account_relay, args = (accounts_list[3],
                s3)).start()
    except IndexError:
        pass

    try:
        if accounts_list[4] is not None:
            t4 = Thread(target = account_relay, args = (accounts_list[4],
                s4)).start()
    except IndexError:
        pass

    try:
        if accounts_list[5] is not None:
            t5 = Thread(target = account_relay, args = (accounts_list[5],
                s5)).start()
    except IndexError:
        pass

    try:
        if accounts_list[6] is not None:
            t6 = Thread(target = account_relay, args = (accounts_list[6],
                s6)).start()
    except IndexError:
        pass

    try:
        if accounts_list[7] is not None:
            t7 = Thread(target = account_relay, args = (accounts_list[7],
                s7)).start()
    except IndexError:
        pass

    try:
        if accounts_list[8] is not None:
            t8 = Thread(target = account_relay, args = (accounts_list[8],
                s8)).start()
    except IndexError:
        pass

    try:
        if accounts_list[9] is not None:
            t9 = Thread(target = account_relay, args = (accounts_list[9],
                s9)).start()
    except IndexError:
        pass


if __name__ == "__main__":
    main()
