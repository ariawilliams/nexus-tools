from classes.config.NexusBotConfig import NexusBotConfig
from classes.core.NexusBotFunctions import NexusBotFunctions
from classes.core.NexusBotHelper import set_interval
from classes.core.NexusTwitchPubsub import NexusTwitchPubsub
from random import randint
from socket import socket
from ssl import wrap_socket
from threading import Thread
from time import sleep
from twi.classes.core.NexusCore import NexusCore
from urllib import parse
import json
import re
import requests
import signal
import sys


nbc = NexusBotConfig()
nbf = NexusBotFunctions()
ntp = NexusTwitchPubsub()
ncr = NexusCore()
account = sys.argv[1]
channel = sys.argv[2]
oauth = nbc.retrieve_oauth(account)
s = socket()
s = wrap_socket(s)


class SignalHandler:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


def main():
    sig = SignalHandler()

    s.connect(("irc.chat.twitch.tv", 6697))
    s.send("PASS {}\r\n".format(oauth).encode("utf-8"))
    s.send("NICK {}\r\n".format(account).encode("utf-8"))
    s.send("JOIN #{}\r\n".format(channel).encode("utf-8"))
    s.send("CAP REQ :twitch.tv/tags\r\n".encode("utf-8"))
    s.send("CAP REQ :twitch.tv/commands\r\n".encode("utf-8"))

    while True:
        if sig.kill_now:
            break
        response = s.recv().decode("utf-8")
        if response == "PING :tmi.twitch.tv\r\n":
            s.send("PONG :tmi.twitch.tv\r\n".encode("utf-8"))
        else:
            print(response)

        sleep(0.5)


if __name__ == "__main__":
    main()
