from ..config.NexusBotConfig import NexusBotConfig
from .NexusBotFunctions import NexusBotFunctions
from .NexusBotHelper import set_interval
from classes.core.NexusBotHelper import set_interval
from websocket import create_connection
from threading import Timer
from time import sleep
import json
import string
import random
import sys


nbc = NexusBotConfig()
nbf = NexusBotFunctions()
user = sys.argv[1]
config = nbc.retrieve_config(user)
caster_oauth = nbf.validate_oauth(config["caster_channel"])
bot_oauth = nbf.validate_oauth(config["bot_nick"])


class NexusTwitchPubsub(object):

    @set_interval(60)
    def heartbeat(self, ws, debug_mode):
        msg = { "type": "PING" }
        json_msg = json.dumps(msg)
        if debug_mode:
            print("PUBSUB SEND HEARTBEAT: %s\n" % json_msg)
        ws.send(json_msg)


    @set_interval(3, 1)
    def reconnect(self, debug_mode):
        self.initialize(debug_mode)


    def nonce(self, length):
        final = ""
        possible = string.ascii_uppercase + string.ascii_lowercase + string.digits
        while True:
            if len(final) == length:
                break
            final = final + random.choice(possible)
        return final


    def get_topic_list_caster(self, channel_id):
        return [
            # "whispers.%s" % channel_id,
            # "channel-subscribe-events-v1.%s" % channel_id,
            "channel-bits-events-v1.%s" % channel_id
        ]

    def get_topic_list_bot(self, channel_id):
        return [
            # "channel-bits-events-v1.%s" % channel_id,
            # "channel-subscribe-events-v1.%s" % channel_id,
            "whispers.%s" % channel_id
        ]


    def listen(self, ws, topic_list, oauth, debug_mode):
        nonce = self.nonce(15)
        msg = {
            "type": "LISTEN",
            "nonce": nonce,
            "data": {
                "topics": topic_list,
                "auth_token": oauth
            }
        }
        json_msg = json.dumps(msg)
        if debug_mode:
            print("PUBSUB SEND LISTEN: ID: %s TOPICS: %s\n" % (nonce, " ".join(topic_list)))
        ws.send(json_msg)


    def connect(self, debug_mode):
        ws = create_connection("wss://pubsub-edge.twitch.tv")
        if debug_mode:
            print("PUBSUB INFO: WebSocket Opened\n")
        
        t1 = self.heartbeat(ws, debug_mode)

        return ws


    def initialize(self, debug_mode):
        ws = self.connect(debug_mode)

        self.listen(ws, self.get_topic_list_caster(config["caster_channel_id"]), caster_oauth, debug_mode)
        self.listen(ws, self.get_topic_list_bot(config["bot_nick_id"]), bot_oauth, debug_mode)

        return ws
