import sqlite3
import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__)).replace(os.path.join(
    "classes", "core"), "")

class NexusDatabaseInterface(object):

    def init_connection(self, user):
        return sqlite3.connect(os.path.join(ROOT_DIR,
            "database", "%s_database.db" % user))


    def convert_string_boolean(self, input):
        try:
            if input.upper() == "TRUE":
                return True
            elif input.upper() == "FALSE":
                return False
            else:
                return None
        except AttributeError:
            return None


    def escape_text(self, input):
        return str(input).replace("'", "''")


    def select_table(self, user, table_name):
        try:
            conn = self.init_connection(user)
            conn.row_factory = sqlite3.Row
            cursor = conn.cursor()

            query = "SELECT * FROM %s;" % table_name
            cursor.execute(query)

            result = {
                table_name: [dict(row) for row in cursor.fetchall()]
            }

            conn.close()

            return result
        except:
            return None


    def select_table_row(self, user, table_name, row_id):
        try:
            conn = self.init_connection(user)
            conn.row_factory = sqlite3.Row
            cursor = conn.cursor()

            query = "SELECT * FROM %s WHERE ROWID = %d;" \
                % (table_name, row_id)
            cursor.execute(query)

            result = dict(cursor.fetchone())

            conn.close()

            return result
        except:
            return None


    def select_table_row_index(self, user, table_name, index):
        try:
            conn = self.init_connection(user)
            conn.row_factory = sqlite3.Row
            cursor = conn.cursor()

            check_query = "SELECT name FROM pragma_table_info('%s')" \
                % table_name
            cursor.execute(check_query)

            field_names_list = [row[0] for row in cursor.fetchall()]

            if "index" in field_names_list:
                query = "SELECT * FROM %s WHERE \"index\" = %d;" \
                    % (table_name, index)
                cursor.execute(query)

                result = dict(cursor.fetchone())

                conn.close()

                return result
            else:
                conn.close()
                return None
        except:
            return None


    def rebuild_table_index(self, user, table_name):
        try:
            conn = self.init_connection(user)
            cursor = conn.cursor()

            check_query = "SELECT name FROM pragma_table_info('%s')" \
                % table_name
            cursor.execute(check_query)

            field_names_list = [row[0] for row in cursor.fetchall()]

            if "index" in field_names_list:
                rowids_query = "SELECT ROWID FROM %s ORDER BY ROWID;" \
                    % table_name
                cursor.execute(rowids_query)

                rowids = [row[0] for row in cursor.fetchall()]

                for x, rows in enumerate(rowids):
                    query = "UPDATE %s SET \"index\" = %d WHERE ROWID = %d;" \
                        % (table_name, x + 1, rowids[x])
                    cursor.execute(query)

                conn.commit()
                conn.close()

                return True
            else:
                conn.close()
                return None
        except:
            return False


    def select_field_from_table(self, user, table_name, field_name):
        try:
            conn = self.init_connection(user)
            conn.row_factory = sqlite3.Row
            cursor = conn.cursor()

            check_query = "SELECT name FROM pragma_table_info('%s')" \
                % table_name
            cursor.execute(check_query)

            field_names_list = [row[0] for row in cursor.fetchall()]

            if field_name in field_names_list:
                query = "SELECT \"%s\" FROM %s;" % (field_name, table_name)
                cursor.execute(query)

                result = [row[0] for row in cursor.fetchall()]
            else:
                result = None

            conn.close()

            return result
        except:
            return None


    def resolve_query(self, user, table_name, cond_list):
        try:
            conn = self.init_connection(user)
            conn.row_factory = sqlite3.Row
            cursor = conn.cursor()

            where_clause = "%s %s" % ("WHERE", " AND ".join(cond_list))
            query = "SELECT * FROM %s %s" % (table_name, where_clause)
            cursor.execute(query)

            result = {
                table_name: [dict(row) for row in cursor.fetchall()]
            }

            conn.close()

            if not result[table_name]:
                result = None
            return result
        except:
            return None


    def count_table_records(self, user, table_name):
        try:
            conn = self.init_connection(user)
            cursor = conn.cursor()

            query = "SELECT COUNT(*) FROM %s;" % table_name
            cursor.execute(query)

            result = cursor.fetchone()[0]

            conn.close()

            return result
        except:
            return None


    def select_all_alertmsg(self, user):
        final_result = {}
        try:
            conn = self.init_connection(user)
            cursor = conn.cursor()

            query = "SELECT * FROM alertmsg;"
            cursor.execute(query)

            result = cursor.fetchall()

            conn.close()

            for record in result:
                final_result[record[0]] = record[1]

            return final_result
        except:
            return None


    def select_alertmsg(self, user, alert_id):
        try:
            conn = self.init_connection(user)
            cursor = conn.cursor()

            check_query = "SELECT \"alert_id\" FROM alertmsg;"
            cursor.execute(check_query)

            current_alertmsg_list = [row[0] for row in cursor.fetchall()]

            if alert_id in current_alertmsg_list:
                query = "SELECT \"alert_msg\" FROM alertmsg " \
                    "WHERE alert_id = '%s';" % alert_id
                cursor.execute(query)

                result = cursor.fetchone()[0]

                conn.close()
                return result
            else:
                conn.close()
                return None
        except:
            return None


    def insert_new_command(self, user, cmd_ref, output):
        output = self.escape_text(output)
        cmd_ref = self.escape_text(cmd_ref)
        data_dict = {
            "cmd_id": cmd_ref,
            "response": output,
            "userlevel": "all",
            "type": "text",
            "cooldown": 0,
            "usercooldown": 0,
            "status": "True",
            "cmdlist": "True"
        }
        columns = "\"" + "\", \"".join(data_dict.keys()) + "\""
        placeholders = ":" + ", :".join(data_dict.keys())
        try:
            conn = self.init_connection(user)
            cursor = conn.cursor()

            check_query = "SELECT \"cmd_id\" FROM cmds;"
            cursor.execute(check_query)

            current_cmd_list = [row[0] for row in cursor.fetchall()]

            if cmd_ref in current_cmd_list:
                conn.close()
                return None
            else:
                query = "INSERT INTO cmds (%s) VALUES (%s)" % (columns,
                    placeholders)
                cursor.execute(query, data_dict)
                conn.commit()

                conn.close()
                return True
        except:
            return False


    def update_command(self, user, cmd_ref, table_name, field_name,
            new_value):
        try:
            new_value = self.escape_text(new_value)
            conn = self.init_connection(user)
            cursor = conn.cursor()

            check_query = "SELECT name FROM pragma_table_info('%s')" \
                % table_name
            cursor.execute(check_query)

            field_names_list = [row[0] for row in cursor.fetchall()]

            if field_name in field_names_list:
                query = "UPDATE %s SET \"%s\" = '%s' WHERE \"cmd_id\" = " \
                    "'%s';" % (table_name, field_name, new_value, cmd_ref)
                cursor.execute(query)
                conn.commit()

                conn.close()
                return True
            else:
                conn.close()
                return None
        except:
            return False


    def update_quote(self, user, quote_ref, new_value):
        try:
            new_value = self.escape_text(new_value)
            conn = self.init_connection(user)
            cursor = conn.cursor()

            query = "UPDATE quotes SET \"text\" = '%s' WHERE \"index\" = " \
                "%d;" % (new_value, quote_ref)
            cursor.execute(query)
            conn.commit()

            conn.close()
            return True
        except:
            return False


    def insert_new_quote(self, user, data_dict):
        data_dict["text"] = self.escape_text(data_dict["text"])
        data_dict["game"] = self.escape_text(data_dict["game"])
        data_dict["author"] = self.escape_text(data_dict["author"])
        data_dict["dayweek"] = self.escape_text(data_dict["dayweek"])
        columns = "\"" + "\", \"".join(data_dict.keys()) + "\""
        placeholders = ":" + ", :".join(data_dict.keys())
        try:
            conn = self.init_connection(user)
            cursor = conn.cursor()

            query = "INSERT INTO quotes (%s) VALUES (%s)" % (columns,
                placeholders)
            cursor.execute(query, data_dict)
            conn.commit()

            conn.close()
            return True
        except:
            return False


    def delete_quote(self, user, quote_ref):
        try:
            conn = self.init_connection(user)
            conn.row_factory = sqlite3.Row
            cursor = conn.cursor()

            check_query = "SELECT NULL FROM quotes WHERE \"index\" = %d" \
                % quote_ref
            cursor.execute(check_query)
            row_exists = cursor.fetchone() is not None

            if not row_exists:
                return None

            select_query = "SELECT * FROM quotes WHERE \"index\" = %d" \
                % quote_ref
            cursor.execute(select_query)
            result = dict(cursor.fetchone())

            delete_query = "DELETE FROM quotes WHERE \"index\" = %d" \
                % quote_ref
            cursor.execute(delete_query)
            conn.commit()

            try:
                columns = "\"" + "\", \"".join(result.keys()) + "\""
                placeholders = ":" + ", :".join(result.keys())
                insert_query = "INSERT INTO deleted_quotes (%s) VALUES " \
                    "(%s)" % (columns, placeholders)
                cursor.execute(insert_query, result)
                conn.commit()
            except:
                pass

            conn.close()
            return True
        except:
            return False


    def resolve_currency_balance(self, user, delta):
        pass
