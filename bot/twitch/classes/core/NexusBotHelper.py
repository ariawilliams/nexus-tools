from threading import Thread
from threading import Timer
from threading import Event


def set_interval(interval, times = -1):
    def outer_wrap(function):
        def wrap(*args, **kwargs):
            stop = Event()
            def inner_wrap():
                i = 0
                while i != times and not stop.isSet():
                    stop.wait(interval)
                    function(*args, **kwargs)
                    i += 1
            t = Timer(0, inner_wrap)
            t.daemon = True
            t.start()
            return stop
        return wrap
    return outer_wrap