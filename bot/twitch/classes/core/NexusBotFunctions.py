from .NexusDatabaseInterface import NexusDatabaseInterface
from ..config.NexusBotConfig import NexusBotConfig
from datetime import datetime
from itertools import cycle
from operator import itemgetter
from pytz import timezone
from random import randint
from time import sleep
from twi.classes.core.NexusCore import NexusCore
import calendar
import json
import re
import requests
import sys
import time
import urllib


ncr = NexusCore()
nbc = NexusBotConfig()
ndi = NexusDatabaseInterface()
config = nbc.retrieve_config(sys.argv[1])
msgs = ndi.select_all_alertmsg(sys.argv[1])

op_dict = {}
used = {}
user_used = {}


class NexusBotFunctions(object):

    def __init__(self, tmr_index=-1):
        self.tmr_index = tmr_index


    def validate_oauth(self, channel):
        return ncr.validate_twitch_oauth(channel)


    def send(self, sock, msg, debug_mode, channel = config["caster_channel"],
            delay = True):
        sock.send(
            "PRIVMSG #{} :{}\r\n".format(
                channel,
                msg).encode("utf-8"))
        if debug_mode:
            print(":bot_send: %s\n" % msg)
        if delay:
            sleep(0.5)


    def send_channel(self, sock, channel, msg, debug_mode):
        sock.send(
            "PRIVMSG #{} :{}\r\n".format(
                channel,
                msg).encode("utf-8"))
        if debug_mode:
            print("Sent to #%s: %s\n" % (channel, msg))
        sleep(0.5)


    def submit_user_ban(self, sock, user, debug_mode):
        self.send(sock, "/ban %s" % user, debug_mode)


    def submit_user_timeout(self, sock, user, seconds, debug_mode):
        self.send(sock, "/timeout %s %s" % (user, seconds), debug_mode)


    def build_cmds_dict(self, account):
        cmds = ndi.select_table(account, "cmds")["cmds"]
        builtin_cmds = ndi.select_table(account,
            "builtin_cmds")["builtin_cmds"]
        builtin_data = ndi.select_table("default",
            "builtin_commands")["builtin_commands"]
        combined = {}
        abs_builtin_data = {}
        abs_builtin_cmds = {}

        for data in builtin_data:
            command = data["cmd_id"]
            del data["cmd_id"]
            abs_builtin_data[command] = data

        for data in builtin_cmds:
            command = data["cmd_id"]
            del data["cmd_id"]
            abs_builtin_cmds[command] = data

        for command, data in abs_builtin_data.items():
            combined[command] = {
                "response": abs_builtin_data[command]["response"],
                "type": abs_builtin_data[command]["type"],
                "userlevel": abs_builtin_cmds[command]["userlevel"],
                "cooldown": abs_builtin_cmds[command]["cooldown"],
                "usercooldown": abs_builtin_cmds[command]["usercooldown"],
                "status": ndi.convert_string_boolean(
                    abs_builtin_cmds[command]["status"]),
                "cmdlist": ndi.convert_string_boolean(
                    abs_builtin_cmds[command]["cmdlist"])
            }

        for data in cmds:
            command = data["cmd_id"]
            del data["cmd_id"]
            data["status"] = ndi.convert_string_boolean(data["status"])
            data["cmdlist"] = ndi.convert_string_boolean(data["cmdlist"])
            combined[command] = data

        return {
            "cmds": combined
        }


    def get_cmd_data(self, cmds, command):
        return {
            "response": cmds["cmds"][command]["response"],
            "type": cmds["cmds"][command]["type"],
            "userlevel": cmds["cmds"][command]["userlevel"],
            "cooldown": cmds["cmds"][command]["cooldown"],
            "usercooldown": cmds["cmds"][command]["usercooldown"],
            "status": cmds["cmds"][command]["status"],
            "cmdlist": cmds["cmds"][command]["cmdlist"]
        }


    def process_text_reply(self, s, reply, user, limit_str, debug_mode):
        reply_list = reply.split("\r\n")
        if reply_list is not None:
            for reply_msg in reply_list:
                if len(reply_msg) > 500:
                    self.send(s, "%s > %s" % (user, limit_str), debug_mode)
                else:
                    self.send(s, reply_msg, debug_mode)
                if len(reply_list) > 1:
                    sleep(1)


    def process_url_reply(self, s, reply, user, limit_str, debug_mode):
        url_reply = self.resolve_url(reply)
        if url_reply is not None:
            if len(url_reply) > 500:
                self.send(s, "%s > %s" % (user, limit_str), debug_mode)
            else:
                self.send(s, url_reply, debug_mode)


    def process_def_reply(self, s, reply, user, limit_str, debug_mode):
        def_reply = self.resolve_def(reply)
        if def_reply is not None:
            if len(def_reply) > 500:
                self.send(s, "%s > %s" % (user, limit_str), debug_mode)
            else:
                self.send(s, def_reply, debug_mode)


    def process_response(self, response, values):
        var_list = list(set(values.keys()))
        for variable in var_list:
            if "#[%s]#" % variable in response:
                response = response.replace(
                    "#[%s]#" %
                    variable, values[variable])
        response_list = response.split(" ")
        prog = re.compile(r"^\#\[(.*)\]\#$")
        for word in response_list:
            if prog.match(word) is not None:
                return None
        return response


    def process_alert_response(self, response, values):
        var_list = set(values.keys())
        for variable in var_list:
            if "#[%s]#" % variable in response:
                response = response.replace(
                    "#[%s]#" %
                    variable, values[variable])
        return response


    def process_quote_response(self, response, values, quote_ref):
        var_list = set(values.keys())
        var_list.add("index")
        values["index"] = quote_ref
        for variable in var_list:
            if "#[%s]#" % variable in response:
                response = response.replace(
                    "#[%s]#" %
                    variable, str(values[variable]))
        return response


    def get_display_name(self, user):
        return ncr.get_display_name_string(user)


    def resolve_url(self, url):
        try:
            r = requests.get(
                url,
                headers={
                    "Accept": "*/*",
                    "Content-Type": "text/plain"})
            return r.text
        except:
            pass


    def resolve_def(self, reply):
        try:
            return eval("ncr.%s" % reply)
        except:
            pass


    def cooldown_string(self, command, user, cooldown):
        return "%s > %s is on global cooldown for %s seconds!" % (
            user, command, cooldown)


    def usercooldown_string(self, command, user, usercooldown):
        return "%s > %s is on cooldown for you for %s seconds!" % (
            user, command, usercooldown)


    def check_userlevel(self, userlevel, user, caster, operators):
        if userlevel == "all":
            return True
        elif userlevel == "sub" and ncr.is_user_subscriber(
                caster, user) or userlevel == "mod" and user in \
                list(operators.keys()) and operators[user] == "mod":
            return True
        elif userlevel == "vip" and user in list(operators.keys()) and \
                operators[user] == "vip":
            return True
        elif userlevel == "mod" and user in list(operators.keys()) and \
                operators[user] == "mod":
            return True
        elif userlevel == "caster" and user == caster:
            return True
        else:
            return False


    def process_cooldown(self, command, user, cooldown):
        try:
            cooldown = int(cooldown)
        except:
            return None
        if command not in used or time.time() - used[command] > cooldown:
            used[command] = time.time()
            return None
        else:
            current_cooldown = int(cooldown - (time.time() - used[command]))
            return self.cooldown_string(command, user, current_cooldown)


    def process_usercooldown(self, command, user, usercooldown):
        try:
            usercooldown = int(usercooldown)
            key_string = "%s:%s" % (command, user)
        except:
            return None
        if key_string not in user_used or time.time(
        ) - user_used[key_string] > usercooldown:
            user_used[key_string] = time.time()
            return None
        else:
            current_usercooldown = int(
                usercooldown - (time.time() - user_used[key_string]))
            return self.usercooldown_string(
                command, user, current_usercooldown)


    def process_link_protection(self, socket, config, values, message_list,
            message, username, op_dict, permitted, debug_mode):
        is_link = False
        is_permitted = False
        is_chatrelay = False
        is_valid_userlevel = False
        for userlevel in config["link_protection_immunity"]:
            is_valid_userlevel = self.check_userlevel(userlevel,
                username, values["host"], op_dict)
            if is_valid_userlevel:
                break
        for word in message_list:
            if not is_valid_userlevel:
                if username in permitted:
                    is_permitted = True
                elif username == "chatrelaybot":
                    is_chatrelay = True
                else:
                    sr_prog = re.compile(r"(\!sr|\!songrequest)\s(https?\:" \
                        "\/\/|http?\:\/\/|)(www\.|)(youtube\.com|" \
                        "youtu\.?be|open\.spotify\.com)\/.+")
                    link_prog = re.compile(r".*[^.\s]\.[^.\s]+")
                    if link_prog.match(word) is not None:
                        if sr_prog.match(message) is None:
                            if self.check_link(word):
                                is_link = True
        if is_link and not is_chatrelay:
            self.submit_user_timeout(socket, username, 1, debug_mode)
        if is_permitted:
            permitted.remove(username)


    def process_editquote_command(self, socket, values, message_list,
            username, op_dict, permitted, super_users, trigger, debug_mode):
        if config["quotes_enabled"]:
            if trigger == "!editquote" and self.check_userlevel(
                    config["quote_user_level"], username, values["host"],
                    op_dict):
                try:
                    quote_ref = int(message_list[1])
                    new_text = " ".join(message_list[2:]).strip()
                    update_result = ndi.update_quote(values["host"],
                        quote_ref, new_text)
                    if update_result:
                        self.send(socket, "%s > Quote #%d successfully " \
                            "edited!" % (values["realuser"], quote_ref),
                            debug_mode)
                    else:
                        self.send(socket, "%s > Quote #%d failed to be " \
                            "edited!" % (values["realuser"], quote_ref),
                            debug_mode)
                except IndexError:
                    self.send(socket, "%s > Invalid format or quote " \
                        "doesn't exist, please try again using !editquote " \
                        "<number> <text>" % (values["realuser"]), debug_mode)
                except ValueError:
                    self.send(socket, "%s > Quote reference \"%s\" is " \
                        "invalid , please try again!" %
                        (values["realuser"], message_list[1]), debug_mode)


    def process_newquote_command(self, socket, values, message_list,
            username, op_dict, permitted, super_users, trigger, debug_mode):
        newquote_index = ndi.count_table_records(values["host"], "quotes") + 1
        if config["quotes_enabled"]:
            if trigger == "!newquote" and self.check_userlevel(
                    config["quote_user_level"], username, values["host"],
                    op_dict):
                try:
                    local_now = datetime.now(timezone(
                        config["caster_timezone"]))
                    quote_dict = {
                        "index": newquote_index,
                        "text": " ".join(message_list[1:]).strip(),
                        "game": ncr.get_host_game(values["host"]),
                        "author": values["realuser"],
                        "dayweek": calendar.day_name[local_now.weekday()],
                        "yearint": local_now.year,
                        "monthint": local_now.month,
                        "dayint": local_now.day
                    }
                    insert_result = ndi.insert_new_quote(values["host"],
                        quote_dict)
                    if insert_result:
                        self.send(socket, "%s > Quote #%d successfully " \
                            "added!" % (values["realuser"], newquote_index),
                            debug_mode)
                    else:
                        self.send(socket, "%s > Quote failed to be added, " \
                            "it probably already exists!" %
                            (values["realuser"]), debug_mode)
                except IndexError:
                    self.send(socket, "%s > Invalid format, please try " \
                        "again using !newquote <text>" %
                        (values["realuser"]), debug_mode)


    def process_deletequote_command(self, socket, values, message_list,
            username, op_dict, permitted, super_users, trigger, debug_mode):
        if config["quotes_enabled"]:
            if trigger == "!deletequote" and self.check_userlevel(
                    config["quote_user_level"], username, values["host"],
                    op_dict):
                try:
                    quote_ref = int(message_list[1])
                    if quote_ref < 0:
                        raise IndexError
                    delete_result = ndi.delete_quote(values["host"],
                        quote_ref)
                    ndi.rebuild_table_index(values["host"], "quotes")
                    if delete_result is None:
                        self.send(socket, "%s > Quote reference #%d does " \
                            "not exist, please try again!" %
                            (values["realuser"], quote_ref), debug_mode)
                    elif delete_result:
                        self.send(socket, "%s > Quote #%d successfully " \
                            "deleted!" % (values["realuser"], quote_ref),
                            debug_mode)
                    else:
                        self.send(socket, "%s > Quote #%d failed to be " \
                            "deleted!" % (values["realuser"], quote_ref),
                            debug_mode)
                except IndexError:
                    self.send(socket, "%s > Invalid reference format, " \
                        "please try again using !deletequote <number>" %
                        (values["realuser"]), debug_mode)
                except ValueError:
                    self.send(socket, "%s > Quote reference \"%s\" is " \
                        "invalid, please try again!" %
                        (values["realuser"], message_list[1]), debug_mode)


    def retrieve_quote_response(self, socket, values, quote_ref, quote_msg,
            debug_mode):
        quote_data = ndi.select_table_row_index(values["host"], "quotes",
            quote_ref)
        if quote_data is None:
            return None
        resp = self.process_quote_response(quote_msg, quote_data, quote_ref)
        self.send(socket, "%s > %s" % (values["realuser"], resp), debug_mode)


    def process_quote_command(self, socket, config, values, message_list,
            username, op_dict, permitted, super_users, trigger, debug_mode):
        quote_ceiling = ndi.count_table_records(values["host"], "quotes")
        quote_msg = ndi.select_alertmsg(values["host"], "quote_msg")
        if config["quotes_enabled"]:
            for word in message_list:
                if trigger == "!quote":
                    if quote_ceiling <= 0:
                        self.send(socket, "%s > No quotes exist!" %
                            (values["realuser"]), debug_mode)
                        return False
                    try:
                        quote_ref = int(message_list[1])
                        if quote_ref <= 0 or quote_ref > quote_ceiling:
                            quote_ref = randint(1, quote_ceiling)
                            self.retrieve_quote_response(socket, values,
                                quote_ref, quote_msg, debug_mode)
                            return True
                    except IndexError:
                        quote_ref = randint(1, quote_ceiling)
                        self.retrieve_quote_response(socket, values,
                            quote_ref, quote_msg, debug_mode)
                        return True
                    except ValueError:
                        self.send(socket, "%s > Quote reference must be a " \
                            "number, please try again!" %
                            (values["realuser"]), debug_mode)
                        return True
                    try:
                        if quote_ceiling >= quote_ref:
                            self.retrieve_quote_response(socket, values,
                                quote_ref, quote_msg, debug_mode)
                        return True
                    except IndexError:
                        self.send(socket, "%s > Something went wrong, " \
                            "please try again!" %
                            (values["realuser"]), debug_mode)
                        return False


    def process_permit_command(self, socket, values, message_list, username,
            op_dict, permitted, super_users, trigger, debug_mode):
        if trigger == "!permit" and (self.check_userlevel("mod", username,
                values["host"], op_dict) or username in super_users):
            for permit_user in message_list[1:]:
                permit_user_dname = self.get_display_name(permit_user)
                if permit_user_dname is None:
                    self.send(socket, "%s > \"%s\" is not a valid Twitch " \
                        "username, please try again." %
                        (values["realuser"], permit_user), debug_mode)
                else:
                    permitted.append(permit_user.lower())
                    self.send(socket, "%s > You may post one message " \
                        "containing links because %s permitted you to do " \
                        "so. With great power comes great responsibility, " \
                        "grasshopper!" %
                        (self.get_display_name(permit_user),
                            values["realuser"]), debug_mode)


    def process_revoke_command(self, socket, values, message_list, username,
            op_dict, permitted, super_users, trigger, debug_mode):
        if trigger == "!revoke" and (self.check_userlevel("mod", username,
                values["host"], op_dict) or username in super_users):
            if len(message_list) == 1:
                permitted.clear()
                self.send(socket, "%s > Link permit list cleared!" %
                    values["realuser"], debug_mode)
            else:
                for revoke_user in message_list[1:]:
                    revoke_user_dname = self.get_display_name(revoke_user)
                    if revoke_user_dname is None:
                        self.send(socket, "%s > \"%s\" is not a valid " \
                            "Twitch username, please try again." %
                            (values["realuser"], revoke_user), debug_mode)
                    else:
                        if revoke_user in permitted:
                            permitted.remove(revoke_user)
                            self.send(socket, "%s > %s is no longer " \
                                "permitted to post a link. Thou shalt " \
                                "giveth and thou shalt taketh away!" %
                                (values["realuser"], nbf.get_display_name(
                                revoke_user)), debug_mode)


    def process_edit_command(self, config, account, socket, values,
            message_list, username, cmds, op_dict, super_users, trigger,
            debug_mode):
        all_cmds = self.get_all_cmds(cmds)
        bulitin_cmdlist = self.get_all_builtin_cmds()
        userlevel_list = ["all", "sub", "mod", "caster"]
        end_edit = False
        invalid_val = False
        if trigger == "!edit" and (self.check_userlevel("mod", username,
                values["host"], op_dict) or username in super_users or \
                username in config["editors"]):
            try:
                action = message_list[1]
                cmd_ref = message_list[2]
            except IndexError:
                self.send(socket, "%s > Sincerest apologies, but the data " \
                    "you have provided is invalid! Did you want to try " \
                    "that again, perhaps?" %
                    (values["realuser"]), debug_mode)
                end_edit = True
            action_list = ["new", "disable", "enable", "output", "cool",
                "usercool", "level"]
            if action not in action_list:
                self.send(socket, "%s > Sincerest apologies, but I'm " \
                    "afraid I don't comprehend the action \"%s\". Did " \
                    "you want to try that again, perhaps?" %
                    (values["realuser"], action), debug_mode)
            elif action == "new":
                if cmd_ref in bulitin_cmdlist:
                    self.send(socket, "%s > Does not compute! \"%s\" is a " \
                        "reserved in built command! Please use a different " \
                        "command trigger." % (values["realuser"], cmd_ref),
                        debug_mode)
                elif cmd_ref not in all_cmds:
                    output = " ".join(message_list[3:])
                    insert_result = ndi.insert_new_command(account, cmd_ref,
                        output)
                    if insert_result:
                        self.send(socket, "%s > \"%s\" was added to the " \
                            "database!" % (values["realuser"], cmd_ref),
                            debug_mode)
                    else:
                        self.send(socket, "%s > \"%s\" failed to be added " \
                            "to the database!" % (values["realuser"],
                            cmd_ref), debug_mode)
                else:
                    self.send(socket, "%s > Does not compute! \"%s\" " \
                        "already exists! Please use a different command " \
                        "trigger." % (values["realuser"], cmd_ref),
                        debug_mode)

            elif cmd_ref in bulitin_cmdlist or cmd_ref in all_cmds:
                is_builtin = cmd_ref in bulitin_cmdlist
                try:
                    if action == "disable":
                        if is_builtin:
                            update_result = ndi.update_command(account,
                                cmd_ref, "builtin_cmds", "status", "False")
                        else:
                            update_result = ndi.update_command(account,
                                cmd_ref, "cmds", "status", "False")
                        if update_result:
                            self.send(socket, "%s > %s was disabled!" % (
                                values["realuser"], cmd_ref), debug_mode)
                        else:
                            self.send(socket, "%s > %s failed to be set to " \
                                "disabled!" % (values["realuser"], cmd_ref),
                                debug_mode)
                    elif action == "enable":
                        if is_builtin:
                            update_result = ndi.update_command(account,
                                cmd_ref, "builtin_cmds", "status", "True")
                        else:
                            update_result = ndi.update_command(account,
                                cmd_ref, "cmds", "status", "True")
                        if update_result:
                            self.send(socket, "%s > %s was enabled!" % (
                                values["realuser"], cmd_ref), debug_mode)
                        else:
                            self.send(socket, "%s > %s failed to be set to " \
                                "enabled!" % (values["realuser"], cmd_ref),
                                debug_mode)
                    elif action == "output":
                        if is_builtin:
                            self.send(socket, "%s > Does not compute! You " \
                                "can't change the output of in built command " \
                                "%s, you can only change its status, change " \
                                "user level and modify cooldown and user " \
                                "cooldown." %
                                (values["realuser"], cmd_ref), debug_mode)
                        else:
                            out_val = " ".join(message_list[3:])
                            update_result = ndi.update_command(account,
                                cmd_ref, "cmds", "response", out_val)
                            if update_result:
                                self.send(socket, "%s > %s command output " \
                                    "was successfully updated!" %
                                    (values["realuser"], cmd_ref), debug_mode)
                            else:
                                self.send(socket, "%s > %s command output " \
                                    "failed to update!" % (values["realuser"],
                                    cmd_ref), debug_mode)
                    elif action == "cool":
                        cool_val = message_list[3]
                        try:
                            cool_val = int(round(float(cool_val)))
                            if is_builtin:
                                update_result = ndi.update_command(account,
                                    cmd_ref, "builtin_cmds", "cooldown",
                                    cool_val)
                            else:
                                update_result = ndi.update_command(account,
                                    cmd_ref, "cmds", "cooldown", cool_val)
                            if update_result:
                                self.send(socket, "%s > Cooldown for " \
                                    "\"%s\" was set to %d seconds!" % (
                                    values["realuser"], cmd_ref, cool_val),
                                    debug_mode)
                            else:
                                self.send(socket, "%s > Cooldown for %s " \
                                    "failed to be updated!" % (
                                    values["realuser"], cmd_ref), debug_mode)
                        except ValueError:
                            self.send(socket, "%s > Does not compute! " \
                                "Cooldown of \"%s\" is invalid! It must be " \
                                "in whole seconds!" %
                                (values["realuser"], cool_val), debug_mode)
                    elif action == "usercool":
                        cool_val = message_list[3]
                        try:
                            cool_val = int(round(float(cool_val)))
                            if is_builtin:
                                update_result = ndi.update_command(account,
                                    cmd_ref, "builtin_cmds", "usercooldown",
                                    cool_val)
                            else:
                                update_result = ndi.update_command(account,
                                    cmd_ref, "cmds", "usercooldown", cool_val)
                            if update_result:
                                self.send(socket, "%s > User cooldown for " \
                                    "\"%s\" was set to %d seconds!" % (
                                    values["realuser"], cmd_ref, cool_val),
                                    debug_mode)
                            else:
                                self.send(socket, "%s > User cooldown for " \
                                    "%s failed to be updated!" % (
                                    values["realuser"], cmd_ref), debug_mode)
                        except ValueError:
                            self.send(socket, "%s > Does not compute! " \
                                "User cooldown of \"%s\" is invalid! It " \
                                "must be in whole seconds!" % (
                                    values["realuser"], cool_val), debug_mode)
                    elif action == "level":
                        level_val = message_list[3]
                        if level_val in userlevel_list:
                            new_cmds = nbc.retrieve_commands(account)
                            if is_builtin:
                                update_result = ndi.update_command(account,
                                    cmd_ref, "builtin_cmds", "userlevel",
                                    level_val)
                            else:
                                update_result = ndi.update_command(account,
                                    cmd_ref, "cmds", "userlevel",
                                    level_val)
                            if update_result:
                                self.send(socket, "%s > User level for %s " \
                                    "was set to %s!" % (values["realuser"],
                                    cmd_ref, level_val), debug_mode)
                            else:
                                self.send(socket, "%s > User level for " \
                                    "%s failed to be updated!" % (
                                    values["realuser"], cmd_ref), debug_mode)
                        else:
                            self.send(socket, "%s > User level of \"%s\" " \
                                "is invalid, accepted values are %s!" %
                                (values["realuser"], level_val, ("%s and %s" %
                                    (", ".join(userlevel_list[:-1]),
                                    userlevel_list[-1]))), debug_mode)
                except IndexError:
                    self.send(socket, "%s > Does not compute! Something " \
                        "went wrong, please try again." %
                        values["realuser"], debug_mode)
            else:
                if cmd_ref is not None:
                    self.send(socket, "%s > Terribly sorry, but it " \
                        "appears that \"%s\" ceases to exist! "" You can " \
                        "spawn it however, if you so desire..." % (
                        values["realuser"], cmd_ref), debug_mode)


    def validate_cmd(self, values, username, cmd_data, op_dict, trigger,
            debug_mode):
        if self.check_userlevel(cmd_data["userlevel"], username,
                values["host"], op_dict):
            cooldown = self.process_cooldown(trigger, values["realuser"],
                cmd_data["cooldown"])
            usercooldown = self.process_usercooldown(trigger,
                values["realuser"], cmd_data["usercooldown"])
            if self.is_mod(username, op_dict):
                return True
            elif usercooldown is not None:
                if cooldown_status:
                    self.send(s, usercooldown, debug_mode)
                return False
            elif cooldown is not None:
                if cooldown_status:
                    self.send(s, cooldown, debug_mode)
                return False
            else:
                return True
        return False


    def get_all_builtin_cmds(self):
        return ndi.select_field_from_table("default", "builtin_commands",
            "cmd_id")


    def get_active_cmds(self, cmds):
        return [cmd for cmd in set(cmds["cmds"].keys()) if cmds[
            "cmds"][cmd]["status"]]


    def get_all_cmds(self, cmds):
        return [cmd for cmd in set(cmds["cmds"].keys())]


    def build_cmdlist(self, cmds):
        cmd_list = [cmd for cmd in self.get_active_cmds(
            cmds) if cmds["cmds"][cmd]["cmdlist"]]
        cmd_list.sort()
        return " ".join(cmd_list)


    def get_active_tmrs(self, tmrs):
        return [i for i, tmr in enumerate(tmrs["tmrs"]) \
            if ndi.convert_string_boolean(tmrs["tmrs"] \
            [i]["status"])]


    def sort_tmrs(self, tmrs):
        tmr_list = self.get_active_tmrs(tmrs)
        final_tmrs = [(tmrs["tmrs"][i]["index"], tmrs["tmrs"]
                       [i]["response"]) for i in tmr_list]
        final_tmrs.sort(key = itemgetter(0))
        return [x[1] for x in final_tmrs]


    def get_tmr_response(self, tmrs, i):
        sorted_tmrs = self.sort_tmrs(tmrs)
        try:
            return sorted_tmrs[i]
        except IndexError:
            self.tmr_index = 0
            return sorted_tmrs[0]


    def run_tmr(self, tmrs):
        self.tmr_index = self.tmr_index + 1
        return self.get_tmr_response(tmrs, self.tmr_index)


    def check_link(self, link):
        output = False
        try:
            if link.startswith("http://") or link.startswith("https://"):
                return requests.head(link).status_code != 404
            else:
                return requests.head("http://%s" % link).status_code != 404
        except requests.ConnectionError:
            return False
        except UnicodeError:
            return False
        return False


    def is_host_alert(self, msg):
        av_prog = re.compile(r"(\w+) is now auto hosting.*?(\d+\,\d+|\d+).*")
        a_prog = re.compile(r"(\w+) is now auto hosting you\..*")
        hv_prog = re.compile(r"(\w+) is now hosting.*?(\d+\,\d+|\d+).*")
        h_prog = re.compile(r"(\w+) is now hosting you\..*")

        av = av_prog.match(msg)
        hv = hv_prog.match(msg)
        a = a_prog.match(msg)
        h = h_prog.match(msg)

        if av is not None:
            return True
        elif hv is not None:
            return True
        elif a is not None:
            return True
        elif h is not None:
            return True
        else:
            return False


    def is_bits_alert(self, meta):
        values = {}
        try:
            if "bits" in meta.keys():
                return True
            else:
                return False    
        except:
            return False


    def is_raid_alert(self, meta):
        values = {}
        try:
            if meta["msg-id"] == "raid":
                return True
            else:
                return False    
        except:
            return False


    def is_sub_alert(self, meta):
        values = {}
        try:
            if meta["msg-id"] == "sub":
                return True
            elif meta["msg-id"] == "resub":
                return True
            elif meta["msg-id"] == "subgift":
                return True
            else:
                return False
        except:
            return False


    def select_follower_alert(self, follower, config):
        values = {}
        try:
            if config["follow_alert"]:
                values["follow_name"] = follower.lower()
                values["follow_realname"] = follower
                return self.process_alert_response(
                    msgs["follow_msg"], values)
            else:
                return None    
        except KeyError:
            return None


    def select_sub_alert(self, meta, config):
        values = {}
        try:
            values["sub_name"] = meta["login"]
            values["sub_realname"] = self.get_display_name(meta["login"])
            values["sub_message"] = meta["message"]
            sub_tier = { "Prime": "Twitch Prime", "1000": "$4.99",
                "2000": "$9.99", "3000": "$24.99" }

            if meta["msg-id"] == "sub" and config["new_sub_alert"]:
                values["sub_tier"] = sub_tier[meta["msg-param-sub-plan"]]
                values["sub_tier_name"] = meta["msg-param-sub-plan-name"]
                return self.process_alert_response(msgs["new_sub_msg"],
                    values)
            elif meta["msg-id"] == "resub" and config["resub_alert"]:
                values["sub_tenure"] = meta["msg-param-months"] 
                values["sub_tier"] = sub_tier[meta["msg-param-sub-plan"]]
                values["sub_tier_name"] = meta["msg-param-sub-plan-name"]
                return self.process_alert_response(msgs["resub_msg"],
                    values)
            elif meta["msg-id"] == "subgift" and config["gift_sub_alert"]:
                values["sub_name"] = meta["msg-param-recipient-user-name"]
                values["sub_realname"] = self.get_display_name(
                    meta["msg-param-recipient-user-name"])
                values["gifter_name"] = meta["login"]
                values["gifter_realname"] = self.get_display_name(
                    meta["login"])
                values["sub_tier"] = sub_tier[meta["msg-param-sub-plan"]]
                values["sub_tier_name"] = meta["msg-param-sub-plan-name"]
                return self.process_alert_response(msgs["gift_sub_msg"],
                    values)
            else:
                return None
        except KeyError:
            return None
        

    def select_raid_alert(self, meta, config):
        values = {}
        try:
            if meta["msg-id"] == "raid" and config["raid_alert"]:
                values["raid_name"] = meta["msg-param-login"]
                values["raid_game"] = ncr.get_host_game(values["raid_name"])
                values["raid_realname"] = self.get_display_name(
                    meta["msg-param-login"])
                values["raid_viewers"] = meta["msg-param-viewerCount"]
                return self.process_alert_response(msgs["raid_msg"], values)
            else:
                return None    
        except KeyError:
            return None


    def select_bits_alert(self, meta, config):
        values = {}
        try:
            if "bits" in meta.keys() and config["bits_alert"]:
                values["bits_message"] = meta["message"]
                values["total_bits"] = meta["bits"]
                values["cheer_name"] = meta["login"]
                values["cheer_realname"] = self.get_display_name(
                    meta["login"])
                return self.process_alert_response(msgs["bits_msg"], values)
            else:
                return None    
        except KeyError:
            return None


    def parse_host_alert(self, msg):
        av_prog = re.compile(r"(\w+) is now auto hosting.*?(\d+\,\d+|\d+).*")
        a_prog = re.compile(r"(\w+) is now auto hosting you\..*")
        hv_prog = re.compile(r"(\w+) is now hosting.*?(\d+\,\d+|\d+).*")
        h_prog = re.compile(r"(\w+) is now hosting you\..*")

        av = av_prog.match(msg)
        hv = hv_prog.match(msg)
        a = a_prog.match(msg)
        h = h_prog.match(msg)

        if av is not None:
            host_realname = av.group(1)
            viewers = av.group(2)
            auto_host = True
        elif hv is not None:
            host_realname = hv.group(1)
            viewers = hv.group(2)
            auto_host = False
        elif a is not None:
            host_realname = a.group(1)
            viewers = None
            auto_host = True
        elif h is not None:
            host_realname = h.group(1)
            viewers = None
            auto_host = False
        else:
            return None
        
        return {
            "host_realname": host_realname,
            "host_name": host_realname.lower(),
            "viewers": viewers, 
            "auto_host": auto_host
        }


    def select_host_message(self, msg, config):
        host_data = self.parse_host_alert(msg)
        if host_data is None:
            return None
        host_game = ncr.get_host_game(host_data["host_name"])
        if config["auto_host_alert"] and host_data["auto_host"]:
            if host_game is not None:
                alert_msg = msgs["auto_host_msg_game"]
            else:
                alert_msg = msgs["auto_host_msg"]
        elif config["host_alert"] and not host_data["auto_host"]:
            if host_game is not None:
                alert_msg = msgs["host_msg_game"]
            else:
                alert_msg = msgs["host_msg"]
        values = {
            "host_realname": host_data["host_realname"],
            "host_name": host_data["host_name"],
            "viewers": host_data["viewers"],
            "host_game": host_game
        }
        return self.process_alert_response(alert_msg, values)


    def is_op(self, user, op_dict):
        return user in op_dict


    def is_mod(self, user, op_dict):
        return user in op_dict and op_dict[user] == "mod"


    def is_global_mod(self, user, op_dict):
        return user in op_dict and op_dict[user] == "global_mod"


    def is_admin(self, user, op_dict):
        return user in op_dict and op_dict[user] == "admin"


    def is_staff(self, user, op_dict):
        return user in op_dict and op_dict[user] == "staff"


    def process_nye_message(self, current_utc):
        nye_times_utc = [
            (
                "New Zealand",
                {"month": 12, "day": 31, "hour": 11, "minute": 0}
            ),
            (
                "NSW / VIC / ACT Australia",
                {"month": 12, "day": 31, "hour": 13, "minute": 0}
            ),
            (
                "SA Australia",
                {"month": 12, "day": 31, "hour": 13, "minute": 30}
            ),
            (
                "QLD Australia",
                {"month": 12, "day": 31, "hour": 14, "minute": 0}
            ),
            (
                "NT Australia",
                {"month": 12, "day": 31, "hour": 14, "minute": 30}
            ),
            (
                "Japan and South Korea",
                {"month": 12, "day": 31, "hour": 15, "minute": 0}
            ),
            (
                "China, Philippines, Hong Kong, Singapore and WA Australia",
                {"month": 12, "day": 31, "hour": 16, "minute": 00}
            ),
            (
                "Indonesia, Thailand, Vietnam and Cambodia",
                {"month": 12, "day": 31, "hour": 17, "minute": 00}
            ),
            (
                "Myanmar and Cocos Islands",
                {"month": 12, "day": 31, "hour": 17, "minute": 30}
            ),
            (
                "Bangladesh, Kazakhstan, Kyrgyzstan and Bhutan",
                {"month": 12, "day": 31, "hour": 18, "minute": 0}
            ),
            (
                "Nepal",
                {"month": 12, "day": 31, "hour": 18, "minute": 15}
            ),
            (
                "India and Sri Lanka",
                {"month": 12, "day": 31, "hour": 18, "minute": 30}
            ),
            (
                "Pakistan and Uzbekistan",
                {"month": 12, "day": 31, "hour": 19, "minute": 0}
            ),
            (
                "Afghanistan",
                {"month": 12, "day": 31, "hour": 19, "minute": 30}
            ),
            (
                "Azerbaijan, UAE, Oman and Mauritius",
                {"month": 12, "day": 31, "hour": 20, "minute": 0}
            ),
            (
                "Iran",
                {"month": 12, "day": 31, "hour": 20, "minute": 30}
            ),
            (
                "Russia, Turkey, Iraq and Kenya",
                {"month": 12, "day": 31, "hour": 21, "minute": 0}
            ),
            (
                "Egypt, Eastern Europe and South Africa",
                {"month": 12, "day": 31, "hour": 22, "minute": 0}
            ),
            (
                "Central Europe and Algiers",
                {"month": 12, "day": 31, "hour": 23, "minute": 0}
            ),
            (
                "United Kingdom, Ireland, Portugal, Ghana and Iceland",
                {"month": 1, "day": 1, "hour": 0, "minute": 0}
            ),
            (
                "Cape Verde, Ponta Delgada and Greenland",
                {"month": 1, "day": 1, "hour": 1, "minute": 0}
            ),
            (
                "Brazil",
                {"month": 1, "day": 1, "hour": 2, "minute": 0}
            ),
            (
                "Argentina, Chile and Paraguay",
                {"month": 1, "day": 1, "hour": 3, "minute": 0}
            ),
            (
                "Newfoundland and Labrador Canada",
                {"month": 1, "day": 1, "hour": 3, "minute": 0}
            ),
            (
                "Venezuela, Bolivia, Puerto Rico and the Dominican Republic",
                {"month": 1, "day": 1, "hour": 4, "minute": 0}
            ),
            (
                "Eastern Time USA and Cuba",
                {"month": 1, "day": 1, "hour": 5, "minute": 0}
            ),
            (
                "Central Time USA, Mexico and Guatemala",
                {"month": 1, "day": 1, "hour": 6, "minute": 0}
            ),
            (
                "Mountain Time USA",
                {"month": 1, "day": 1, "hour": 7, "minute": 0}
            ),
            (
                "Western Time USA",
                {"month": 1, "day": 1, "hour": 8, "minute": 0}
            ),
            (
                "Alaska USA and French Polynesia",
                {"month": 1, "day": 1, "hour": 9, "minute": 0}
            ),
            (
                "Hawaii and the Cook Islands",
                {"month": 1, "day": 1, "hour": 10, "minute": 0}
            )
        ]

        for nye_time in nye_times_utc:
            if nye_time[1]["month"] == current_utc.month \
                    and nye_time[1]["day"] == current_utc.day \
                    and nye_time[1]["hour"] == current_utc.hour \
                    and nye_time[1]["minute"] == current_utc.minute:
                return "Happy New Year to %s!" % nye_time[0]
        return False
