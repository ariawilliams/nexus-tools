import json
import os


class NexusBotConfig(object):

    def retrieve_config(self, user):
        try:
            file = os.path.join(os.path.dirname(__file__), "main", "%s_config.json" % user)
            with open(file) as json_data:
                return json.load(json_data)
        except:
            return None


    def retrieve_oauth(self, user):
        try:
            file = os.path.join(os.path.dirname(__file__), "oauth", "%s_oauth.json" % user)
            with open(file) as json_data:
                return json.load(json_data)
        except:
            return None


    def retrieve_oauth(self, user):
        try:
            file = os.path.join(os.path.dirname(__file__), "oauth", "%s_oauth.txt" % user)
            with open(file, "r") as f:
                return f.read().strip()
        except:
            return None


    def retrieve_commands(self, user):
        try:
            file = os.path.join(os.path.dirname(__file__), "commands", "%s_commands.json" % user)
            with open(file) as json_data:
                return json.load(json_data)
        except:
            return None


    def put_commands(self, user, data):
        try:
            file = os.path.join(os.path.dirname(__file__), "commands", "%s_commands.json" % user)
            with open(file, "w+") as f:
                f.write(json.dumps(data, indent = 4, sort_keys = False))
        except:
            return None


    def retrieve_quotes(self, user):
        try:
            file = os.path.join(os.path.dirname(__file__), "commands", "%s_quotes.json" % user)
            with open(file) as json_data:
                return json.load(json_data)
        except:
            return None


    def put_quotes(self, user, data):
        try:
            file = os.path.join(os.path.dirname(__file__), "commands", "%s_quotes.json" % user)
            with open(file, "w+") as f:
                f.write(json.dumps(data, indent = 4, sort_keys = False))
        except:
            return None


    def retrieve_timers(self, user):
        try:
            file = os.path.join(os.path.dirname(__file__), "timers", "%s_timers.json" % user)
            with open(file) as json_data:
                return json.load(json_data)
        except:
            return None


    def append_text_data(self, data, file):
        try:
            with open(os.path.join(os.path.dirname(__file__), "logs", file), "a+") as f:
                f.write(str(data))
            return "Success"
        except:
            return None


    def put_text_data(self, data, file):
        try:
            with open(os.path.join(os.path.dirname(__file__), "text", file), "w+") as f:
                f.write(str(data))
            return "Success"
        except:
            return None


    def get_text_data(self, file):
        try:
            with open(os.path.join(os.path.dirname(__file__), "text", file), "r") as f:
                return f.read().strip()
        except:
            return None