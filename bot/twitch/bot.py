from classes.config.NexusBotConfig import NexusBotConfig
from classes.core.NexusDatabaseInterface import NexusDatabaseInterface
from classes.core.NexusBotFunctions import NexusBotFunctions
from classes.core.NexusBotHelper import set_interval
from classes.core.NexusTwitchPubsub import NexusTwitchPubsub
from datetime import datetime
from random import randint
from socket import socket
from ssl import wrap_socket
from threading import Thread
from time import sleep
from twi.classes.core.NexusCore import NexusCore
from urllib import parse
import json
import re
import requests
import signal
import sys


nbc = NexusBotConfig()
nbf = NexusBotFunctions()
ndi = NexusDatabaseInterface()
ntp = NexusTwitchPubsub()
ncr = NexusCore()
account = sys.argv[1]
start_exit_msg = False
op_dict = {}
chatters = []
permitted = []
current_uptime_hours = None
nye_msgs_elapsed = []

kitti_ash_in_chat = False
kitti_ash_msg_sent = False

kitti_alice_in_chat = False
kitti_alice_msg_sent = False

all_kitties_msg_sent = False

kitti_ash_msg = "Issa kitti Ashley! Aria loves her soooo much! bleedPurple"
kitti_alice_msg = "Issa kitti Alice! Aria loves her soooo much! bleedPurple"
all_kitties_msg = "GivePLZ All Aria's kitties are here! TakeNRG"

config = nbc.retrieve_config(account)
followers = ncr.get_follower_list(config["caster_channel"])
oauth = nbf.validate_oauth(config["bot_nick"])
caster_oauth = nbf.validate_oauth(config["caster_channel"])
limit_str = "Response exceeds chat message character limit."
super_users = [config["caster_channel"], "ariialux"]
debug_mode = False
s = socket()
s = wrap_socket(s)
ss = socket()
ss = wrap_socket(ss)


class SignalHandler:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


def connect_bot_socket():
    try:
        s.send("Testing socket connection...".encode("utf-8"))
    except:
        s.connect(("irc.chat.twitch.tv", 6697))
        s.send("PASS oauth:{}\r\n".format(oauth).encode("utf-8"))
        s.send("NICK {}\r\n".format(config["bot_nick"]).encode("utf-8"))
        s.send("JOIN #{}\r\n".format(config["caster_channel"]).encode(
            "utf-8"))
        s.send("CAP REQ :twitch.tv/tags\r\n".encode("utf-8"))
        s.send("CAP REQ :twitch.tv/commands\r\n".encode("utf-8"))


def connect_caster_socket():
    try:
        ss.send("Testing socket connection...".encode("utf-8"))
    except:
        ss.connect(("irc.chat.twitch.tv", 6697))
        ss.send("PASS oauth:{}\r\n".format(caster_oauth).encode("utf-8"))
        ss.send("NICK {}\r\n".format(config["caster_channel"]).encode(
            "utf-8"))
        ss.send("JOIN #{}\r\n".format(config["caster_channel"]).encode(
            "utf-8"))
        ss.send("CAP REQ :twitch.tv/tags\r\n".encode("utf-8"))
        ss.send("CAP REQ :twitch.tv/commands\r\n".encode("utf-8"))


def get_uptime_hours():
    stream_uptime = ncr.get_host_uptime(config["caster_channel"])
    if stream_uptime is not None:
        return int(stream_uptime.total_seconds()) // 3600


@set_interval(5)
def get_ops():
    try:
        url = "http://tmi.twitch.tv/group/user/%s/chatters" % config[
            "caster_channel"]
        r = requests.get(url, headers = {"Accept": "*/*"})
        response = r.text
        if r.status_code == 200:
            op_dict.clear()
            chatters.clear()
            data = json.loads(response)
            op_dict[config["caster_channel"]] = "mod"
                # Caster is added as a mod becuase when user level is mod
                # they are now not included as a mod
            for chat_user in data["chatters"]["moderators"]:
                chatters.append(chat_user)
                op_dict[chat_user] = "mod"
            for chat_user in data["chatters"]["global_mods"]:
                chatters.append(chat_user)
                op_dict[chat_user] = "global_mod"
            for chat_user in data["chatters"]["admins"]:
                chatters.append(chat_user)
                op_dict[chat_user] = "admin"
            for chat_user in data["chatters"]["staff"]:
                chatters.append(chat_user)
                op_dict[chat_user] = "staff"
            for chat_user in data["chatters"]["vips"]:
                chatters.append(chat_user)
                op_dict[chat_user] = "vip"
            for chat_user in data["chatters"]["viewers"]:
                chatters.append(chat_user)
    except:
        pass


@set_interval(10)
def check_follower_list():
    global followers
    new_followers = ncr.get_follower_list(config["caster_channel"])
    if len(new_followers) > 10:
        recent_follows = new_followers[:9]
    else:
        recent_follows = new_followers
    for follower in recent_follows:
        if follower not in followers:
            follower_output = nbf.select_follower_alert(follower, config)
            
            if follower_output is not None:
                nbf.send(s, follower_output, debug_mode)

            followers = new_followers

    if new_followers != followers:
        followers = new_followers


@set_interval(+config["timer_intv"])
def timers():
    tmrs = ndi.select_table(account, "tmrs")
    stream_live = ncr.is_host_live(config["caster_channel"])

    if tmrs is not None and stream_live:
        nbf.send(s, nbf.run_tmr(tmrs), debug_mode)


@set_interval(30)
def hydration_alert():
    global current_uptime_hours
    stream_uptime = ncr.get_host_uptime(config["caster_channel"])

    if stream_uptime is not None:
        new_uptime_hours = int(stream_uptime.total_seconds()) // 3600

        if current_uptime_hours != new_uptime_hours \
                and config["hydration_alert"]:
            if new_uptime_hours != 0:
                fluid_ml = new_uptime_hours * 125
                if fluid_ml >= 1000:
                    fluid_str = "%sL" % round(fluid_ml / 1000, 2)
                else:
                    fluid_str = "%smL" % fluid_ml
                display_name = nbf.get_display_name(
                    config["caster_channel"])
                alert_data = (display_name, new_uptime_hours, fluid_str)
                nbf.send(s, "%s > You've been live for just over %d " \
                    "hours. To stay hydrated, ensure you've had %s of " \
                    "water." % alert_data, debug_mode)

        current_uptime_hours = new_uptime_hours
    else:
        current_uptime_hours = None


@set_interval(30)
def nye_timer():
    global nye_msgs_elapsed
    nye_msg = nbf.process_nye_message(datetime.utcnow())
    if nye_msg != False and nye_msg not in nye_msgs_elapsed:
        nye_msgs_elapsed.append(nye_msg)
        nbf.send(s, nye_msg, debug_mode)


@set_interval(5)
def check_for_cuties():
    global kitti_ash_msg_sent
    global kitti_alice_msg_sent
    global all_kitties_msg_sent

    if kitti_ash_in_chat and not kitti_ash_msg_sent:
        nbf.send(s, kitti_ash_msg, debug_mode)
        kitti_ash_msg_sent = True
        sleep(2)

    if kitti_alice_in_chat and not kitti_alice_msg_sent:
        nbf.send(s, kitti_alice_msg, debug_mode)
        kitti_alice_msg_sent = True
        sleep(2)

    if kitti_ash_in_chat and kitti_alice_in_chat and not all_kitties_msg_sent:
        nbf.send(s, all_kitties_msg, debug_mode)
        all_kitties_msg_sent = True
        sleep(2)


def caster_main():
    connect_caster_socket()

    privmsg_prog = re.compile(r"^(\@.*) :(\w+)!\w+@\w+\.tmi\.twitch\.tv " \
        "PRIVMSG #\w+ :(.*)")
    usernotice_prog = re.compile(r"^(\@.*) :tmi\.twitch\.tv USERNOTICE #\w+" \
        "(.*)")
    host_msg_prog = re.compile(r"^:jtv!jtv@jtv\.tmi\.twitch\.tv PRIVMSG " \
        "\w+ :")

    while True:
        response_two = ss.recv().decode("utf-8")
        if response_two == "PING :tmi.twitch.tv\r\n":
            ss.send("PONG :tmi.twitch.tv\r\n".encode("utf-8"))
        pv = privmsg_prog.match(response_two)
        if pv is not None:
            privmsg_meta_dict = {}
            privmsg_meta_raw = pv.group(1).strip()
            privmsg_meta = privmsg_meta_raw.split(";")
            for var in privmsg_meta:
                var_pair = var.split("=")
                privmsg_meta_dict[var_pair[0]] = var_pair[1]
            privmsg_meta_dict["login"] = pv.group(2).strip()
            privmsg_meta_dict["message"] = pv.group(3).strip()

            bits_output = nbf.select_bits_alert(privmsg_meta_dict, config)

            if bits_output is not None:
                nbf.send(s, bits_output, debug_mode)

        un = usernotice_prog.match(response_two)
        if un is not None:
            usernotice_meta_dict = {}
            usernotice_meta_raw = un.group(1).strip()
            usernotice_meta = usernotice_meta_raw.split(";")
            for var in usernotice_meta:
                var_pair = var.split("=")
                usernotice_meta_dict[var_pair[0]] = var_pair[1]
            if un.group(2) is not None:
                usernotice_meta_dict["message"] = un.group(2).strip()[1:]
            else:
                usernotice_meta_dict["message"] = ""

            sub_output = nbf.select_sub_alert(usernotice_meta_dict, config)
            raid_output = nbf.select_raid_alert(usernotice_meta_dict, config)

            if sub_output is not None:
                nbf.send(s, sub_output, debug_mode)
            elif raid_output is not None:
                nbf.send(s, raid_output, debug_mode)

        if host_msg_prog.match(response_two) is not None:
            host_msg = host_msg_prog.sub("", response_two)
            host_msg = host_msg.strip()
            host_output = nbf.select_host_message(host_msg, config)
            if host_output is not None:
                nbf.send(s, host_output, debug_mode)

        sleep(1)


def timeout_user(user):
    nbf.send(s, "/timeout %s 1" % user, debug_mode)


def pubsub_main():
    ws = ntp.initialize(debug_mode)
    while True:
        res = ws.recv()
        res_dict = json.loads(res)

        if res_dict["type"] == "MESSAGE":
            res_dict["data"]["message"] = json.loads(
                res_dict["data"]["message"])
            res_dict["data"]["message"]["data"] = json.loads(
                res_dict["data"]["message"]["data"])
            if "whispers" in res_dict["data"]["topic"]:
                if debug_mode:
                    print("PUBSUB WHISPER: %s\n" %
                      res_dict["data"]["message"]["data"]["body"])
            else:
                if debug_mode:
                    print("PUBSUB RECV: %s\n" % json.dumps(res_dict, indent=2)
                    	)
        elif res_dict["type"] == "RECONNECT":
            if debug_mode:
                print("PUBSUB INFO: WebSocket disconnecting to reconnect\n")
            ws.close()
            ntp.reconnect(debug_mode)

        sleep(0.5)


def main():
    global kitti_ash_in_chat
    global kitti_alice_in_chat
    sig = SignalHandler()
    t1 = get_ops()
    t2 = Thread(target = caster_main, daemon = True).start()
    t3 = Thread(target = pubsub_main, daemon = True).start()
    t4 = check_follower_list()

    status = True
    connect_bot_socket()

    chat_msg_prog = re.compile(r"^(\@.*) :(\w+)!\w+@\w+\.tmi\.twitch\.tv " \
        "PRIVMSG #\w+ :(.*)")

    if status:
        if start_exit_msg:
            nbf.send(s, "Hiya!", debug_mode)
        t6 = timers()
        t7 = hydration_alert()
        t8 = nye_timer()
        t9 = check_for_cuties()

    while True:
        if sig.kill_now:
            break
        response = s.recv().decode("utf-8")
        if response == "PING :tmi.twitch.tv\r\n":
            s.send("PONG :tmi.twitch.tv\r\n".encode("utf-8"))
        chat_msg = chat_msg_prog.match(response)
        if chat_msg is not None:
            chat_msg_meta_dict = {}
            chat_msg_meta_raw = chat_msg.group(1).strip()
            chat_msg_meta = chat_msg_meta_raw.split(";")
            for var in chat_msg_meta:
                var_pair = var.split("=")
                chat_msg_meta_dict[var_pair[0]] = var_pair[1]
            chat_msg_meta_dict["login"] = chat_msg.group(2).strip()
            chat_msg_meta_dict["message"] = chat_msg.group(3).strip()

            username = chat_msg_meta_dict["login"]
            message = chat_msg_meta_dict["message"]
            display_name = chat_msg_meta_dict["display-name"].strip()
            user_id = chat_msg_meta_dict["user-id"].strip()

            if display_name == "" or display_name == None:
                display_name = username

            message = message.replace('\\','\\\\').replace('"', '\\"')
            if debug_mode:
                print(message)

            message = message.strip()

            if debug_mode:
                print(message)
            if debug_mode:
                print(response)

            message_list = message.strip().split(" ")
            message_list = list(filter(None, message_list))
            try:
                trigger = str(message_list[0]).lower()
            except (TypeError, AttributeError):
                trigger = message_list[0]
            msg = " ".join(message_list[1:])
            values = {
                "user": username,
                "userid": user_id,
                "host": config["caster_channel"],
                "nickname": config["caster_nickname"],
                "botname": config["bot_nick"],
                "realuser": display_name,
                "msg": msg,
                "cmd": trigger
            }

            if trigger == "!whisper":
                nbf.send, debug_mode(
                    s, "/w %s This is a test whisper! Hello there %s! XD" %
                    (username, username))

            if user_id == "192466634": # Kitti Ash - AshTRGaming
                kitti_ash_in_chat = True
            if user_id == "43625829": # Kitti Alice - AliciaBytes
                kitti_alice_in_chat = True

            for i, msg_word in enumerate(message_list[1:]):
                values["msg%d" % i] = msg_word

            if not status and trigger == "!enable" and \
            		username in super_users:
                status = True
                nbf.send(s, "I return!", debug_mode
                	)

            if status and trigger == "!disable" and username in super_users:
                status = False
                nbf.send(s, "Goodbye, cruel world!", debug_mode)

            if status and trigger == "!restart" and username in super_users:
                if start_exit_msg:
                    nbf.send(s, "Kthxbai!", debug_mode)
                sys.exit(0)

            if status:
                nbf.process_link_protection(s, config, values, message_list,
                	message, username, op_dict, permitted, debug_mode)
                nbf.process_permit_command(s, values, message_list,
                    username, op_dict, permitted, super_users, trigger,
                    debug_mode)
                nbf.process_revoke_command(s, values, message_list,
                    username, op_dict, permitted, super_users, trigger,
                    debug_mode)

                nbf.process_quote_command(s, config, values, message_list,
                    username, op_dict, permitted, super_users, trigger,
                    debug_mode)
                nbf.process_newquote_command(s, values, message_list,
                    username, op_dict, permitted, super_users, trigger,
                    debug_mode)
                nbf.process_editquote_command(s, values, message_list,
                    username, op_dict, permitted, super_users, trigger,
                    debug_mode)
                nbf.process_deletequote_command(s, values, message_list,
                    username, op_dict, permitted, super_users, trigger,
                    debug_mode)

                cooldown_status = config["cooldownmsg"]
                cmds = nbf.build_cmds_dict(account)

                if cmds is not None:
                    nbf.process_edit_command(config, account, s, values,
                        message_list, username, cmds, op_dict, super_users,
                        trigger, debug_mode)

                    cmd_active = nbf.get_active_cmds(cmds)
                    values["cmdlist"] = nbf.build_cmdlist(cmds)
                    if trigger in cmd_active:
                        cmd_data = nbf.get_cmd_data(cmds, trigger)
                        reply = nbf.process_response(
                            cmd_data["response"], values)
                        if reply is not None:
                            if cmd_data["type"] == "text":
                                output = nbf.validate_cmd(values, username,
                                    cmd_data, op_dict, trigger, debug_mode)
                                if output:
                                    nbf.process_text_reply(s, reply,
                                        values["realuser"], limit_str,
                                        debug_mode)
                            elif cmd_data["type"] == "url":
                                output = nbf.validate_cmd(values, username,
                                    cmd_data, op_dict, trigger, debug_mode)
                                if output:
                                    nbf.process_url_reply(s, reply,
                                        values["realuser"], limit_str,
                                        debug_mode)
                            elif cmd_data["type"] == "func":
                                output = nbf.validate_cmd(values, username,
                                    cmd_data, op_dict, trigger, debug_mode)
                                if output:
                                    nbf.process_def_reply(s, reply,
                                        values["realuser"], limit_str,
                                        debug_mode)

        else:
            if debug_mode:
                print(response)

        sleep(0.5)


if __name__ == "__main__":
    main()
