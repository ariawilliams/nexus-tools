from classes.config.NexusBotConfig import NexusBotConfig
from classes.core.NexusBotFunctions import NexusBotFunctions
from classes.core.NexusBotHelper import set_interval
from classes.core.NexusTwitchPubsub import NexusTwitchPubsub
from random import randint
from socket import socket
from ssl import wrap_socket
from threading import Thread
from time import sleep
from twi.classes.core.NexusCore import NexusCore
from urllib import parse
from os.path import join
from os import sep
import json
import re
import requests
import signal
import sys


nbc = NexusBotConfig()
nbf = NexusBotFunctions()
ntp = NexusTwitchPubsub()
ncr = NexusCore()
account = sys.argv[1]
channel = sys.argv[2]
oauth = nbc.retrieve_oauth(account)
s = socket()
s = wrap_socket(s)
ss = socket()
ss = wrap_socket(ss)


class SignalHandler:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


def sender():
    ss.connect(("irc.chat.twitch.tv", 6697))
    ss.send("PASS {}\r\n".format(oauth).encode("utf-8"))
    ss.send("NICK {}\r\n".format(account).encode("utf-8"))
    ss.send("JOIN #{}\r\n".format(channel).encode("utf-8"))

    chat_msg_prog = re.compile(
        r"^:\w+!\w+@\w+\.tmi\.twitch\.tv PRIVMSG #\w+ :")

    x = int(sys.argv[3])

    while True:
        response = ss.recv().decode("utf-8")
        if response == "PING :tmi.twitch.tv\r\n":
            ss.send("PONG :tmi.twitch.tv\r\n".encode("utf-8"))
        ss.send("PRIVMSG #{} :{}\r\n".format(
            channel, "!quote %d" % x).encode("utf-8"))

        x += 1

        sleep(20)


def main():
    sig = SignalHandler()
    t1 = Thread(target = sender, daemon = True).start()

    s.connect(("irc.chat.twitch.tv", 6697))
    s.send("PASS {}\r\n".format(oauth).encode("utf-8"))
    s.send("NICK {}\r\n".format(account).encode("utf-8"))
    s.send("JOIN #{}\r\n".format(channel).encode("utf-8"))

    chat_msg_prog = re.compile(
        r"^:\w+!\w+@\w+\.tmi\.twitch\.tv PRIVMSG #\w+ :")

    while True:
        if sig.kill_now:
            break
        response = s.recv().decode("utf-8")
        if response == "PING :tmi.twitch.tv\r\n":
            s.send("PONG :tmi.twitch.tv\r\n".encode("utf-8"))
        elif chat_msg_prog.match(response) is not None:
            username = re.search(r"\w+", response).group(0)
            username = username.strip()
            message = chat_msg_prog.sub("", response)
            message = message.strip()

            if username == "streamelements" and \
                    message.startswith("@AriiaLux"):
                quote_text = message.replace("@AriiaLux, ", "")
                with open(join(sep, "home", "ubuntu",
                        "%s_quotes.txt" % channel), "a") as f:
                    f.write("%s\n" % quote_text)

        sleep(0.5)


if __name__ == "__main__":
    main()
