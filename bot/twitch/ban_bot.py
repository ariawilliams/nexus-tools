from ban_list import ban_reason
from ban_list import ban_users
from os.path import join
from socket import socket
from ssl import wrap_socket
from sys import argv
from sys import exit
from time import sleep
import signal


try:
    account = argv[1]
except IndexError:
    print("Account to use to connect to chat not specified!")
    exit(1)
try:
    channel = argv[2]
except IndexError:
    channel = account

try:
    file = join("classes", "config", "oauth", "%s_oauth.txt" % channel)
    with open(file, "r") as f:
        oauth = f.read().strip()
except FileNotFoundError:
    print("Saved OAuth key not found!")
    exit(1)

s = socket()
s = wrap_socket(s)


class SignalHandler:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


def main():
    sig = SignalHandler()

    s.connect(("irc.chat.twitch.tv", 6697))
    s.send("PASS {}\r\n".format(oauth).encode("utf-8"))
    s.send("NICK {}\r\n".format(account).encode("utf-8"))
    s.send("JOIN #{}\r\n".format(channel).encode("utf-8"))

    while True:
        if sig.kill_now:
            break

        print("Connected to %s, as %s yeeting the bots..." % (
            account, channel))
        
        i = 0

        for user in ban_users:
            if i == 10:
                i = 0
                sleep(1)
            if i < 10:
                s.send(
                    "PRIVMSG #{} :{}\r\n".format(
                        account,
                        ".ban %s %s" % (user, ban_reason)
                    ).encode("utf-8")
                )
                sleep(0.3)
                i += 1

        exit(0)


if __name__ == "__main__":
    main()
