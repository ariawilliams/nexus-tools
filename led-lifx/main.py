from bot.twitch.classes.core.NexusBotFunctions import NexusBotFunctions
from bot.twitch.classes.config.NexusBotConfig import NexusBotConfig
from bot.twitch.classes.core.NexusBotHelper import set_interval
from twi.classes.core.NexusCore import NexusCore
from colors import named_colors
from colors import disallowed_colors
from datetime import datetime
from random import randint
from socket import socket
from ssl import wrap_socket
from threading import Thread
from time import sleep
import re
import signal
import sys


nbc = NexusBotConfig()
nbf = NexusBotFunctions()
ncr = NexusCore()
account = sys.argv[1]
oauth = nbc.retrieve_oauth("droidexlux")
caster_oauth = nbc.retrieve_oauth(account)
followers = ncr.get_follower_list(account)
debug_mode = False
super_users = ["ariialux"]
s = socket()
s = wrap_socket(s)
ss = socket()
ss = wrap_socket(ss)

status = True
override = False
alert_event = False

queue_list = []
alert_list = []


class SignalHandler:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


def timeout_user(user):
    nbf.send(s, "/timeout %s 1" % user, debug_mode)


def disallowed_handler(user, realuser):
    if user == "wyo123":
        nbf.send(s, "%s > Yes! Kappa" % realuser, debug_mode)
    else:    
        nbf.send(s, "%s > You is cheeky! Turning off the lights is not allowed. " \
            "Please try again." % realuser, debug_mode)


def toggle_power(power):
    if power == 0:
        hue.power(ser, 0, "off")
    elif power == 1:
        hue.marquee(ser, 0, 0, "FF00AA", 2, 0, 0)


@set_interval(30)
def check_stream_up():
    global status
    host_live = ncr.is_host_live(account)

    if not override:
        if status:
            if not host_live:
                toggle_power(0)
                status = False
            else:
                pass
        else:
            if host_live:
                toggle_power(1)
                status = True
            else:
                pass


@set_interval(5)
def print_lists():
    print("left_list - %s" % left_list)
    print("current_left - %s" % current_left)
    print("right_list - %s" % right_list)
    print("current_right - %s" % current_right)
    print("alert_list - %s" % alert_list)


@set_interval(5)
def change_leds():
    global alert_event
    global current_left
    global current_right
    if status:
        try:
            if alert_list[0] == "FOLLOW":
                alert_event = True
                if debug_mode:
                    print("%s alert mode is being set" % alert_list[0])
                # EFFECT WITH "FF00AA"
                del alert_list[0]

            elif alert_list[0] == "SUB":
                alert_event = True
                if debug_mode:
                    print("%s alert mode is being set" % alert_list[0])
                # EFFECT WITH "87CEFA"
                del alert_list[0]

            elif alert_list[0] == "BITS":
                alert_event = True
                if debug_mode:
                    print("%s alert mode is being set" % alert_list[0])
                # EFFECT WITH "FF4400"
                del alert_list[0]

            elif alert_list[0] == "RAID":
                alert_event = True
                if debug_mode:
                    print("%s alert mode is being set" % alert_list[0])
                # EFFECT WITH "FF0000"
                del alert_list[0]

            elif alert_list[0] == "HOST":
                alert_event = True
                if debug_mode:
                    print("%s alert mode is being set" % alert_list[0])
                # EFFECT WITH "00FF00"
                del alert_list[0]

        except IndexError:
            if alert_event:
                # RESET
                alert_event = False

            if status and not alert_event:
                leds_set = False
                try:
                    if left_list[0] == "SIREN":
                        if debug_mode:
                            print("%s mode is being set" % left_list[0])
                        hue.pulse(ser, 0, 2, ["FF0000", "0000FF"], 4)
                        hue.pulse(ser, 0, 1, ["0000FF", "FF0000"], 4)
                        del queue_list[0]
                        del right_list[0]
                        leds_set = True

                    elif queue_list[0] == "RAINBOW":
                        if debug_mode:
                            print("%s mode is being set" % queue_list[0])
                        hue.spectrum(ser, 2, 3, 0)
                        hue.spectrum(ser, 1, 3, 1)
                        del queue_list[0]
                        del right_list[0]
                        leds_set = True

                    elif queue_list[0] == right_list[0]:
                        if debug_mode:
                            print("%s being set on left channel" % \
                                queue_list[0])
                            print("%s being set on right channel" % \
                                right_list[0])
                        hue.marquee(ser, 0, 0, queue_list[0], 2, 0, 0)
                        current_left = queue_list[0]
                        current_right = right_list[0]
                        del queue_list[0]
                        del right_list[0]
                        leds_set = True
                except IndexError:
                    pass

                if not leds_set:
                    try:
                        if debug_mode:
                            print("%s being set on left channel" % \
                                left_list[0])
                        hue.marquee(ser, 0, 2, left_list[0], 2, 0, 0)
                        current_left = left_list[0]
                        del left_list[0]
                    except IndexError:
                        pass

                    try:
                        if debug_mode:
                            print("%s being set on right channel" % \
                                right_list[0])
                        hue.marquee(ser, 0, 1, right_list[0], 2, 0, 0)
                        current_right = right_list[0]
                        del right_list[0]
                    except IndexError:
                        pass


@set_interval(10)
def check_follower_list():
    global followers
    new_followers = ncr.get_follower_list(account)
    if len(new_followers) > 10:
        recent_follows = new_followers[:9]
    else:
        recent_follows = new_followers
    for follower in recent_follows:
        if follower not in followers:
            alert_list.append("FOLLOW")
            followers = new_followers

    if new_followers != followers:
        followers = new_followers


def caster_main():
    ss.connect(("irc.chat.twitch.tv", 6697))
    ss.send("PASS {}\r\n".format(caster_oauth).encode("utf-8"))
    ss.send("NICK {}\r\n".format(account).encode("utf-8"))
    ss.send("JOIN #{}\r\n".format(account).encode("utf-8"))
    ss.send("CAP REQ :twitch.tv/tags\r\n".encode("utf-8"))
    ss.send("CAP REQ :twitch.tv/commands\r\n".encode("utf-8"))

    privmsg_prog = re.compile(r"^(\@.*) :(\w+)!\w+@\w+\.tmi\.twitch\.tv " \
        "PRIVMSG #\w+ :(.*)")
    usernotice_prog = re.compile(r"^(\@.*) :tmi\.twitch\.tv USERNOTICE #\w+" \
        "(.*)")
    host_msg_prog = re.compile(r"^:jtv!jtv@jtv\.tmi\.twitch\.tv PRIVMSG " \
        "\w+ :")

    while True:
        response_two = ss.recv().decode("utf-8")
        if response_two == "PING :tmi.twitch.tv\r\n":
            ss.send("PONG :tmi.twitch.tv\r\n".encode("utf-8"))
        pv = privmsg_prog.match(response_two)
        if pv is not None:
            privmsg_meta_dict = {}
            privmsg_meta_raw = pv.group(1).strip()
            privmsg_meta = privmsg_meta_raw.split(";")
            for var in privmsg_meta:
                var_pair = var.split("=")
                privmsg_meta_dict[var_pair[0]] = var_pair[1]
            privmsg_meta_dict["login"] = pv.group(2).strip()
            privmsg_meta_dict["message"] = pv.group(3).strip()

            if nbf.is_bits_alert(privmsg_meta_dict):
                alert_list.append("BITS")

        un = usernotice_prog.match(response_two)
        if un is not None:
            usernotice_meta_dict = {}
            usernotice_meta_raw = un.group(1).strip()
            usernotice_meta = usernotice_meta_raw.split(";")
            for var in usernotice_meta:
                var_pair = var.split("=")
                usernotice_meta_dict[var_pair[0]] = var_pair[1]
            if un.group(2) is not None:
                usernotice_meta_dict["message"] = un.group(2).strip()[1:]
            else:
                usernotice_meta_dict["message"] = ""

            if nbf.is_sub_alert(usernotice_meta_dict):
                alert_list.append("SUB")
            elif nbf.is_raid_alert(usernotice_meta_dict):
                alert_list.append("RAID")

        if host_msg_prog.match(response_two) is not None:
            host_msg = host_msg_prog.sub("", response_two)
            host_msg = host_msg.strip()
            if nbf.is_host_alert(host_msg):
                alert_list.append("HOST")

        sleep(1)


def main():
    global status
    global override
    sig = SignalHandler()
    t1 = Thread(target = change_leds, daemon = True).start()
    t2 = Thread(target = caster_main, daemon = True).start()
    t2 = Thread(target = caster_main, daemon = True).start()
    t3 = check_follower_list()
    t4 = Thread(target = check_stream_up, daemon = True).start()
    if debug_mode:
        t5 = Thread(target = print_lists, daemon = True).start()

    s.connect(("irc.chat.twitch.tv", 6697))
    s.send("PASS {}\r\n".format(oauth).encode("utf-8"))
    s.send("NICK {}\r\n".format("droidexlux").encode("utf-8"))
    s.send("JOIN #{}\r\n".format(account).encode("utf-8"))

    chat_msg_prog = re.compile(
        r"^:\w+!\w+@\w+\.tmi\.twitch\.tv PRIVMSG #\w+ :")

    if debug_mode:
        print("Initiating LEDs...")
    hue.marquee(ser, 0, 0, "FF00AA", 2, 0, 0)

    while True:
        if sig.kill_now:
            break
        response = s.recv().decode("utf-8")
        if response == "PING :tmi.twitch.tv\r\n":
            s.send("PONG :tmi.twitch.tv\r\n".encode("utf-8"))
        elif chat_msg_prog.match(response) is not None:
            username = re.search(r"\w+", response).group(0)
            username = username.strip()
            message = chat_msg_prog.sub("", response)
            message = message.strip()
            if debug_mode:
                print(response)

            message_list = message.strip().split(" ")
            message_list = list(filter(None, message_list))
            try:
                trigger = str(message_list[0]).lower()
            except (TypeError, AttributeError):
                trigger = message_list[0]
            msg = " ".join(message_list[1:])
            values = {
                "user": username,
                "host": account,
                "botname": "droidexlux",
                "realuser": nbf.get_display_name(username),
                "msg": msg,
                "cmd": trigger
            }

            if username in super_users and \
                    trigger == "!ledoff" and status:
                toggle_power(0)
                status = False
                override = True
            elif username in super_users and \
                    trigger == "!ledon" and not status:
                toggle_power(1)
                status = True
                override = False

            hex_color_prog = re.compile(r"^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})")

            try:
                hex_color = message_list[1].upper()
            except IndexError:
                hex_color = None

            try:
                next_left = left_list[0]
            except IndexError:
                next_left = ""

            try:
                next_right = right_list[0]
            except IndexError:
                next_right = ""

            if trigger == "!ledsiren":
                if "SIREN" not in left_list:
                    left_list.insert(0, "SIREN")
                    left_list.insert(1, current_left)
                    right_list.insert(0, "SIREN")
                    right_list.insert(1, current_right)
                    nbf.send(s, "%s > \"I live my life a quarter mile at a " \
                        "time.\"" % values["realuser"], debug_mode)

            elif trigger == "!ledrainbow":
                if "RAINBOW" not in left_list:
                    left_list.insert(0, "RAINBOW")
                    left_list.insert(1, current_left)
                    right_list.insert(0, "RAINBOW")
                    right_list.insert(1, current_right)
                    nbf.send(s, "%s > Sunshine, lollipops and rainbows, " \
                        "everything that\'s wonderful is what I feel when " \
                        "we\'re together!" % values["realuser"], debug_mode)

            elif trigger == "!ledl":
                if hex_color is None:
                    nbf.send(s, "%s > Color not specified! Please try " \
                        "again." % values["realuser"], \
                        debug_mode)
                elif hex_color == "BLACK" or hex_color in disallowed_colors:
                    disallowed_handler(values["user"], values["realuser"])
                elif hex_color in named_colors.keys():
                    if named_colors[hex_color] not in left_list:
                        left_list.append(named_colors[hex_color])
                elif hex_color_prog.match(hex_color) is not None:
                    if hex_color not in left_list:
                        left_list.append(hex_color)
                else:
                    nbf.send(s, "%s > Invalid color \"%s\". Please try " \
                        "again." % (values["realuser"], hex_color), \
                        debug_mode)
                
            elif trigger == "!ledr":
                if hex_color is None:
                    nbf.send(s, "%s > Color not specified! Please try " \
                        "again." % values["realuser"], \
                        debug_mode)
                elif hex_color == "BLACK" or hex_color in disallowed_colors:
                    disallowed_handler(values["user"], values["realuser"])
                elif hex_color in named_colors.keys():
                    if named_colors[hex_color] not in right_list:
                        right_list.append(named_colors[hex_color])
                elif hex_color_prog.match(hex_color) is not None:
                    if hex_color not in right_list:
                        right_list.append(hex_color)
                else:
                    nbf.send(s, "%s > Invalid color \"%s\". Please try " \
                        "again." % (values["realuser"], hex_color), \
                        debug_mode)

            elif trigger == "!ledlr":
                if hex_color is None:
                    nbf.send(s, "%s > Color not specified! Please try " \
                        "again." % values["realuser"], \
                        debug_mode)
                elif hex_color == "BLACK" or hex_color in disallowed_colors:
                    disallowed_handler(values["user"], values["realuser"])
                elif hex_color in named_colors.keys():
                    if named_colors[hex_color] != next_left \
                            and named_colors[hex_color] != next_right:
                        left_list.insert(0, named_colors[hex_color])
                        right_list.insert(0, named_colors[hex_color])
                elif hex_color_prog.match(hex_color) is not None:
                    if hex_color != next_left \
                            and hex_color != next_right:
                        left_list.insert(0, hex_color)
                        right_list.insert(0, hex_color)
                else:
                    nbf.send(s, "%s > Invalid color \"%s\". Please try " \
                        "again." % (values["realuser"], hex_color), \
                        debug_mode)

        else:
            if debug_mode:
                print(response)

        sleep(0.5)


if __name__ == "__main__":
    main()
