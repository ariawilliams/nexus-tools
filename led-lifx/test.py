from copy import deepcopy
from time import sleep
from helper import convert_color

from lifxlan import LifxLAN

lifx = LifxLAN(None, False)

strip = lifx.get_device_by_name("LIFX Z Chair")

az = strip.get_color_zones()
oz = deepcopy(az)
zc = len(az)

main_col = convert_color("FF0000")
back_col = convert_color("000000")

refs = [ x for x, v in enumerate(az) ]
off = refs[:3] + refs[zc - 3:]
on = [ x for x in refs if x not in off ]

for z in off:
    strip.set_zone_color(z, z, back_col)

for z in on:
    strip.set_zone_color(z, z, main_col)

az = strip.get_color_zones()
dim = []
bright = []
for x, [h,s,v,k] in enumerate(az):
    if x in on:
        dim.append((h,s,14000,k))
        bright.append((h,s,v,k))
    else:
        dim.append((h,s,v,k))
        bright.append((h,s,v,k))

while True:
    strip.set_zone_colors(bright, 2000, True)
    sleep(2)
    strip.set_zone_colors(dim, 2000, True)
    sleep(2)
