

def convert_color(color, brightness = 0.5, temp = 3500):
    if not isinstance(temp, (int, float)):
        temp = 3500
    elif temp < 2500:
        temp = 2500
    elif temp > 9000:
        temp = 9000
    temp = int(temp)
    ratio = 65535 / 100
    if len(color) == 6:
        rgb = [ int(color[i:i + 2], 16) for i in range(0, len(color), 2) ]
        try:
            r = rgb[0] / 255
        except ValueError:
            r = 0
        try:
            g = rgb[1] / 255
        except ValueError:
            g = 0
        try:
            b = rgb[2] / 255
        except ValueError:
            b = 0
        mx = max(r, g, b)
        mn = min(r, g, b)
        df = mx - mn
        if mx == mn:
            h = 0
        elif mx == r:
            h = (60 * ((g - b) / df) + 360) % 360
        elif mx == g:
            h = (60 * ((b - r) / df) + 120) % 360
        elif mx == b:
            h = (60 * ((r - g) / df) + 240) % 360
        if mx == 0:
            s = 0
        else:
            s = (df / mx) * 100
        v = mx * 100
        res = [ h, s, v ]
        res[0] = (res[0] / 360 * 100)
        res[2] = res[2] * brightness
        final = [ int(x * ratio) for x in res ]
        final.append(temp)
        return final
    else:
        return None
