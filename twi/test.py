from classes.core.NexusCore import NexusCore

from classes.interfaces.NexusTwitchInterface \
     import NexusTwitchInterface
import json

nti = NexusTwitchInterface()

ncr = NexusCore()

print(json.dumps(nti.get_channel_id("qbunnytv"), indent = 4, sort_keys = False))

# from classes.interfaces.NexusStreamlinkInterface \
#     import NexusStreamlinkInterface

# nsi = NexusStreamlinkInterface()

# print(nsi.get_quality_options("nibenwensworld"))

# # from classes.interfaces.NexusQueryInterface import NexusQueryInterface

# # nqi = NexusQueryInterface()

# # print(nqi.get_openweathertime_result("nepal"))