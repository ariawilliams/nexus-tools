from bs4 import BeautifulSoup
from time import sleep
from urllib import parse
import numbers
import re
import requests
from ..logic.NexusDeltaFunctions import NexusDeltaFunctions

ndf = NexusDeltaFunctions()

headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) " \
        "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 " \
        "Safari/537.36 OPR/68.0.3618.125",
    "Accept-Language": "en-us",
    "Accept-Encoding": "br, gzip, deflate",
    "Accept": "test/html,application/xhtml+xml,application/xml;q=0.9," \
        "*/*;q=0.8",
    "Referer": "https://www.google.com/"
}

class NexusSoupInterface(object):

    def get_googlesearch_result(self, query, count = 5):
        # results = []
        # query = str(query).strip()

        # urls = [
        #     "https://www.google.com/search?gl=us&safe=active&q=%s" % query
        # ]

        # for url in urls:
        #     headers["User-Agent"] = ndf.get_random_ua_static()
        #     page = requests.get(url, headers = headers)
        #     soup = BeautifulSoup(page.content, "html.parser")

        #     a_tags = soup.select("div.g div.r > a")
        #     if a_tags is None:
        #         return None
        #     for i, a_tag in enumerate(a_tags):
        #         if i > count:
        #             break
        #         link = a_tag["href"]
        #         r = requests.get(link, allow_redirects = True,
        #             headers = headers)
        #         true_link = r.url
        #         final_link = requests.get(
        #             "http://tinyurl.com/api-create.php", params = dict(
        #             url = true_link)).text

        #         final_link = final_link.replace("http://", "")
        #         final_link = final_link.replace("https://", "")

        #         results.append({
        #             "title": a_tag.select_one("h3").get_text(),
        #             "url": final_link
        #         })

        # return results
        return None


    def get_googleweather_result(self, query):
        # query = str(query).strip()

        # url = "https://www.google.com/search?gl=us&safe=active&q=weather" \
        #     "+in+" + query + "+in+celsius"

        # headers["User-Agent"] = ndf.get_random_ua_static()
        # page = requests.get(url, headers = headers)
        # soup = BeautifulSoup(page.content, "html.parser")

        # div_tag = soup.select("div.g div#wob_wc")

        # temp_type = soup.select_one("div.g div#wob_wc div.wob-unit span.wob_t")
        # if temp_type is not None:
        #     temp_type = temp_type.get_text()
        # else:
        #     return None

        # location = soup.select_one("div.g div#wob_wc div#wob_loc")
        # if location is not None:
        #     location = location.get_text()
        # else:
        #     return None
        # current_temp = soup.select_one("div.g div#wob_wc span.wob_t")
        # if current_temp is not None:
        #     current_temp = current_temp.get_text()
        # else:
        #     return None
        # high_temp = soup.select_one("div.g div#wob_wc div.wob_df.wob_ds " \
        #     "div.vk_gy span.wob_t")
        # if high_temp is not None:
        #     high_temp = high_temp.get_text()
        # else:
        #     return None
        # low_temp = soup.select_one("div.g div#wob_wc div.wob_df.wob_ds " \
        #     "div.vk_lgy span.wob_t")
        # if low_temp is not None:
        #     low_temp = low_temp.get_text()
        # else:
        #     return None
        # current_stat = soup.select_one("div.g div#wob_wc div#wob_dcp")
        # if current_stat is not None:
        #     current_stat = current_stat.get_text()
        # else:
        #     return None
        # humidity = soup.select_one("div.g div#wob_wc div#wob_d span#wob_hm")
        # if humidity is not None:
        #     humidity = humidity.get_text()
        # else:
        #     return None

        # result = {
        #     "location": location.replace("Weather for ", ""),
        #     "current_temp": "%s%s" % (current_temp, temp_type),
        #     "high_temp": "%s%s" % (high_temp, temp_type),
        #     "low_temp": "%s%s" % (low_temp, temp_type),
        #     "status": current_stat,
        #     "humidity": humidity
        # }

        # return result
        return None


    def get_googledefine_result(self, query):
        # query = str(query).strip()

        # url = "https://www.google.com/search?gl=us&safe=active&q=define+%s" \
        #     % query

        # headers["User-Agent"] = ndf.get_random_ua_static()
        # page = requests.get(url, headers = headers)
        # soup = BeautifulSoup(page.content, "html.parser")

        # word = soup.select_one("div.g div.lr_dct_ent div span")
        # if word is None:
        #     return None
        # word_type = soup.select_one("div.g div.lr_dct_ent div " \
        #     "div.lr_dct_sf_h")
        # if word_type is None:
        #     return None
        # word_definition = soup.select_one("div.g div.lr_dct_ent div " \
        #     "ol.lr_dct_sf_sens li div.PNlCoe.XpoqFe span")
        # if word_definition is None:
        #     return None

        # result = {
        #     "word": word.get_text().capitalize(),
        #     "type": word_type.get_text().capitalize(),
        #     "definition": word_definition.get_text().capitalize()
        # }

        # return result
        return None


    def get_googletime_result(self, query):
        # query = str(query).strip()

        # url = "https://www.google.com/search?gl=us&safe=active&q=" \
        #     "time+in+%s" % query

        # headers["User-Agent"] = ndf.get_random_ua_static()
        # page = requests.get(url, headers = headers)
        # soup = BeautifulSoup(page.content, "html.parser")

        # time = soup.select_one("div.g div.vk_c div.vk_bk")
        # if time is None:
        #     return None
        # date_timezone = soup.select_one("div.g div.vk_c div.vk_gy")
        # if date_timezone is None:
        #     return None
        # location = soup.select_one("div.g div.vk_c > span")
        # if location is None:
        #     return None

        # result = {
        #     "time": str(time.get_text()).strip(),
        #     "date_timezone": str(date_timezone.get_text()).strip(),
        #     "location": str(location.get_text()).replace(
        #         "Time in", "").strip()
        # }

        # return result
        return None


    def get_googlecurrency_result(self, query):
        # query = str(query).strip()

        # query_list = query.split(" ")

        # currency_codes = [
        #     "AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "AUD", "AWG",
        #     "AZN", "BAM", "BBD", "BDT", "BGN", "BHD", "BIF", "BMD", "BND",
        #     "BOB", "BRL", "BSD", "BTN", "BWP", "BYN", "BZD", "CAD", "CDF",
        #     "CHF", "CLP", "CNY", "COP", "CRC", "CUC", "CUP", "CVE", "CZK",
        #     "DJF", "DKK", "DOP", "DZD", "EGP", "ERN", "ETB", "EUR", "FJD",
        #     "FKP", "GBP", "GEL", "GGP", "GHS", "GIP", "GMD", "GNF", "GTQ",
        #     "GYD", "HKD", "HNL", "HRK", "HTG", "HUF", "IDR", "ILS", "IMP",
        #     "INR", "IQD", "IRR", "ISK", "JEP", "JMD", "JOD", "JPY", "KES",
        #     "KGS", "KHR", "KMF", "KPW", "KRW", "KWD", "KYD", "KZT", "LAK",
        #     "LBP", "LKR", "LRD", "LSL", "LYD", "MAD", "MDL", "MGA", "MKD",
        #     "MMK", "MNT", "MOP", "MRO", "MUR", "MVR", "MWK", "MXN", "MYR",
        #     "MZN", "NAD", "NGN", "NIO", "NOK", "NPR", "NZD", "OMR", "PAB",
        #     "PEN", "PGK", "PHP", "PKR", "PLN", "PYG", "QAR", "RON", "RSD",
        #     "RUB", "RWF", "SAR", "SBD", "SCR", "SDG", "SEK", "SGD", "SHP",
        #     "SLL", "SOS", "SPL", "SRD", "STD", "SVC", "SYP", "SZL", "THB",
        #     "TJS", "TMT", "TND", "TOP", "TRY", "TTD", "TVD", "TWD", "TZS",
        #     "UAH", "UGX", "USD", "UYU", "UZS", "VEF", "VND", "VUV", "WST",
        #     "XAF", "XCD", "XDR", "XOF", "XPF", "YER", "ZAR", "ZMW", "ZWD"
        # ]

        # source_currency = query_list[1].upper() in currency_codes
        # destination_currency = query_list[3].upper() in currency_codes

        # if not source_currency:
        #     return {
        #         "error": "Source currency invalid",
        #         "input": query_list[1]
        #     }
        # elif not destination_currency:
        #     return {
        #         "error": "Destination currency invalid",
        #         "input": query_list[3]
        #     }

        # url = "https://www.google.com/search?gl=us&safe=active&q=" \
        #     "%s+%s+in+%s" % (query_list[0], query_list[1].upper(),
        #     query_list[3].upper())

        # headers["User-Agent"] = ndf.get_random_ua_static()
        # page = requests.get(url, headers = headers)
        # soup = BeautifulSoup(page.content, "html.parser")

        # conversion = soup.select_one("div.g div.vk_c " \
        #     "div#knowledge-currency__v2-header")
        # if conversion is None:
        #     return None

        # result_list = conversion.get_text().split("equals")
        # source_list = result_list[0].strip().split(" ")
        # dest_list = result_list[1].strip().split(" ")

        # try:
        #     source_amount = float(source_list[0].strip())
        #     dest_amount = float(dest_list[0].strip())
        # except:
        #     return None

        # result = {
        #     "source_amount": "%.2f" % source_amount,
        #     "source_name": ndf.ucwords(" ".join(source_list[1:])),
        #     "dest_amount": "%.2f" % dest_amount,
        #     "dest_name": ndf.ucwords(" ".join(dest_list[1:]))
        # }

        # return result
        return None


    def get_steamstore_result(self, query, store_code = "AU"):
        results = []
        query = str(query).strip()

        url_options = {
            "term": query,
            "f": "games",
            "cc": store_code,
            "lang": "english"
        }

        url = "https://store.steampowered.com/search/suggest?%s" % (parse.urlencode(url_options))

        page = requests.get(url, allow_redirects = False, headers = headers)
        soup = BeautifulSoup(page.content, "html.parser")
    
        a_tags = soup.find_all("a")
        if a_tags is None:
            return None
        for a_tag in a_tags:
            try:
                game_appid = a_tag["data-ds-appid"]
                game_storeurl = a_tag["href"]
            except KeyError:
                continue
            try:
                game_name = a_tag.select_one("div.match_name").get_text()
                game_price = a_tag.select_one("div.match_price").get_text()
                game_name = re.sub(r"[^A-Za-z0-9\-\s\(\)\[\]\:\;\<\>\'\"\/\\\\]+", "", game_name)
            except AttributeError:
                continue
            results.append({
                "game_appid": game_appid,
                "game_storeurl": game_storeurl,
                "game_name": game_name,
                "game_price": game_price
            })

        return results
