import json
import requests
import re
from urllib import request
from urllib import parse
from random import randint
from ..config.NexusConfig import NexusConfig
from .NexusDatabaseInterface import NexusDatabaseInterface


ndi = NexusDatabaseInterface()
nc = NexusConfig()
config = nc.retrieve_config()

master_channel = "ariialux"
        
class NexusTwitchInterface(object):

    def build_access_token_url(self, scopes, client_id = None):
        if client_id is None:
            client_id = config["twitch-client-id"]

        url_options = {
            "client_id": client_id,
            "redirect_uri": "https://ariia.xyz/authorize",
            "response_type": "code"
        }

        if scopes.lower() == "all":
            url_options["scope"] = ndi.get_text_data(
                "twitch_oauth_all_scopes.txt"
            )
        elif scopes.lower() == "mod":
            url_options["scope"] = ndi.get_text_data(
                "twitch_oauth_mod_scopes.txt"
            )
        elif scopes.lower() == "chat":
            url_options["scope"] = ndi.get_text_data(
                "twitch_oauth_chat_scopes.txt"
            )

        url_options["state"] = "4Lvpx8Gh66kCdL2ZCuUyhP5gf2SPDkfy"
        url_options["force_verify"] = "true"

        return "https://id.twitch.tv/oauth2/authorize?%s" % (
            parse.urlencode(url_options, safe = ":+/")
        )


    def request_oauth(self, access_token, client_id = None,
            client_secret = None):
        if client_id is None:
            client_id = config["twitch-client-id"]

        if client_secret is None:
            client_secret = config["twitch-client-secret"]

        if access_token is None:
            return None

        url_options = {
            "client_id": client_id,
            "client_secret": client_secret,
            "code": access_token,
            "grant_type": "authorization_code",
            "redirect_uri": "https://ariia.xyz/authorize"
        }

        url = "https://id.twitch.tv/oauth2/token?%s" % (
            parse.urlencode(url_options, safe = ":+/")
        )

        r = requests.post(url)

        return r.json()


    def refresh_oauth(self, channel = None, client_id = None,
            client_secret = None):
        if client_id is None:
            client_id = config["twitch-client-id"]

        if client_secret is None:
            client_secret = config["twitch-client-secret"]

        if channel is None:
            channel = master_channel

        oauth_dict = ndi.retrieve_oauth(channel)

        if oauth_dict is None:
            return None

        try:
            refresh_token = oauth_dict["refresh_token"]
        except KeyError:
            return None

        url_options = {
            "grant_type": "refresh_token",
            "refresh_token": refresh_token,
            "client_id": client_id,
            "client_secret": client_secret,
        }

        url = "https://id.twitch.tv/oauth2/token?%s" % (
            parse.urlencode(url_options)
        )

        r = requests.post(url)

        response_dict = r.json()

        try:
            oauth_dict = {
                "access_token": response_dict["access_token"],
                "refresh_token": response_dict["refresh_token"],
            }
            ndi.put_oauth(oauth_dict, channel)
            return "Success"
        except KeyError:
            return None


    def validate_oauth(self, channel = None, client_id = None):
        if client_id is None:
            client_id = config["twitch-client-id"]

        if channel is None:
            channel = master_channel

        oauth_dict = ndi.retrieve_oauth(channel)

        if oauth_dict is None:
            return None

        try:
            access_token = oauth_dict["access_token"]
        except KeyError:
            return None

        request_headers = {
            "Authorization": "Bearer %s" % access_token,
        }

        url = "https://id.twitch.tv/oauth2/validate"

        r = requests.get(url, headers = request_headers)

        response_dict = r.json()

        try:
            if response_dict["client_id"] == client_id:
                return access_token
        except KeyError:
            self.refresh_oauth()
        

    def get_curl(self, url_input, url_options = None,
            client_id = None):
        if client_id is None:
            client_id = config["twitch-client-id"]

        if url_options is not None:
            url = "%s?%s" % (url_input, parse.urlencode(url_options))
        else:
            url = url_input

        request_headers = {
            "Authorization": "Bearer %s" % self.validate_oauth(),
            "Client-ID": client_id
        }

        if request_headers is not None:
            r = requests.get(url, headers = request_headers)
        else:
            r = requests.get(url)

        json_dict = r.json()
        json_final = json.dumps(json_dict, indent = 4, sort_keys = False)

        return [ json_dict, str(json_final) ]


    def get_viewer_list(self, channel):
        if channel is not None:
            channel = re.sub("(\@|\%40)", "", channel)
            channel = parse.quote(parse.unquote(channel))
        else:
            return None

        url = "http://tmi.twitch.tv/group/user/%s/chatters" % channel

        request_headers = None

        if request_headers is not None:
            r = requests.get(url, headers = request_headers)
        else:
            r = requests.get(url)

        json_dict = r.json()
        json_final = json.dumps(json_dict, indent = 4, sort_keys = False)

        return [ json_dict, str(json_final) ]


    def get_channel_id(self, channel, user = None):
        if channel is not None:
            channel = re.sub("(\@|\%40)", "", channel)
            channel = parse.quote(parse.unquote(channel))
        else:
            return None

        if user is None:
            user = channel

        url_options = { "login": user }
        url_input = "https://api.twitch.tv/helix/users"

        response_list = self.get_curl(url_input, url_options)

        try:
            if response_list[0]["data"]:
                return response_list
            else:
                return None
        except KeyError:
            return None


    def get_subscriber_status(self, channel, user):
        if channel is not None:
            channel = re.sub("(\@|\%40)", "", channel)
            channel = parse.quote(parse.unquote(channel))
        else:
            return None

        channel_id_data = self.get_channel_id(channel)
        if channel_id_data is not None:
            channel_id = channel_id_data[0]["data"][0]["id"]
        else:
            return None

        if channel != user:
            user_id_data = self.get_channel_id(channel, user)
            if user_id_data is not None:
                uer_id = user_id_data[0]["data"][0]["id"]
            else:
                return None
        else:
            user_id = channel_id

        url_options = { "broadcaster_id": channel_id, "user_id": user_id }
        url_input = "https://api.twitch.tv/helix/subscriptions"

        return self.get_curl(url_input, url_options)


    def get_channel_data(self, channel):
        if channel is not None:
            channel = re.sub("(\@|\%40)", "", channel)
            channel = parse.quote(parse.unquote(channel))
        else:
            return None

        channel_id_data = self.get_channel_id(channel)
        if channel_id_data is not None:
            channel_id = channel_id_data[0]["data"][0]["id"]
        else:
            return None
        
        url_options = { "broadcaster_id": channel_id }
        url_input = "https://api.twitch.tv/helix/channels"

        return self.get_curl(url_input, url_options)


    def get_video_data(self, channel):
        if channel is not None:
            channel = re.sub("(\@|\%40)", "", channel)
            channel = parse.quote(parse.unquote(channel))
        else:
            return None

        channel_id_data = self.get_channel_id(channel)
        if channel_id_data is not None:
            channel_id = channel_id_data[0]["data"][0]["id"]
        else:
            return None

        url_options = { "user_id": channel_id }
        url_input = "https://api.twitch.tv/helix/videos"

        return self.get_curl(url_input, url_options)


    def get_stream_data(self, channel):
        if channel is not None:
            channel = re.sub("(\@|\%40)", "", channel)
            channel = parse.quote(parse.unquote(channel))
        else:
            return None

        channel_id_data = self.get_channel_id(channel)
        if channel_id_data is not None:
            channel_id = channel_id_data[0]["data"][0]["id"]
        else:
            return None

        url_options = { "user_id": channel_id }
        url_input = "https://api.twitch.tv/helix/streams"

        return self.get_curl(url_input, url_options)


    def get_bits_data(self, channel = None):
        # if channel is not None:
        #     if channel is not None:
        #         channel = re.sub("(\@|\%40)", "", channel)
        #         channel = parse.quote(parse.unquote(channel))
        #     else:
        #         return None
        #     channel_id_data = self.get_channel_id(channel)
        #     if channel_id_data is not None:
        #         channel_id = channel_id_data[0]["data"][0]["id"]
        #     else:
        #         return None
        #     url_options = { "channel_id" : channel_id }
        # else:
        #     url_options = None

        # url_input = "https://api.twitch.tv/helix/bits/actions"

        # return self.get_curl(url_input, url_options)

        # Not in Helix so return None
        return None


    def get_random_stream(self, channel, viewer_tier):
        # if viewer_tier == 1:
        #     random_offset = randint(1,100)
        # elif viewer_tier == 2:
        #     random_offset = randint(100,500)
        # elif viewer_tier == 3:
        #     random_offset = randint(500,2000)
        # elif viewer_tier == 4:
        #     random_offset = randint(2000,8000)
        # elif viewer_tier == 5:
        #     random_offset = randint(8000,16000)
        # else:
        #     random_offset = randint(500,2000)

        # url_options = {
        #     "limit": "1",
        #     "offset": random_offset,
        #     "stream_type": "live"
        # }
        # url_input = "https://api.twitch.tv/helix/streams"

        # return self.get_curl(url_input, url_options)

        # Too difficult in Helix so return None
        return None


    def get_random_live_stream(self):
        # tier_selector = randint(1,5)

        # if tier_selector == 1:
        #     random_offset = randint(1,100)
        # elif tier_selector == 2:
        #     random_offset = randint(100,500)
        # elif tier_selector == 3:
        #     random_offset = randint(500,2000)
        # elif tier_selector == 4:
        #     random_offset = randint(2000,8000)
        # elif tier_selector == 5:
        #     random_offset = randint(8000,16000)
        # else:
        #     random_offset = randint(500,2000)

        # url_options = {
        #     "limit": "1",
        #     "offset": random_offset,
        #     "stream_type": "live"
        # }
        # url_input = "https://api.twitch.tv/helix/streams"

        # return self.get_curl(url_input, url_options)

        # Too difficult in Helix so return None
        return None


    def get_games_search(self, channel, query):
        query = parse.unquote(query)

        url_options = {
            "query": query,
            "first": "1"
        }
        url_input = "https://api.twitch.tv/helix/search/categories"

        return self.get_curl(url_input, url_options)


    def get_streams_search(self, channel, query):
        query = parse.unquote(query)

        try:
            game_dict = self.get_games_search(channel, query)
            game_id = game_dict[0]["data"][0]["id"]
        except KeyError:
            return None

        url_options = {
            "game_id": game_id,
            "first": "1"
        }
        url_input = "https://api.twitch.tv/helix/streams"

        return self.get_curl(url_input, url_options)


    def get_streams_search_game(self, channel, game_name):
        game_name = parse.unquote(game_name)

        try:
            game_dict = self.get_games_search(channel, game_name)
            game_id = game_dict[0]["data"][0]["id"]
        except KeyError:
            return None

        url_options = {
            "game_id": game_id,
            "first": "100"
        }
        url_input = "https://api.twitch.tv/helix/streams"

        final = { "data": [] }

        initial_dict = self.get_curl(url_input, url_options)[0]

        for record in initial_dict["data"]:
                final["data"].append(record)

        if initial_dict["pagination"]:
            url_options["after"] = initial_dict["pagination"]["cursor"]
            new_dict = self.get_curl(url_input, url_options)[0]

            for record in new_dict["data"]:
                final["data"].append(record)

            while True:
                if new_dict["pagination"]:
                    url_options["after"] = new_dict["pagination"]["cursor"]
                    new_dict = self.get_curl(url_input, url_options)[0]

                    for record in new_dict["data"]:
                        final["data"].append(record)
                else:
                    break

        return final


    def get_channel_search(self, channel, query):
        query = parse.unquote(query)

        url_options = {
            "query": query,
            "first": "1"
        }
        url_input = "https://api.twitch.tv/helix/search/channels"

        return self.get_curl(url_input, url_options)


    def get_follower_data(self, channel, follower):
        if channel is not None:
            channel = re.sub("(\@|\%40)", "", channel)
            channel = parse.quote(parse.unquote(channel))
        else:
            return None
        channel_id_data = self.get_channel_id(channel)
        if channel_id_data is not None:
            channel_id = channel_id_data[0]["data"][0]["id"]
        else:
            return None

        follower = parse.quote(parse.unquote(follower))
        follower_id_data = self.get_channel_id(channel, follower)
        if follower_id_data is not None:
            channel_id = follower_id_data[0]["data"][0]["id"]
        else:
            return None

        url_options = { "to_id": channel_id, "from_id": follower_id }
        url_input = "https://api.twitch.tv/helix/users/follows"

        return self.get_curl(url_input, url_options)


    def get_recent_followers(self, channel, amount):
        if channel is not None:
            channel = re.sub("(\@|\%40)", "", channel)
            channel = parse.quote(parse.unquote(channel))
        else:
            return None
        channel_id_data = self.get_channel_id(channel)
        if channel_id_data is not None:
            channel_id = channel_id_data[0]["data"][0]["id"]
        else:
            return None
        
        try:
            amount = int(amount)
            if amount > 100:
                amount = 100
        except ValueError:
            return None

        url_options = { "to_id": channel_id, "first": amount }
        url_input = "https://api.twitch.tv/helix/users/follows"

        return self.get_curl(url_input, url_options)


    def get_first_followers(self, channel, amount):
        if channel is not None:
            channel = re.sub("(\@|\%40)", "", channel)
            channel = parse.quote(parse.unquote(channel))
        else:
            return None
        channel_id_data = self.get_channel_id(channel)
        if channel_id_data is not None:
            channel_id = channel_id_data[0]["data"][0]["id"]
        else:
            return None
        
        try:
            amount = int(amount)
            if amount > 100:
                amount = 100
        except ValueError:
            return None

        url_options = { "to_id": channel_id, "first": "100" }
        url_input = "https://api.twitch.tv/helix/users/follows"

        final = { "data": [] }

        initial_dict = self.get_curl(url_input, url_options)[0]

        for record in initial_dict["data"]:
                final["data"].append(record)

        if initial_dict["pagination"]:
            url_options["after"] = initial_dict["pagination"]["cursor"]
            new_dict = self.get_curl(url_input, url_options)[0]

            for record in new_dict["data"]:
                final["data"].append(record)

            while True:
                if new_dict["pagination"]:
                    url_options["after"] = new_dict["pagination"]["cursor"]
                    new_dict = self.get_curl(url_input, url_options)[0]

                    for record in new_dict["data"]:
                        final["data"].append(record)
                else:
                    break

        slice_int = slice(-amount, None)

        new_final = { "data": final["data"][slice_int] }

        return new_final
