import re
from streamlink import Streamlink

        
class NexusStreamlinkInterface(object):

    def get_quality_options(self, channel):
        res_prog = re.compile(r"^\d+p(\d+)?$")
        url = "twitch.tv/%s" % channel

        session = Streamlink()
        url_obj = session.resolve_url(url, follow_redirect = False)

        if url_obj._channel == channel:
            streams = url_obj.streams()
            quality_list = [x for x in streams.keys() if res_prog.match(x)]

            if not quality_list:
                return None

            source_quality = quality_list[-1]

            if len(quality_list) == 1:
                quality_list = None

            return (source_quality, quality_list)

        return None
