import json
import requests
from urllib import request
from urllib import parse


class NexusUrbanInterface(object):

    def get_curl(self, url_input, url_options = None):
        if url_options is not None:
            url = "%s?%s" % (url_input, parse.urlencode(url_options))
        else:
            url = url_input

        request_headers = None

        if request_headers is not None:
            r = requests.get(url, headers = request_headers, allow_redirects = True)
        else:
            r = requests.get(url, allow_redirects = True)

        json_dict = r.json()
        json_final = json.dumps(json_dict, indent = 4, sort_keys = False)

        return [ json_dict, str(json_final) ]


    def get_definition_result(self, query):
        query = str(query).strip()

        url = "http://api.urbandictionary.com/v0/define?term=%s" % query

        return self.get_curl(url)
