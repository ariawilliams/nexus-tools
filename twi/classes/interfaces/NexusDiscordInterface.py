import json
import requests
import re
import base64
from urllib import request
from urllib import parse
from random import randint
from ..config.NexusConfig import NexusConfig
from .NexusDatabaseInterface import NexusDatabaseInterface


ndi = NexusDatabaseInterface()
nc = NexusConfig()
config = nc.retrieve_config()
token = None


class NexusDiscordInterface(object):

    def get_token(self, token = None):
        if token is not None and int(token["expires_in"]) < 60:
            return
        elif token is None or int(token["expires_in"]) > 60:
            data = {
                "grant_type": "client_credentials",
                "scope": "identify connections"
            }

            headers = {
                "Content-Type": "application/x-www-form-urlencoded"
            }

            r = requests.post("https://discordapp.com/api/v6/oauth2/token", data, headers, auth = (config["discord-client-id"], config["discord-client-secret"]))
            r.raise_for_status()

            token = r.json()
            return token


    def get_curl(self, url_input, url_options):
        global token
        if token is not None:
            access_token = token["access_token"]
        else:
            token = self.get_token()
            access_token = token["access_token"]

        if url_options is not None:
            url = "%s?%s" % (url_input, parse.urlencode(url_options))
        else:
            url = url_input

        request_headers = {
            "Authorization": "Bearer %s" % access_token
        }

        if request_headers is not None:
            r = requests.get(url, headers = request_headers)
        else:
            r = requests.get(url)

        json_dict = r.json()
        json_final = json.dumps(json_dict, indent = 4, sort_keys = False)

        return [ json_dict, str(json_final) ]


    def get_user_data(self, user_id):
        if user_id is not None:
            user_id = parse.quote(parse.unquote(user_id))
        else:
            return None

        url_options = None

        url_input = "https://discordapp.com/api/v6/users/%s" % user_id

        return self.get_curl(url_input, url_options)

