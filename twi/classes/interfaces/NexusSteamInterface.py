import json
import requests
import re
from urllib import request
from urllib import parse
from random import randint
from operator import itemgetter
from ..config.NexusConfig import NexusConfig


nc = NexusConfig()
config = nc.retrieve_config()

        
class NexusSteamInterface(object):

    def get_curl(self, url_input, url_options, client_id = None):
        if client_id is None:
            client_id = config["steam-client-id"]

        url_options["key"] = client_id
        url_options["format"] = "json"

        if url_options is not None:
            url = "%s?%s" % (url_input, parse.urlencode(url_options))
        else:
            url = url_input

        url = re.sub(r"/(appids_filter)\%5B(\d{1,5})\%5D(\=)/", "$1[$2]$3", url)

        request_headers = None

        if request_headers is not None:
            r = requests.get(url, headers = request_headers)
        else:
            r = requests.get(url)

        json_dict = r.json()
        json_final = json.dumps(json_dict, indent = 4, sort_keys = False)

        return [ json_dict, str(json_final) ]


    def get_steamuser_data(self, target):
        url_options = {
            "vanityurl": target
        }

        url_input = "http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/"

        return self.get_curl(url_input, url_options)[0]


    def get_steamuser_profile_data(self, target):
        target_steam_id = self.get_steamuser_data(target)["response"]["steamid"]

        url_options = {
            "steamids": target_steam_id
        }

        url_input = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/"

        return self.get_curl(url_input, url_options)[0]


    def get_usergame_data(self, appid, target):
        target_steam_id = self.get_steamuser_data(target)["response"]["steamid"]

        url_options = {
            "steamid": target_steam_id,
            str("appids_filter[0]"): appid
        }

        url_input = "http://api.steampowered.com/IPlayerService/GetOwnedGames/v1"

        return self.get_curl(url_input, url_options)[0]

