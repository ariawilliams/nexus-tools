import json
import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__)).replace(os.path.join("classes", "interfaces"), "")

class NexusDatabaseInterface(object):

    def get_json_data(self, file):
        try:
            with open(os.path.join(ROOT_DIR, "database", file), "r") as f:
                return json.loads(f.read().strip())
        except:
            return None


    def put_json_data(self, data, file):
        try:
            with open(os.path.join(ROOT_DIR, "database", file), "w+") as f:
                json_data = json.dumps(data, separators = (",", ":"),
                    sort_keys = False)
                f.write(json_data)
            return "Success"
        except:
            return None


    def append_json_data(self, data, file):
        try:
            with open(os.path.join(ROOT_DIR, "database", file), "a+") as f:
                json_data = json.dumps(data, separators = (",", ":"),
                    sort_keys = False)
                f.write(json_data)
            return "Success"
        except:
            return None


    def get_text_data(self, file):
        try:
            with open(os.path.join(ROOT_DIR, "database", file), "r") as f:
                return f.read().strip()
        except:
            return None


    def put_text_data(self, data, file):
        try:
            with open(os.path.join(ROOT_DIR, "database", file), "w+") as f:
                f.write(str(data))
            return "Success"
        except:
            return None


    def append_text_data(self, data, file):
        try:
            with open(os.path.join(ROOT_DIR, "database", file), "a+") as f:
                f.write(str(data))
            return "Success"
        except:
            return None


    def retrieve_oauth(self, user):
        try:
            with open(os.path.join(ROOT_DIR, "database", "oauth",
                    "%s_oauth.json" % user), "r") as f:
                return json.loads(f.read().strip())
        except:
            return None


    def put_oauth(self, data, user):
        try:
            with open(os.path.join(ROOT_DIR, "database", "oauth",
                    "%s_oauth.json" % user), "w+") as f:
                json_data = json.dumps(data, separators = (",", ":"),
                    sort_keys = False)
                f.write(json_data)
            return "Success"
        except:
            return None
