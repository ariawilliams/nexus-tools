import json
import requests
from urllib import request
from urllib import parse


class NexusStrawpollInterface(object):

    def get_curl(self, url_input, url_options):
        if url_options is not None:
            url = "%s?%s" % (url_input, parse.urlencode(url_options))
        else:
            url = url_input

        request_headers = None

        if request_headers is not None:
            r = requests.get(url, headers = request_headers, allow_redirects = True)
        else:
            r = requests.get(url, allow_redirects = True)

        json_dict = r.json()
        json_final = json.dumps(json_dict, indent = 4, sort_keys = False)

        return [ json_dict, str(json_final) ]


    def post_curl(self, url_input, url_options, data_dict):
        if url_options is not None:
            url = "%s?%s" % (url_input, parse.urlencode(url_options))
        else:
            url = url_input

        data_json = json.dumps(data_dict)

        request_headers = {
            "Content-Type": "application/json",
            "Content-Length": str(len(data_json))
        }
        
        if request_headers is not None:
            r = requests.post(url, data = data_json, headers = request_headers, allow_redirects = True)
        else:
            r = requests.post(url, data = data_json, allow_redirects = True)

        json_dict = r.json()
        json_final = json.dumps(json_dict, indent = 4, sort_keys = False)

        return [ json_dict, str(json_final) ]


    def parse_poll_message(self, message):
        poll_list = message.split(" | ")
        poll_options = []

        for i, option in enumerate(poll_list):
            if i == 0:
                continue
            poll_options.append(option)

        poll_final = {
            "title": poll_list[0],
            "options": poll_options,
            "multi": "false"
        }

        return poll_final


    def get_poll(self, poll_id):
        url_input = "https://strawpoll.me/api/v2/polls/" + poll_id
        url_options = None
        json_dict = self.get_curl(url_input, url_options)

        return json_dict


    def post_new_poll(self, message):
        poll_dict = self.parse_poll_message(message)
        url_input = "https://strawpoll.me/api/v2/polls"

        url_options = None
        json_dict = self.post_curl(url_input, url_options, poll_dict)

        return json_dict[0]
