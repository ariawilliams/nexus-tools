import json
import requests
from urllib import request
from urllib import parse
from random import randint
from ..config.NexusConfig import NexusConfig


nc = NexusConfig()
config = nc.retrieve_config()

        
class NexusYoutubeInterface(object):

    def get_curl(self, url_input, url_options, client_id = None):
        if client_id is None:
            client_id = config["youtube-client-id"]

        url_options["key"] = client_id

        if url_options is not None:
            url = "%s?%s" % (url_input, parse.urlencode(url_options))
        else:
            url = url_input

        request_headers = None

        if request_headers is not None:
            r = requests.get(url, headers = request_headers)
        else:
            r = requests.get(url)

        json_dict = r.json()
        json_final = json.dumps(json_dict, indent = 4, sort_keys = False)

        return [ json_dict, str(json_final) ]


    def get_video_search(self, query):
        query = parse.unquote(query)

        url_options = {
            "part": "snippet",
            "q": query,
            "type": "video"
        }

        url_input = "https://www.googleapis.com/youtube/v3/search"

        return self.get_curl(url_input, url_options)
