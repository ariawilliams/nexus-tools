from ..config.NexusConfig import NexusConfig
from datetime import datetime
from datetime import timedelta
from pycountry import countries
from urllib import parse
import json
import requests


nc = NexusConfig()
config = nc.retrieve_config()


class NexusQueryInterface(object):

    def get_curl(self, url_input, url_options):
        if url_options is not None:
            url = "%s?%s" % (url_input, parse.urlencode(url_options))
        else:
            url = url_input

        request_headers = {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }

        if request_headers is not None:
            r = requests.get(url, headers = request_headers, allow_redirects = True)
        else:
            r = requests.get(url, allow_redirects = True)

        json_dict = r.json()
        json_final = json.dumps(json_dict, indent = 4, sort_keys = False)

        return [ json_dict, str(json_final) ]


    def get_google_spellcheck(self, query):
        query_str = str(query).strip()
        query_str = str(query).lower()

        url_input = "https://www.google.com/complete/search"
        url_options = {
            "output": "firefox",
            "q": query_str
        }

        json_dict = self.get_curl(url_input, url_options)

        res_dict = json_dict[0]

        search_term = res_dict[0]
        suggestions = res_dict[1]

        if not suggestions:
            return None
        elif search_term in suggestions:
            return search_term
        else:
            return suggestions[0].split(" ", 1)[0]


    def get_openweather_result(self, query):
        query_str = str(query).strip()
        query_str = str(query).lower()

        url_input = "https://api.openweathermap.org/data/2.5/weather"
        url_options = {
            "q": query_str,
            "units": "metric",
            "appid": config["openweather-api-key"]
        }

        json_dict = self.get_curl(url_input, url_options)

        res = json_dict[0]

        try:
            if res["cod"] == 200:
                country_name = countries.get(
                    alpha_2 = res["sys"]["country"]).name
                if country_name.lower() != res["name"].lower():
                    location_str = "%s, %s" % (res["name"], country_name)
                else:
                    location_str = res["name"]
                return {
                    "location": location_str,
                    "temp_type": "C",
                    "current_temp": "%s" % round(res["main"]["temp"]),
                    "high_temp": "%s" % round(res["main"]["temp_max"]),
                    "low_temp": "%s" % round(res["main"]["temp_min"]),
                    "status": res["weather"][0]["description"],
                    "humidity": "%s%%" % res["main"]["humidity"]
                }
            else:
                return None
        except KeyError:
            return None


    def get_openweathertime_result(self, query):
        query_str = str(query).strip()
        query_str = str(query).lower()

        url_input = "https://api.openweathermap.org/data/2.5/weather"
        url_options = {
            "q": query_str,
            "units": "metric",
            "appid": config["openweather-api-key"]
        }

        json_dict = self.get_curl(url_input, url_options)

        res = json_dict[0]

        try:
            if res["cod"] == 200:
                country_name = countries.get(
                    alpha_2 = res["sys"]["country"]).name
                if country_name.lower() != res["name"].lower():
                    location_str = "%s, %s" % (res["name"], country_name)
                else:
                    location_str = res["name"]
                if res["timezone"] == 0:
                    utc_offset = 0
                else:
                    utc_offset = res["timezone"] / 60 / 60
                    utc_now = datetime.utcnow()

                    if utc_offset == 0:
                        local_now = utc_now
                        timezone_str = "UTC +0"
                    else:
                        offset_dir = utc_offset > 0

                        os_split = divmod(abs(utc_offset), 1)
                        os_split = [
                            int(os_split[0]),
                            int(os_split[1] * 100)
                        ]

                        local_offset = divmod(
                            int(abs(utc_offset) * 60),
                            60
                        )

                        if offset_dir:
                            timezone_str = "UTC +%d" % os_split[0]
                            if os_split[1] != 0:
                                timezone_str += ":%d" % os_split[1]

                            local_now = utc_now + timedelta(
                                minutes = local_offset[1],
                                hours = local_offset[0]
                            )
                        else:
                            timezone_str = "UTC -%d" % os_split[0]
                            if os_split[1] != 0:
                                timezone_str += ":%d" % os_split[1]
                            local_now = utc_now - timedelta(
                                minutes = local_offset[1],
                                hours = local_offset[0]
                            )

                return {
                    "time": local_now.strftime("%H:%M"),
                    "date": local_now.strftime("%A, %-d %B, %Y"),
                    "timezone": timezone_str,
                    "location": location_str
                }
            else:
                return None
        except KeyError:
            return None
