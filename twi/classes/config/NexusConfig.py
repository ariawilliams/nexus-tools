import json
import os


class NexusConfig(object):

    def retrieve_config(self):
        config_file = os.path.join(os.path.dirname(__file__), "config.json")
        with open(config_file) as json_data:
            data = json.load(json_data)
            return data


# Create config.json file under this directory with the following template

# {
#     "twitch-client-id": "",
#     "youtube-client-id": "",
#     "steam-client-id": ""
# }