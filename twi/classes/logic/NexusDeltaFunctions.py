from ..interfaces.NexusTwitchInterface import NexusTwitchInterface
from random import randint
from random_user_agent.params import HardwareType as hwt
from random_user_agent.params import OperatingSystem as ops
from random_user_agent.params import Popularity as ppl
from random_user_agent.params import SoftwareEngine as swe
from random_user_agent.params import SoftwareName as swn
from random_user_agent.params import SoftwareType as swt
from random_user_agent.user_agent import UserAgent
from string import whitespace
import json


nti = NexusTwitchInterface()

class NexusDeltaFunctions(object):

    def ucwords(self, source):
        uc_source = ""
        for idx, a_char in enumerate(source):
            if a_char in whitespace or not a_char.isalpha():
                uc_source += a_char
            elif a_char.isalpha() and (not idx or source[idx - 1] in whitespace):
                uc_source += a_char.upper()
            else:
                uc_source += a_char
        return uc_source


    def parse_notes_message(self, message):
        return message.split(" | ")


    def channel_exists(self, channel, flag):
        if flag == "data":
            try:
                return nti.get_channel_data(channel)[0]
            except TypeError:
                return None
        elif flag == "id":
            try:
                return nti.get_channel_id(channel)[0]
            except TypeError:
                return None
        else:
            return None


    def get_random_ua(self):
        user_agent_rotator = UserAgent(
            hardware_types = [
                hwt.COMPUTER.value
            ],
            software_types = [
                swt.WEB_BROWSER.value
            ],
            software_names = [
                swn.CHROME.value,
                swn.OPERA.value
            ],
            operating_systems = [
                ops.MAC_OS_X.value,
                ops.LINUX.value
            ],
            popularity = [
                ppl.POPULAR.value,
                ppl.COMMON.value
            ],
            limit = 100
        )

        return user_agent_rotator.get_random_user_agent()


    def get_random_ua_static(self):
        x = randint(0, 29)
        ua_list = [
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/42.0.2311.90 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/45.0.2454.101 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/46.0.2490.86 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/48.0.2564.116 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/53.0.2785.116 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/53.0.2785.143 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/81.0.4044.138 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) " \
                "AppleWebKit/537.17 (KHTML, like Gecko) " \
                "Chrome/24.0.1312.56 Safari/537.17",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/39.0.2171.95 Safari/537.36",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/41.0.2272.104 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/54.0.2840.99 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/51.0.2704.103 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/46.0.2490.86 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/49.0.2623.112 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; Win64; x64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/51.0.2704.106 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; Win64; x64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/54.0.2840.59 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/46.0.2490.80 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/47.0.2526.80 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/51.0.2704.106 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/51.0.2704.63 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/52.0.2743.82 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/55.0.2883.87 Safari/537.36",
            "Mozilla/5.0 (Windows NT 6.3; WOW64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/47.0.2526.106 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64) " \
                "AppleWebKit/534.24 (KHTML, like Gecko) " \
                "Chrome/11.0.696.34 Safari/534.24",
            "Mozilla/5.0 (X11; Linux x86_64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/28.0.1500.71 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/43.0.2357.134 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/44.0.2403.157 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/49.0.2623.87 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/51.0.2704.0 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64) " \
                "AppleWebKit/537.36 (KHTML, like Gecko) " \
                "Chrome/55.0.2883.44 Safari/537.36"
        ]
        try:
            return ua_list[x]
        except IndexError:
            return ua_list[7]
