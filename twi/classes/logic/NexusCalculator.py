from datetime import datetime
from dateutil.relativedelta import relativedelta
import json

class NexusCalculator(object):
    def compute_time_interval(self, a, b, acc = 9):
        diff = relativedelta(a, b)

        decades_years = divmod(diff.years, 10)

        weeks_days = divmod(diff.days, 7)

        keys = [
            "decades",
            "years",
            "months",
            "weeks",
            "days",
            "hours",
            "minutes",
            "seconds",
            "milliseconds",
        ]
        vals = [
            decades_years[0],
            decades_years[1],
            diff.months,
            weeks_days[0],
            weeks_days[1],
            diff.hours,
            diff.minutes,
            diff.seconds,
            int(diff.microseconds / 1000),
        ]

        times = []; i = 0
        for interval, value in zip(keys, vals):
            if i >= acc:
                break
            if value is 0:
                continue
            elif value is 1:
                times.append(" ".join([str(value), interval[:-1]]))
            else:
                times.append(" ".join([str(value), interval]))
            i += 1

        if len(times) is 1:
            return times[0]
        elif len(times) > 1:
            return "%s and %s" % (", ".join(times[:-1]), times[-1])


    def convert_to_fahrenheit(self, c):
        return (c * 9/5) + 32


    def convert_to_celsius(self, f):
        return (f - 32) / 1.8
