weather_lists = {
    "cold_places_list": [
        "antarctica", "south pole", "north pole"
    ],
    "days_list": [
        "tomorrow", "today", "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"
    ],
    "planets_list": [
        "mercury", "mars", "jupiter", "saturn", "neptune", "the milky way", "universe", "the universe", "moon", "the moon"
    ],
    "star_wars_list": [
        "ahch-to", "alderaan", "anoat", "atollon", "bespin",
        "cato neimoidia", "christophsis", "concord dawn", "corellia", "coruscant",
        "crait", "d'qar", "dagobah", "dantooine", "dathomir",
        "devaron", "eadu", "endor", "felucia", "geonosis",
        "hosnian prime", "hoth", "iego", "ilum", "iridonia",
        "jakku", "jedha", "kamino", "kashyyyk", "kessel",
        "lah'mu", "lothal,", "malachor", "malastare", "mandalore",
        "maridun", "mon calamari", "moraband", "mortis", "mustafar",
        "mygeeto", "naboo", "nal hutta", "onderon", "ord mantell",
        "polis massa", "rishi", "rodia", "ruusan", "ryloth",
        "scarif", "shili", "starkiller base", "subterrel", "sullust",
        "takodana", "tatooine", "toydaria", "trandosha", "umbara",
        "utapau", "wobani", "yavin", "yavin 4",
    ],
    "middle_earth_list": [
        "aldburg", "alqualondë", "andúnië", "annúminas", "archet", "armenelos", "avallonë", "barad-dûr", "belegost", "bree",
        "brockenborings", "brithombar", "bywater", "calembel", "caras galadhon", "carn dûm", "combe", "crickhollow", "dale", "dol amroth",
        "dunharrow", "dunlan", "dol guldur", "edhellond", "edoras", "eldalondë", "eglarest", "ephel brandir", "erebor", "erech",
        "esgaroth", "ethring", "forlond", "formenos", "fornost erain", "frogmorton", "goblin-town", "gondolin", "grey havens", "harlond",
        "havens of the falas", "haysend", "helm's deep", "hobbiton", "isengard", "khazad-dûm", "kortirion", "linhir", "lond daer enedh", "lorien",
        "menegroth", "michel delving", "minas anor", "minas ithil", "minas morgul", "minas tirith", "mithlond", "moria", "nargothrond", "needlehole",
        "newbury", "nindamos", "nobottle", "nogrod", "oatbarton", "ondosto", "osgiliath", "ost-in-edhil", "pelargir", "pincup",
        "rivendell", "rómenna", "rushey", "scary", "staddle", "standelf", "stock", "tharbad", "tirion", "tuckborough",
        "umbar", "upbourn", "utumno", "valmar", "vinyamar", "waymoot", "wilderfoot",
    ],
    "mass_effect_list": [
        "2175 aeia", "2175 ar2", "2181 arion", "2181 despoina", "2181 eubolos", "acaeria", "adas", "adek", "aegis", "aequitas",
        "aganju", "agebinium", "ageko", "agessia", "agetoton", "agnin", "aigela", "aitarus", "aite", "aitis",
        "akraia", "alchera", "alcyoneus", "alformus", "alingon", "alko", "alkonost", "allusah", "almarcrux", "almos",
        "alrumter", "alsages", "altaaya", "altahe", "altakiril", "altanorch", "amaranthine", "ammut", "anedia", "anhur",
        "anjea", "ansuz", "antibaar", "antictra", "antida", "antigar", "antinax", "antiroprus", "antirumgon", "antitarra",
        "aphras", "apo", "aratoht", "arcadia", "archanes", "arcturus station", "aria's fleet", "armeni", "arvuna", "asteria",
        "asteroid x57", "atahil", "atebolos", "athame", "atos irn", "aventen", "azimir", "azrahas", "bannik", "bast",
        "bastzuda", "beach thunder", "bekenstein", "benda", "beness", "benning", "beregale", "beyalt", "bindur", "binthu",
        "boro", "borr", "bothros", "bovis tor", "bres", "caelax", "caleston", "camala", "camaron", "canalus",
        "canctra", "canrum", "capek", "carborix", "carcosa", "casbin", "cernunnos", "chalkhos", "charoum", "chasca",
        "cherk sab", "chofen", "chohe", "choitadix", "cholis", "circe", "citadel", "clobaka", "clocrolis", "clogon",
        "clojiia", "clomarthu", "cloroplon", "clotanca", "clugon", "corang", "crick", "cronos station", "cyllene", "cyone",
        "damkianna", "daratar", "darwin", "datriux", "dekuuna", "derelict reaper", "derneuca", "dezda", "digeris", "diplomatic ships",
        "dobrovolski", "doldit", "dor", "dorgal", "doriae", "doz atab", "dragel", "dranen", "dregir", "dumah",
        "durak", "echidna", "eden prime", "edmos", "edolus", "egalic", "ehstag", "eingana", "eirene", "ekuna",
        "elatha", "eletania", "elohi", "elysium", "epho", "erinle", "erros", "erszbat", "essenus", "etamis",
        "eunomia", "euntanta", "ezka", "farcrothu", "fargeluse", "faringor", "farlas", "farnuri", "farthorl", "fermi",
        "feros", "feynman", "fiax", "first land", "fitful current", "flett", "franklin", "fuel depot", "gaelon", "gamayun",
        "garan", "garvug", "gei hinnom", "gellix", "geth debris field", "geth dreadnought", "geus", "gilead", "goliath", "gorgun",
        "gotha", "gregas", "gremar", "grissom academy", "gromar", "grosalgen", "haestrom", "hagalaz", "halegeuse", "hali",
        "hanalei", "haza", "hebat", "helyme", "heretic station", "hesano", "heshtok", "hiba", "hito", "horizon",
        "hunidor", "huningto", "hunsalra", "hyetiana", "ilem", "illapa", "illium", "ilmnos", "ilos", "imaen",
        "imaneya", "imorkan", "impera", "inakhos", "intai'sei", "inti", "invictus", "iritum", "irune", "isa",
        "isale", "ishassara", "island wind", "israfil", "ithrone", "jak ser", "janiri", "jarfor", "jarrahe station", "jartar",
        "joab", "jontan", "jonus", "joppa", "juncro", "juntauma", "junthor", "kaddi", "kailo", "kakabel",
        "kanin", "karora", "karumto", "kashshaptu", "katebolo", "kaushus", "kaver station", "keimowitz", "kelim", "kenaz",
        "keph", "ker", "khar'shan", "klencory", "klendagon", "klensal", "klos", "kobayashi", "komarov", "kopis",
        "korar", "korlus", "kralla", "kruban", "kurinth", "laban", "laconix", "laena", "lahu", "lattesh",
        "lemnia", "lenuamund", "lepini", "lesuss", "lethe", "lihrat", "linossa", "lisir", "listening post x-19", "locil",
        "logan", "logasiri", "loki", "lorek", "loxia", "luna", "lusia", "lymetis", "maganlis", "mahavid",
        "maidla", "maisuth", "maitrum", "maji", "makhaira", "maklan", "maldor", "mantun", "maskawa", "mass relay",
        "matar", "matol", "mavigon", "mawinor", "medokos", "melile", "menae", "metaponto", "metgos", "migrant fleet",
        "mingito", "mizraim", "mnemosyne", "mola", "morana", "moros", "msv broken arrow", "msv cornucopia", "msv fedele", "msv majesty",
        "msv ontario", "msv strontium mule", "msv worthington", "murky water", "mylasi", "nalisin", "namakli", "nambu", "naskral", "nataisa",
        "nausicaa", "nauti", "naxell", "neargas", "nearog", "nearrum", "neidus", "neith", "nemata", "nepheron",
        "nephros", "nepmos", "nepneu", "nepyma", "nevos", "niacal", "niagolon", "nios", "nirrus", "nirvana",
        "nodacrux", "nonuel", "norehsa", "nossia", "notanban", "noveria", "nutus", "odasst", "olokun", "oltan",
        "oma ker", "omega", "omega 4 relay", "ontaheter", "ontamalca", "ontarom", "orunmila", "osalri", "paeto", "pahhur",
        "palaven", "pania", "paphos", "parag", "parasc", "paravin", "parnassus", "partholon", "pataiton", "patajiri",
        "patamalrus", "patashi", "patatanlis", "patavig", "patsayev", "pauling", "phaistos", "pharos", "pheiros", "phoros",
        "piares", "pietas", "pinnacle station", "ploba", "pluto", "polissa", "pollino", "poloh tem", "pomal", "ponolus",
        "ponos", "porolan", "pragia", "pregel", "prescyla", "presrop", "pressha", "preying mouth", "promavess", "pronoia",
        "prospect", "proteus", "purgatory", "quaji", "quana", "quarem", "quarian envoy ship", "quirezia", "quodis", "raisaris",
        "ramlat", "rannoch", "rayingri", "raysha", "renshato", "rescel", "rihali", "rilar", "rotesk", "rothla",
        "rough tide", "ruam", "rustaka", "sakata", "salamis", "saleas", "sanctum", "sangel", "sanves", "saradril",
        "sarait", "sarapai", "sazgoth", "sehtor", "sekhmet", "selvos", "serao", "sesmose", "sharblu", "sharjila",
        "sharring", "shastessia", "shasu", "shir", "siano", "sidon", "silva", "sineus", "sinmara", "slekon",
        "sobek", "sogelrus", "solcrum", "solmarlon", "solu paolis", "sonedma", "sotera", "spekilas", "sthenia", "suen",
        "supay", "sur'kesh", "surtur", "svarog", "syba", "sybin", "syided", "sylsalto", "synalus", "sytau",
        "taitus", "takkan", "talaria", "talis fia", "tamahera", "tamgauta", "tarith", "tefnut", "telluune", "temerarus",
        "tenoth", "terapso", "terra nova", "teshub", "teukria", "tevura", "thail", "tharopto", "thegan", "thegeuse",
        "themis", "thenusi", "theonax", "therum", "therumlon", "thesalgon", "theshaca", "thessia", "theyar", "thissioni",
        "thooft", "thrivaldi", "thunawanuro", "thurisaz", "tosal nym", "trategos", "treagir", "trebin", "trelyn", "tremanre",
        "tremar", "treyarmus", "trident", "trigestis", "triginta petra", "trikalon", "triodia", "tritogenith", "tuchanka", "tula",
        "tunfigel", "tungel", "tunshagon", "tuntau", "tyr", "tyre", "tyrix", "urdak", "uriyah", "urmola",
        "uruz", "utha", "utukku", "uwan oche", "uza", "uzin", "vana", "vard", "varmalus", "vatar",
        "vaul", "vebinok", "vecchio", "vectra", "veles", "veltman", "vem osca", "vemal", "venture", "venus",
        "verush", "veyaria", "viantel", "vioresa", "vir", "virits", "virmire", "volkov", "volturno", "vylius",
        "wallace", "watchman", "watson", "welm urun", "wenrum", "wentania", "wermani", "wheeler", "wreckage", "wrill",
        "wuo", "xamarri", "xanadu", "xathorron", "xawin", "xerceo", "xetic", "yamm", "yan tao", "yasilium",
        "yukawa", "yunaca", "yunthorl", "zada ban", "zafe", "zaherux", "zakros", "zanethu", "zatorus", "zayarter",
        "zeona", "zesmeni", "zeth", "zion", "zirnitra", "zorya", "zosteros", "zylium",
    ],
    "world_list": [ "worldoftomorrow", "world of tomorrow" ]
}

system_dicts = {
    "steam": {
        "str": "Steam",
        "inputs" : [ "steam", "valve" ]
    },
    "origin": {
        "str": "Origin",
        "inputs": [ "origin", "ea", "not steam" ]
    },
    "win10": {
        "str": "Windows 10",
        "inputs": [ "windows 10", "win 10", "win10", "w10" ]
    },
    "pc": {
        "str": "the PC",
        "inputs": [ "pc", "computer", "pooter", "puter", "master race", "masterrace" ]
    },
    "xb1": {
        "str": "the Xbox One",
        "inputs": [ "xbox one", "xboxone", "xbone", "xbox 1", "xbox1", "xb1" ]
    },
    "ps3": {
        "str": "the PlayStation 3",
        "inputs": [ "playstation 3", "playstation3", "ps3" ]
    },
    "ps4": {
        "str": "the PlayStation 4",
        "inputs": [ "playstation 4", "playstation4", "ps4" ]
    },
    "wiiu": {
        "str": "the Nintendo Wii U",
        "inputs": [ "wii u", "wiiu" ]
    },
    "3ds": {
        "str": "the Nintendo 3DS",
        "inputs": [ "n3ds", "3ds", "nintendo 3ds" ]
    },
    "switch": {
        "str": "the Nintendo Switch",
        "inputs": [ "switch", "nintendo switch" ]
    },
    "linux": {
        "str": "Ubuntu Linux 16.04 LTS",
        "inputs": [ "development", "linux", "dev", "coding" ]
    }
}

