from classes.core.NexusCore import NexusCore
from urllib import parse
from flask import Flask
from flask import request
from flask import send_from_directory
from werkzeug.contrib.fixers import ProxyFix
import os
import json


ncr = NexusCore()

app = Flask(__name__)


@app.errorhandler(404)
def page_not_found(e):
    print(e)
    return ""


@app.errorhandler(500)
def internal_server_error(e):
    print(e)
    return "I'm terribly sorry, something went wrong in my brain! " \
        "Please try again."


@app.route("/")
def main():
    return "Hi!"

# OAuth Routes
@app.route("/oauth/all")
def oauth_all():
    return ncr.render_twitch_oauth("all")

@app.route("/oauth/mod")
def oauth_mod():
    return ncr.render_twitch_oauth("mod")

@app.route("/oauth/chat")
def oauth():
    return ncr.render_twitch_oauth("chat")

@app.route("/authorize")
def authorize():
    try:
        oauth_dict = ncr.get_twitch_oauth(request.args.get("code"))
        output_dict = {
            "access_token": oauth_dict["access_token"],
            "refresh_token": oauth_dict["refresh_token"]
        }
        return "<pre>%s</pre>" % json.dumps(output_dict, indent = 4,
            sort_keys = False)
    except:
        return "Something went wrong, please try again!"

# Serve Favicon
@app.route("/favicon.ico")
def favicon():
    return send_from_directory(os.path.join(app.root_path),
        "favicon.ico", mimetype = "image/vnd.microsoft.icon")

@app.route("/html/<path:path>")
def send_html_files(path):
    return send_from_directory("html", path)

# Testing routes
@app.route("/googlejson/<query>/")
def googlejson(query):
    return ncr.get_googlejson_string(query)


@app.route("/youtubejson/<query>/")
def youtubejson(query):
    return ncr.get_youtubejson_string(query)


@app.route("/steamjson/<query>/")
def steamjson(query):
    return ncr.get_steamjson_string(query)


@app.route("/weatherjson/<query>/")
def weatherjson(query):
    return ncr.get_weatherjson_string(query)


@app.route("/definejson/<query>/")
def definejson(query):
    return ncr.get_definejson_string(query)


@app.route("/currencyjson/<query>/")
def currencyjson(query):
    return ncr.get_currencyjson_string(query)


@app.route("/timejson/<query>/")
def timejson(query):
    return ncr.get_timejson_string(query)


@app.route("/polljson/<query>/")
def polljson(query):
    return ncr.get_polljson_string(query)


@app.route("/vodjson/<channel>/")
def vodjson(channel):
    return ncr.get_vodjson_string(channel)


@app.route("/channeljson/<channel>/")
def channeljson(channel):
    return ncr.get_channeljson_string(channel)


@app.route("/streamjson/<channel>/")
def streamjson(channel):
    return ncr.get_streamjson_string(channel)


@app.route("/gamesjson/<query>/")
def gamesjson(query):
    return ncr.get_gamesjson_string(query)


@app.route("/urbanjson/<query>/")
def urbanjson(query):
    return ncr.get_urbanjson_string(query)


@app.route("/discordjson/<user_id>/")
def discordjson(user_id):
    return ncr.get_discordjson_string(user_id)


@app.route("/randomjson/")
def randomjson():
    return ncr.get_randomstream_string()



@app.route("/displayname/<user>/")
def displayname(user):
    return ncr.get_display_name_string(user)


@app.route("/interval/")
def interval():
    return ncr.get_interval_string()


# Main routes
@app.route("/transcoding/<host>/", defaults = {"channel": None})
@app.route("/transcoding/<host>/<channel>")
def transcoding(host, channel = None):
    return ncr.get_transcoding_string(host, channel)


@app.route("/caster/<host>/<game>/", defaults = {"channel": None})
@app.route("/caster/<host>/<game>/<channel>")
def caster(host, game, channel = None):
    return ncr.get_caster_string(host, game, channel)


@app.route("/countgames/<nickname>/<host>/<channel>/",
    defaults = {"query": None})
@app.route("/countgames/<nickname>/<host>/<channel>/<query>")
def countgames(channel, host, nickname, query = None):
    return ncr.get_countgames_string(channel, host, nickname, query)


@app.route("/currentgame/<nickname>/<channel>/")
def currentgame(channel, nickname):
    return ncr.get_currentgame_string(channel, nickname)


@app.route("/downtime/<nickname>/<host>/<game>/",
    defaults = {"channel": None})
@app.route("/downtime/<nickname>/<host>/<game>/<channel>")
def downtime(host, game, nickname, channel = None):
    return ncr.get_downtime_string(host, game, nickname, channel)


@app.route("/paxaus/countdown/")
def paxaus():
    return ncr.get_paxaus_string()


@app.route("/poll/<user>/<channel>/")
def poll(user, channel):
    return ncr.get_poll_string(user, channel)


@app.route("/clearpoll/<user>/<channel>/")
def clearpoll(user, channel):
    return ncr.clear_current_poll(user, channel)


@app.route("/newpoll/<user>/<channel>/", defaults = {"message": None})
@app.route("/newpoll/<user>/<channel>/<message>")
def newpoll(user, channel, message = None):
    if request.query_string:
        query_str = str(request.query_string).replace("b'", "").replace(
            "b\"", "").rstrip("\'").rstrip("\"")
        if message is not None:
            message = "%s?%s" % (message, query_str)
            message = message.replace("#", "%23")
    return ncr.get_poll_post(user, channel, message)


@app.route("/twitchcon/countdown/")
def twitchcon():
    return ncr.get_twitchcon_string()


@app.route("/uptime/<nickname>/<host>/<game>/", defaults = {"channel": None})
@app.route("/uptime/<nickname>/<host>/<game>/<channel>")
def uptime(host, game, nickname, channel = None):
    return ncr.get_uptime_string(host, game, nickname, channel)


@app.route("/created/<user>/", defaults = {"channel": None})
@app.route("/created/<user>/<channel>")
def created(user, channel = None):
    return ncr.get_created_string(user, channel)


@app.route("/following/<nickname>/<channel>/<user>/",
    defaults = {"follower": None})
@app.route("/following/<nickname>/<channel>/<user>/<follower>")
def following(nickname, channel, user, follower = None):
    return ncr.get_following_string(nickname, channel, user, follower)


@app.route("/firstfollowers/<nickname>/<host>/<amount>/",
    defaults = {"channel": None})
@app.route("/firstfollowers/<nickname>/<host>/<amount>/<channel>")
def firstfollowers(host, nickname, amount, channel = None):
    return ncr.get_firstfollowers_string(host, nickname, amount, channel)


@app.route("/recentfollowers/<nickname>/<host>/<amount>/",
    defaults = {"channel": None})
@app.route("/recentfollowers/<nickname>/<host>/<amount>/<channel>")
def recentfollowers(host, nickname, amount, channel = None):
    return ncr.get_recentfollowers_string(host, nickname, amount,
        channel)


@app.route("/banfollowers/<amount>/", defaults = {"channel": None})
@app.route("/banfollowers/<amount>/<channel>")
def banfollowers(amount, channel = None):
    return ncr.get_recentfollowers_banlist(amount, channel)


@app.route("/weeb/<user>/", defaults = {"query": None})
@app.route("/weeb/<user>/<query>")
def weeb(user, query = None):
    if request.query_string:
        query_str = str(request.query_string).replace("b'", "").replace(
            "b\"", "").rstrip("\'").rstrip("\"")
        if query is not None:
            query = "%s?%s" % (query, query_str)
    return ncr.get_weeb_string(user, query)


@app.route("/love/<user>/<botname>/", defaults = {"query": None})
@app.route("/love/<user>/<botname>/<query>")
def love(user, botname, query = None):
    if request.query_string:
        query_str = str(request.query_string).replace("b'", "").replace(
            "b\"", "").rstrip("\'").rstrip("\"")
        if query is not None:
            query = "%s?%s" % (query, query_str)
    return ncr.get_lovetester_string(user, botname, query)


@app.route("/weather/<user>/", defaults = {"query": None})
@app.route("/weather/<user>/<query>")
def weather(user, query = None):
    if request.query_string:
        query_str = str(request.query_string).replace("b'", "").replace(
            "b\"", "").rstrip("\'").rstrip("\"")
        if query is not None:
            query = "%s?%s" % (query, query_str)
    return ncr.get_googleweather_string(user, query)


@app.route("/google/<user>/", defaults = {"query": None})
@app.route("/google/<user>/<query>")
def google(user, query = None):
    if request.query_string:
        query_str = str(request.query_string).replace("b'", "").replace(
            "b\"", "").rstrip("\'").rstrip("\"")
        if query is not None:
            query = "%s?%s" % (query, query_str)
    return ncr.get_googlesearch_string(user, query)


@app.route("/ytsearch/<user>/", defaults = {"query": None})
@app.route("/ytsearch/<user>/<query>")
def ytsearch(user, query = None):
    if request.query_string:
        query_str = str(request.query_string).replace("b'", "").replace(
            "b\"", "").rstrip("\'").rstrip("\"")
        if query is not None:
            query = "%s?%s" % (query, query_str)
    return ncr.get_youtubesearch_string(user, query)


@app.route("/define/<user>/", defaults = {"query": None})
@app.route("/define/<user>/<query>")
def define(user, query = None):
    if request.query_string:
        query_str = str(request.query_string).replace("b'", "").replace(
            "b\"", "").rstrip("\'").rstrip("\"")
        if query is not None:
            query = "%s?%s" % (query, query_str)
    return ncr.get_googledefine_string(user, query, False)
        # Third argument displays maintenance message


@app.route("/time/<user>/", defaults = {"query": None})
@app.route("/time/<user>/<query>")
def time(user, query = None):
    if request.query_string:
        query_str = str(request.query_string).replace("b'", "").replace(
            "b\"", "").rstrip("\'").rstrip("\"")
        if query is not None:
            query = "%s?%s" % (query, query_str)
    return ncr.get_googletime_string(user, query)


@app.route("/spotify/<user>/", defaults = {"query": None})
@app.route("/spotify/<user>/<query>")
def spotify(user, query = None):
    if request.query_string:
        query_str = str(request.query_string).replace("b'", "").replace(
            "b\"", "").rstrip("\'").rstrip("\"")
        if query is not None:
            query = "%s?%s" % (query, query_str)
    return ncr.get_googlespotify_string(user, query)


@app.route("/currency/<user>/", defaults = {"query": None})
@app.route("/currency/<user>/<query>")
def currency(user, query = None):
    if request.query_string:
        query_str = str(request.query_string).replace("b'", "").replace(
            "b\"", "").rstrip("\'").rstrip("\"")
        if query is not None:
            query = "%s?%s" % (query, query_str)
    return ncr.get_googlecurrency_string(user, query)


@app.route("/urban/<user>/", defaults = {"query": None})
@app.route("/urban/<user>/<query>")
def urban(user, query = None):
    if request.query_string:
        query_str = str(request.query_string).replace("b'", "").replace(
            "b\"", "").rstrip("\'").rstrip("\"")
        if query is not None:
            query = "%s?%s" % (query, query_str)
    return ncr.get_urbandefine_string(user, query)


@app.route("/svnotes/<user>/<channel>/", defaults = {"message": None})
@app.route("/svnotes/<user>/<channel>/<message>")
def svnotes(user, channel, message = None):
    if request.query_string:
        query_str = str(request.query_string).replace("b'", "").replace(
            "b\"", "").rstrip("\'").rstrip("\"")
        if message is not None:
            message = "%s?%s" % (message, query_str)
    return ncr.get_svnotes(user, channel, message)


@app.route("/sv/", defaults = {"channel": None})
@app.route("/sv/<channel>")
def sv(channel = None):
    return ncr.render_svnotes(channel)


@app.route("/sveditnotes/<user>/<channel>/", defaults = {"message": None})
@app.route("/sveditnotes/<user>/<channel>/<message>")
def sveditnotes(user, channel, message = None):
    if request.query_string:
        query_str = str(request.query_string).replace("b'", "").replace(
            "b\"", "").rstrip("\'").rstrip("\"")
        if message is not None:
            message = "%s?%s" % (message, query_str)
    return ncr.put_svnotes(user, channel, message)


@app.route("/ds3boss/<user>/<channel>/")
def ds3bosses(user, channel):
    return ncr.get_boss_info(user, channel)


@app.route("/ds3setboss/<user>/<channel>/", defaults = {"message": None})
@app.route("/ds3setboss/<user>/<channel>/<message>")
def ds3setbosses(user, channel, message = None):
    if request.query_string:
        query_str = str(request.query_string).replace("b'", "").replace(
            "b\"", "").rstrip("\'").rstrip("\"")
        if message is not None:
            message = "%s?%s" % (message, query_str)
    return ncr.put_boss_info(user, channel, message)


@app.route("/system/<nickname>/", defaults = {"channel": None})
@app.route("/system/<nickname>/<channel>")
def system(nickname, channel = None):
    return ncr.get_system_info(nickname, channel)


@app.route("/setsystem/<channel>/", defaults = {"target": None})
@app.route("/setsystem/<channel>/<target>")
def setsystem(channel, target = None):
    return ncr.put_system_info(channel, target)


@app.route("/seppuku/<host>/<botname>/<user>/")
def seppuku(host, botname, user):
    return ncr.get_seppuku_string(host, botname, user)


@app.route("/steamplaytime/<host>/<target>/", defaults = {"query": None})
@app.route("/steamplaytime/<host>/<target>/<query>")
def steamplaytime(host, target, query = None):
    if request.query_string:
        query_str = str(request.query_string).replace("b'", "").replace(
            "b\"", "").rstrip("\'").rstrip("\"")
        if query is not None:
            query = "%s?%s" % (query, query_str)
    return ncr.get_steamplaytime_string(host, target, query)


@app.route("/eightball/<user>/", defaults = {"question": None})
@app.route("/eightball/<user>/<question>")
def eightball(user, question = None):
    if request.query_string:
        query_str = str(request.query_string).replace("b'", "").replace(
            "b\"", "").rstrip("\'").rstrip("\"")
        if question is not None:
            question = "%s?%s" % (question, query_str)
    return ncr.get_eightball_string(user, question)


@app.route("/gamble/<user>/", defaults = {"points": None})
@app.route("/gamble/<user>/<points>")
def gamble(user, points = None):
    return ncr.get_gamble_string(user, points)


app.wsgi_app = ProxyFix(app.wsgi_app)

if __name__ == "__main__":
    app.run(debug = False)
