from ..interfaces.NexusDatabaseInterface import NexusDatabaseInterface
from ..interfaces.NexusSoupInterface import NexusSoupInterface
from ..interfaces.NexusDiscordInterface import NexusDiscordInterface
from ..interfaces.NexusSteamInterface import NexusSteamInterface
from ..interfaces.NexusStrawpollInterface import NexusStrawpollInterface
from ..interfaces.NexusTwitchInterface import NexusTwitchInterface
from ..interfaces.NexusUrbanInterface import NexusUrbanInterface
from ..interfaces.NexusYoutubeInterface import NexusYoutubeInterface
from ..logic.NexusCalculator import NexusCalculator
from ..logic.NexusDeltaFunctions import NexusDeltaFunctions
from .NexusVariables import weather_lists, system_dicts
from datetime import datetime
from datetime import timedelta
from html import unescape
from operator import itemgetter
from random import randint
from random import choice
from urllib import parse
import json
import hashlib


ndi = NexusDatabaseInterface()
nsi = NexusSoupInterface()
nci = NexusDiscordInterface()
nei = NexusSteamInterface()
npi = NexusStrawpollInterface()
nti = NexusTwitchInterface()
nui = NexusUrbanInterface()
nyi = NexusYoutubeInterface()
ncl = NexusCalculator()
ndf = NexusDeltaFunctions()


class NexusCore(object):

    def get_googlejson_string(self, query):
        response = json.dumps(
            nsi.get_googlesearch_result(
                query, 5), indent=4, sort_keys=False)
        if response is not None:
            return "<pre>%s</pre>" % str(response)
        return ""


    def get_youtubejson_string(self, query):
        response = json.dumps(
            nyi.get_video_search(query),
            indent=4,
            sort_keys=False)
        if response is not None:
            return "<pre>%s</pre>" % str(response)
        return ""


    def get_weatherjson_string(self, query):
        response = json.dumps(
            nsi.get_googleweather_result(query),
            indent=4,
            sort_keys=False)
        if response is not None:
            return "<pre>%s</pre>" % str(response)
        return ""


    def get_definejson_string(self, query):
        response = json.dumps(
            nsi.get_googledefine_result(query),
            indent=4,
            sort_keys=False)
        if response is not None:
            return "<pre>%s</pre>" % str(response)
        return ""


    def get_timejson_string(self, query):
        response = json.dumps(
            nsi.get_googletime_result(query),
            indent=4,
            sort_keys=False)
        response = nsi.get_googletime_result(query)
        if response is not None:
            return "<pre>%s</pre>" % str(response)
        return ""


    def get_currencyjson_string(self, query):
        response = json.dumps(
            nsi.get_googlecurrency_result(query),
            indent=4,
            sort_keys=False)
        if response is not None:
            return "<pre>%s</pre>" % str(response)
        return ""


    def get_steamjson_string(self, query):
        response = json.dumps(
            nsi.get_steamstore_result(query),
            indent=4,
            sort_keys=False)
        if response is not None:
            return "<pre>%s</pre>" % str(response)
        return ""


    def get_discordjson_string(self, user_id):
        response = json.dumps(
            nci.get_user_data(user_id),
            indent=4,
            sort_keys=False)
        if response is not None:
            return "<pre>%s</pre>" % str(response)
        return ""


    def get_polljson_string(self, query):
        response = npi.get_poll(query)[1]
        if response is not None:
            return str(response)
        return ""


    def get_vodjson_string(self, channel):
        response = nti.get_video_data(channel)[1]
        if response is not None:
            return "<pre>%s</pre>" % str(response)
        return ""


    def get_channeljson_string(self, channel):
        response = nti.get_channel_data(channel)[1]
        if response is not None:
            return "<pre>%s</pre>" % str(response)
        return ""


    def get_streamjson_string(self, channel):
        response = nti.get_stream_data(channel)[1]
        if response is not None:
            return "<pre>%s</pre>" % str(response)
        return ""


    def get_gamesjson_string(self, query):
        response = nti.get_games_search(query)[0]
        if response is not None:
            return "<pre>%s</pre>" % str(response)
        return ""


    def get_urbanjson_string(self, query):
        response = nui.get_definition_result(query)[1]
        if response is not None:
            return "<pre>%s</pre>" % str(response)
        return ""


    def get_random_stream(self):
        stream_data = nti.get_random_live_stream()[0]
        while True:
            lng = stream_data["streams"][0]["channel"]["broadcaster_language"]
            if lng.lower() == "en":
                break
            else:
                stream_data = nti.get_random_live_stream()[0]

        if stream_data is not None:
            return stream_data["streams"][0]["channel"]["display_name"]
        return ""


    def get_display_name_string(self, user):
        user_data = ndf.channel_exists(user, "id")
        if user_data is None:
            return None
        return user_data["users"][0]["display_name"]


    def get_interval_string(self):
        a = datetime.now()
        b = datetime(1994, 12, 25)
        response = ncl.compute_time_interval(a, b)
        if response is not None:
            return str(response)
        return ""


    def is_host_live(self, channel):
        channel_data = ndf.channel_exists(channel, "data")

        if channel_data is None:
            return False

        stream_data = nti.get_stream_data(channel)[0]
        
        try:
            return stream_data["stream"] is not None
        except KeyError:
            return False


    def get_host_game(self, channel):
        channel_data = ndf.channel_exists(channel, "data")

        if channel_data is None:
            return None
        
        try:
            if channel_data["game"] == "":
                return None
            else:
                return channel_data["game"]
        except KeyError:
            return None


    def is_user_subscriber(self, channel, user):
        channel_data = ndf.channel_exists(channel, "id")
        user_data = ndf.channel_exists(user, "id")

        if channel_data is None:
            return False

        if user_data is None:
            return False

        sub_status = nti.get_subscriber_status(channel, user)[0]
        
        return "user" in sub_status.keys()


    def get_follower_list(self, channel):
        follower_list = []
        recent_follower_dict = nti.get_recent_followers(channel, 50)[0]

        for i, follower in enumerate(recent_follower_dict["follows"]):
            follower_list.append(recent_follower_dict["follows"][
                                 i]["user"]["display_name"])

        return follower_list


    def get_caster_string(self, host, game, channel = None):
        display_name = channel_url = channel_game = None

        if channel is None:
            return "Sorry you didn't provide a channel to link to! Please try again."

        game = game.lower() == "true"

        channel_data = ndf.channel_exists(channel, "data")

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel
        else:
            display_name = str(channel_data["display_name"])
            channel_game = str(channel_data["game"])
            channel_url = str(channel_data["url"]).replace("https://www.", "")

        if display_name.lower() == host:
            return str("You are currently in this broadcasters stream! Kappa")
        else:
            if display_name is not None:
                if game and channel_game != "None":
                    return "You should check out %s at %s! They were last seen playing %s, they are pretty cool!" % (
                        display_name, channel_url, channel_game)
                else:
                    return "You should check out %s at %s, they are pretty cool!" % (
                        display_name, channel_url)

        return ""


    def get_currentgame_string(self, channel, nickname):
        display_name = None
        stream_live = False

        nickname_active = nickname.lower() != "false"

        channel_data = ndf.channel_exists(channel, "data")

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        stream_data = nti.get_stream_data(channel)[0]

        if nickname_active is False:
            display_name = str(channel_data["display_name"])
        else:
            display_name = str(ndf.ucwords(nickname))

        if stream_data["stream"] is None:
            return "%s is not currently streaming!" % display_name
        else:
            current_game = str(stream_data["stream"]["channel"]["game"])
            stream_live = True

        if display_name is not None and stream_live is True:
            if current_game == "Creative":
                return "%s is currently being %s!" % (
                    display_name, current_game)
            elif current_game == "Talk Shows":
                return "%s is currently hosting %s!" % (
                    display_name, current_game)
            elif current_game == "Music":
                return "%s is currently making %s!" % (
                    display_name, current_game)
            else:
                return "%s is currently playing %s!" % (
                    display_name, current_game)
        else:
            return ""


    def get_countgames_string(self, channel, host, nickname, query=None):
        game_title = viewers_count = viewer_string = viewer_string_two = display_name_single = name_single = None
        channel_game_exists = False

        nickname_active = nickname.lower() != "false"

        channel_data = ndf.channel_exists(channel, "data")

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        if query is None and channel:
            query = parse.quote(channel_data["game"])
            channel_game_exists = True

        # if channel_game_exists == false:
        #     file = _SERVER["DOCUMENT_ROOT"] + "/database/gamesearch.json"
        #     json = array()
        #     json = array_unique(json_decode(file_get_contents(file), true))
        #     cached_query = false
        #     cached_query = array_search(parse.unquote(query), json)
        #     if cached_query is not None:
        #         query = parse.quote(cached_query)

        gamesearch_data = nti.get_games_search(query)[0]

        if gamesearch_data["games"] is None or gamesearch_data is None:
            return ("Sorry, the search for \"%s\" "
                    "returned no results! Please try again."
                    % (parse.unquote(query)))
        else:
            game_title = str(
                parse.unquote(
                    gamesearch_data["games"][0]["name"]))
            viewers_count = str(gamesearch_data["games"][0]["popularity"])

        if game_title is None:
            return ("Sorry, the search for \"%s\" "
                    "returned no results! Please try again."
                    % (parse.unquote(query)))
        else:
            streamsearch_data = nti.get_streams_search_game(
                parse.quote(game_title))[0]
            if "_total" in streamsearch_data:
                streams_count = str(streamsearch_data["_total"])
                if streams_count == "0":
                    streams_count = "no"
            else:
                streams_count = "0"
            if streamsearch_data["_total"] is "1":
                display_name_single = str(
                    streamsearch_data["streams"][0]["channel"]["display_name"])
                name_single = str(
                    streamsearch_data["streams"][0]["channel"]["name"])
                if nickname_active is not False and host.lower() == display_name_single.lower():
                    display_name_single = str(ndf.ucwords(nickname))
                channel_url = str(
                    streamsearch_data["streams"][0]["channel"]["url"]).replace(
                    "https://www.", "")

        if viewers_count is not None or viewers_count != "" or viewers_count != "0":
            viewers_count = str(viewers_count)
            if viewers_count is "1":
                viewer_str = "with %s viewer" % viewers_count
                viewer_str_two = "with only %s viewer" % viewers_count
            else:
                viewer_str = "with %s total viewers" % viewers_count
                viewer_str_two = "with %s viewers" % viewers_count

        if streams_count is 1 and display_name_single is not None:
            if host == name_single.lower():
                return "%s is the only one currently streaming %s, %s! PogChamp" % (
                    display_name_single, game_title, viewer_str_two)
            else:
                return "%s is the only one currently streaming %s, %s! %s PogChamp" % (
                    display_name_single, game_title, viewer_str_two, channel_url)
        else:
            if game_title == "Creative":
                return "There are currently %s streams of people being %s, %s!" % (
                    streams_count, game_title, viewer_str)
            elif game_title == "Talk Shows":
                return "There are currently %s streams of people hosting a Talk Show, %s!" % (
                    streams_count, viewer_str)
            elif game_title == "Music":
                return "There are currently %s streams of people making %s, %s!" % (
                    streams_count, game_title, viewer_str)
            elif game_title == "IRL":
                return "There are currently %s streams of people %s, %s!" % (
                    streams_count, game_title, viewer_str)
            else:
                return "There are currently %s streams of people playing %s, %s!" % (
                    streams_count, game_title, viewer_str)
        return ""


    def get_twitchcon_string(self):
        twitchcon_start = datetime(2018, 10, 26, 17)
        twitchcon_end = datetime(2018, 10, 29, 2)
        current = datetime.utcnow()
        if twitchcon_start - \
                current <= timedelta(0) and twitchcon_end - current >= timedelta(0):
            return "TwitchCon 2018 is on right now!"
        elif twitchcon_end - current >= timedelta(0):
            interval = str(
                ncl.compute_time_interval(
                    twitchcon_start, current, 5))
            return "TwitchCon 2018 starts in %s!" % interval
        else:
            interval = str(
                ncl.compute_time_interval(
                    current, twitchcon_end, 5))
            return "TwitchCon 2018 ended %s ago! BibleThump" % interval


    def get_paxaus_string(self):
        paxaus_start = datetime(2018, 10, 25, 23)
        paxaus_end = datetime(2018, 10, 28, 8)
        current = datetime.utcnow()
        if paxaus_start - \
                current <= timedelta(0) and paxaus_end - current >= timedelta(0):
            return "PAX Aus 2018 is on right now!"
        elif paxaus_end - current >= timedelta(0):
            interval = str(ncl.compute_time_interval(paxaus_start, current, 5))
            return "PAX Aus 2018 starts in %s!" % interval
        else:
            interval = str(ncl.compute_time_interval(current, paxaus_end, 5))
            return "PAX Aus 2018 ended %s ago! FeelsBadMan" % interval


    def get_uptime_string(self, host, game, nickname, channel=None):
        display_name = created = None

        nickname_active = nickname.lower() != "false"
        game = game.lower() == "true"

        if channel is None:
            channel = host
        else:
            if channel != host:
                nickname_active = False

        current = datetime.utcnow()

        channel_data = ndf.channel_exists(channel, "data")

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        stream_data = nti.get_stream_data(channel)[0]
        video_data = nti.get_video_data(channel)[0]

        if nickname_active is False:
            display_name = str(channel_data["display_name"])
        else:
            display_name = str(ndf.ucwords(nickname))

        if stream_data["stream"] is None:
            return "%s is not currently streaming!" % display_name
        else:
            created = datetime.strptime(
                stream_data["stream"]["created_at"],
                "%Y-%m-%dT%H:%M:%SZ")
            stream_created = created
            current_game = str(stream_data["stream"]["channel"]["game"])
            grace_period = timedelta(minutes=10)
            vod_data = []

            for i, video in enumerate(video_data["videos"]):
                if i == 0:
                    continue
                if i < 9:
                    vod_created = datetime.strptime(
                        video_data["videos"][i]["created_at"], "%Y-%m-%dT%H:%M:%SZ")
                    vod_end = vod_created + \
                        timedelta(seconds=video_data["videos"][i]["length"])
                    vod_data.append({
                        "vod_created": vod_created,
                        "vod_end": vod_end
                    })
                else:
                    break

            previous = interval_total = None
            length = len(vod_data)
            for i, vod in enumerate(vod_data):
                if i > 0:
                    previous = vod_data[i - 1]
                else:
                    break
                if i == 0:
                    interval = vod_data[i]["vod_end"] - stream_created
                else:
                    interval = previous["vod_end"] - vod_data[i]["vod_created"]
                    if interval_total is None:
                        interval_total = interval
                    else:
                        interval_total = interval_total + interval
                if interval < grace_period:
                    created = vod_data[i]["vod_created"] + interval_total

        if created is not None:
            interval = str(ncl.compute_time_interval(current, created, 3))
            if game is True:
                if current_game == "Creative":
                    return "%s has been live for %s, currently being %s!" % (
                        display_name, interval, current_game)
                elif current_game == "Talk Shows":
                    return "%s has been live for %s, currently hosting a Talk Show!" % (
                        display_name, interval)
                elif current_game == "Music":
                    return "%s has been live for %s, currently making %s!" % (
                        display_name, interval, current_game)
                elif current_game == "IRL":
                    return "%s has been live for %s, currently %s!" % (
                        display_name, interval, current_game)
                else:
                    return "%s has been live for %s, currently playing %s!" % (
                        display_name, interval, current_game)
            else:
                return "%s has been live for %s!" % (display_name, interval)
        else:
            if display_name is None or display_name == "":
                return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel
            else:
                return "%s is not currently streaming!" % channel
        return ""


    def get_downtime_string(self, host, game, nickname, channel=None):
        display_name = created = None

        nickname_active = nickname.lower() != "false"
        game = game.lower() == "true"

        if channel is None:
            channel = host
        else:
            if channel != host:
                nickname_active = False

        current = datetime.utcnow()

        channel_data = ndf.channel_exists(channel, "data")

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        stream_data = nti.get_stream_data(channel)[0]
        video_data = nti.get_video_data(channel)[0]

        if nickname_active is False:
            display_name = str(channel_data["display_name"])
        else:
            display_name = str(ndf.ucwords(nickname))

        if stream_data["stream"] is not None:
            current_game = str(stream_data["stream"]["channel"]["game"])
            return "%s is currently streaming! They are live in the %s directory!" % (
                display_name, current_game)
        else:
            if video_data["_total"] is 0:
                return "%s does not have any archived videos! FeelsBadMan" % display_name
            current_game = str(channel_data["game"])
            created = datetime.strptime(
                video_data["videos"][0]["created_at"],
                "%Y-%m-%dT%H:%M:%SZ") + timedelta(
                seconds=video_data["videos"][0]["length"])

        if created is not None:
            if current - created < timedelta(minutes=1):
                return "%s is currently streaming! They are live in the %s directory!" % (
                    display_name, current_game)
            interval = str(ncl.compute_time_interval(current, created, 3))
            if display_name[-1] == "s":
                display_name_formatted = "%s'" % display_name
            else:
                display_name_formatted = "%s's" % display_name
            if game is True:
                if current_game == "Creative":
                    return "%s last stream was %s ago, they were being %s!" % (
                        display_name_formatted, interval, current_game)
                elif current_game == "Talk Shows":
                    return "%s last stream was %s ago, they were hosting a Talk Show!" % (
                        display_name_formatted, interval)
                elif current_game == "Music":
                    return "%s last stream was %s ago, they were making %s!" % (
                        display_name_formatted, interval, current_game)
                elif current_game == "IRL":
                    return "%s last stream was %s ago, they were %s!" % (
                        display_name_formatted, interval, current_game)
                else:
                    return "%s last stream was %s ago, they were playing %s!" % (
                        display_name_formatted, interval, current_game)
            else:
                return "%s last stream was %s ago!" % (
                    display_name_formatted, interval)
        else:
            if display_name is None or display_name == "":
                return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel
            else:
                return "%s is currently streaming!" % display_name
        return ""


    def get_created_string(self, user, channel=None):
        display_name = created = None

        if channel is None:
            channel = user.lower()
        else:
            channel = channel.lower()

        current = datetime.utcnow()
        channel_data = ndf.channel_exists(channel, "id")

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        display_name = channel_data["users"][0]["display_name"]
        created = datetime.strptime(
            channel_data["users"][0]["created_at"],
            "%Y-%m-%dT%H:%M:%S.%fZ")

        if created is not None:
            interval = str(ncl.compute_time_interval(current, created, 5))
            return "%s was created %s ago!" % (display_name, interval)
        return ""


    def get_following_string(self, nickname, channel, user, follower = None):
        display_name = follower_display_name = created = None

        if follower is None:
            follower = user.lower()
        else:
            follower = follower.lower()

        nickname_active = nickname.lower() != "false"

        channel_data = ndf.channel_exists(channel, "id")

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        if nickname_active is False:
            display_name = str(channel_data["users"][0]["display_name"])
        else:
            display_name = str(ndf.ucwords(nickname))

        if channel == follower:
            return "Nice try, but its impossible to follow yourself, %s! Kappa" % display_name

        follower_channel_data = ndf.channel_exists(follower, "data")

        if follower_channel_data is None:
            return "Sorry, the specified Twitch channel \"%s\" was not found! Please try again." % follower

        follower_display_name = str(follower_channel_data["display_name"])
        current = datetime.utcnow()

        if follower_display_name is None:
            return "Sorry, the specified Twitch channel \"%s\" was not found! Please try again." % follower

        follower_data = nti.get_follower_data(channel, follower)

        try:
            created = datetime.strptime(
                follower_data[0]["created_at"],
                "%Y-%m-%dT%H:%M:%SZ")
        except KeyError:
            return "%s is not currently following %s! :(" % (
                follower_display_name, display_name)

        if created is None:
            return "%s is not currently following %s! :(" % (
                follower_display_name, display_name)
        else:
            interval = str(ncl.compute_time_interval(current, created, 5))
            return "%s has been following %s for %s!" % (
                follower_display_name, display_name, interval)
        return ""


    def get_firstfollowers_string(self, host, nickname, amount, channel = None):
        display_name = follower_amount = None
        follower_list = []

        nickname_active = nickname.lower() != "false"

        if channel is None:
            channel = host
        else:
            if channel != host:
                nickname_active = False

        channel_data = ndf.channel_exists(channel, "id")

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        if nickname_active is False:
            display_name = str(channel_data["users"][0]["display_name"])
        else:
            display_name = str(ndf.ucwords(nickname))

        first_follower_dict = nti.get_first_followers(channel, amount)[0]

        for i, follower in enumerate(first_follower_dict["follows"]):
            follower_list.append(first_follower_dict["follows"][
                                 i]["user"]["display_name"])

        if follower_list is None:
            return "%s does not have any followers! FeelsBadMan" % display_name
        else:
            if display_name[-1] == "s":
                display_name_formatted = "%s'" % display_name
            else:
                display_name_formatted = "%s's" % display_name
            if len(follower_list) is 1:
                return "%s first and only follower is %s!" % (
                    display_name_formatted, follower_list[0])
            elif len(follower_list) > 1:
                follower_string = "%s and %s" % (
                    ", ".join(follower_list[:-1]), follower_list[-1])
            return "%s first %s followers are %s!" % (
                display_name_formatted, len(follower_list), follower_string)
        return ""


    def get_recentfollowers_string(self, host, nickname, amount, channel = None):
        display_name = follower_amount = None
        follower_list = []

        nickname_active = nickname.lower() != "false"

        if channel is None:
            channel = host
        else:
            if channel != host:
                nickname_active = False

        channel_data = ndf.channel_exists(channel, "id")

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        if nickname_active is False:
            display_name = str(channel_data["users"][0]["display_name"])
        else:
            display_name = str(ndf.ucwords(nickname))

        recent_follower_dict = nti.get_recent_followers(channel, amount)[0]

        for i, follower in enumerate(recent_follower_dict["follows"]):
            follower_list.append(recent_follower_dict["follows"][
                                 i]["user"]["display_name"])

        if follower_list is None:
            return "%s does not have any followers! FeelsBadMan" % display_name
        else:
            if display_name[-1] == "s":
                display_name_formatted = "%s'" % display_name
            else:
                display_name_formatted = "%s's" % display_name
            if len(follower_list) is 1:
                return "%s most recent and only follower is %s!" % (
                    display_name_formatted, follower_list[0])
            elif len(follower_list) > 1:
                follower_string = "%s and %s" % (
                    ", ".join(follower_list[:-1]), follower_list[-1])
            return "%s %s most recent followers are %s!" % (
                display_name_formatted, len(follower_list), follower_string)
        return ""


    def get_lovetester_string(self, user, botname, query=None):
        display_name = url_title = result_url = None

        user_data = ndf.channel_exists(user, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        display_name = str(user_data["users"][0]["display_name"])

        if query is not None and query.strip().lower() == botname.strip().lower():
            return "%s > Silly organic, bots cannot know love! BibleThump" % display_name

        if query is None:
            hash_str = display_name.lower()
        else:
            hash_str = str("%s%s" % (display_name.lower(), query.lower()))

        # Get the first byte from the hash divide by 99 to get 0 to 99 then
        # shift by 1
        percent = hashlib.md5(hash_str.encode('utf8')).digest()[0] % 99 + 1

        if query is None:
            return "There is %s%% <3 between %s and themselves! Kappa" % (
                percent, display_name)
        else:
            return "There is %s%% <3 between %s and %s!" % (
                percent, display_name, ndf.ucwords(query))
        return ""


    def get_googleweather_string(self, user, query=False):
        display_name = None

        user_data = ndf.channel_exists(user, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        display_name = str(user_data["users"][0]["display_name"])

        if query is None:
            return "You didn't provide a search query! Please try again. Usage - !weather <location>"
        else:
            if display_name is None:
                display_name = user

            test_query = parse.unquote(query).lower()
            title_query = ndf.ucwords(parse.unquote(query))

            if test_query == "venus":
                return "%s > Weather for %s // Who cares about the weather... you have the Vex to deal with!" % (
                    display_name, title_query)
            elif test_query == "the sun":
                return "%s > Weather for %s // I think its fairly safe to assume it's immensely hot!" % (
                    display_name, title_query)
            elif test_query == "pluto":
                return "%s > Weather for %s // All I know is Pluto is not considered a planet anymore, and that's just sad! FeelsBadMan" % (
                    display_name, title_query)
            elif test_query == "uranus":
                return "%s > Weather for %s // Are you being lewd there?" % (
                    display_name, title_query)
            elif test_query == "mordor":
                return "%s > Weather for %s // Sauron ensures that Mordor is always super freaking hot! Gotta keep the orcs working under pressure!" % (
                    display_name, title_query)
            elif test_query in weather_lists["world_list"]:
                return "%s > Weather for World of Tomorrow // Sunshine, rainbows, lollipops and lots of love! bleedPurple" % display_name
            elif test_query in weather_lists["cold_places_list"]:
                return "%s > Weather for %s // Colder than you could possibly imagine!" % (
                    display_name, title_query)
            elif test_query in weather_lists["days_list"]:
                return "%s > Weather for %s // That's a day not a location. Please try again." % (
                    display_name, title_query)
            elif test_query in weather_lists["planets_list"]:
                return "%s > Weather for %s // I don't know man... that's out of this world!" % (
                    display_name, title_query)
            elif test_query in weather_lists["star_wars_list"]:
                return "%s > Weather for %s // Be one with the force, and you too can predict the weather!" % (
                    display_name, title_query)
            elif test_query in weather_lists["middle_earth_list"]:
                return "%s > Weather for %s // The temperature in Middle-Earth is usually a brisk 17°C (%s°F)." % (
                    display_name, title_query, str(ncl.convert_to_fahrenheit(round(17, 1))))
            elif test_query in weather_lists["mass_effect_list"]:
                return "%s > Weather for %s // Maybe Commander Shepard can tell you! PogChamp" % (
                    display_name, title_query)

            googleweather_data = nsi.get_googleweather_result(query)

            if googleweather_data is None:
                return ("%s > Location \"%s\" is invalid or something wen't wrong. Please try again." % (
                    display_name, parse.unquote(query)))

            temps = {}

            location = googleweather_data["location"]
            current_temp = googleweather_data["current_temp"]
            if current_temp[-1].upper() == "C":
                temps["current_c_temp"] = current_temp
                temps["current_f_temp"] = "%s°F" % str(ncl.convert_to_fahrenheit(
                    round(int(current_temp.replace("°C", "")), 1)))
            elif current_temp[-1].upper() == "F":
                temps["current_f_temp"] = current_temp
                temps["current_c_temp"] = "%s°C" % str(ncl.convert_to_celsius(
                    round(int(current_temp.replace("°F", "")), 0)))

            high_temp = googleweather_data["high_temp"]
            if high_temp[-1].upper() == "C":
                temps["high_c_temp"] = high_temp
                temps["high_f_temp"] = "%s°F" % str(ncl.convert_to_fahrenheit(
                    round(int(high_temp.replace("°C", "")), 1)))
            elif high_temp[-1].upper() == "F":
                temps["high_f_temp"] = high_temp
                temps["high_c_temp"] = "%s°C" % str(ncl.convert_to_celsius(
                    round(int(high_temp.replace("°F", "")), 0)))

            low_temp = googleweather_data["low_temp"]
            if low_temp[-1].upper() == "C":
                temps["low_c_temp"] = low_temp
                temps["low_f_temp"] = "%s°F" % str(ncl.convert_to_fahrenheit(
                    round(int(low_temp.replace("°C", "")), 1)))
            elif low_temp[-1].upper() == "F":
                temps["low_f_temp"] = low_temp
                temps["low_c_temp"] = "%s°C" % str(ncl.convert_to_celsius(
                    round(int(low_temp.replace("°F", "")), 0)))

            status = ndf.ucwords(googleweather_data["status"])
            humidity = googleweather_data["humidity"]

            return "%s > Weather for %s // %s (%s) // %s // High %s (%s) // Low %s (%s) // %s Humidity" % (
                        display_name,
                        location,
                        temps["current_c_temp"], temps["current_f_temp"],
                        status,
                        temps["high_c_temp"], temps["high_f_temp"],
                        temps["low_c_temp"], temps["low_f_temp"],
                        humidity
                    )
        return ""


    def get_googlesearch_string(self, user, query=None):
        display_name = url_title = result_url = None

        user_data = ndf.channel_exists(user, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        display_name = str(user_data["users"][0]["display_name"])

        if query is None:
            return "You didn't provide a search query! Please try again. Usage - !google <search query>"
        else:
            decoded_query = parse.unquote(query).strip()
            disallowed_keywords = [
                "fuck",
                "dick",
                "porn",
                "pron",
                "fucking",
                "cock",
                "whore",
                "pussy",
                "cunt"]

            query_word_list = decoded_query.split(" ")

            for keyword in query_word_list:
                for bad_keyword in disallowed_keywords:
                    if keyword == bad_keyword:
                        return "Google Search Query contains disallowed keywords. Please try again."

            if display_name is None:
                display_name = user

            google_data = nsi.get_googlesearch_result(query)

            if google_data is None:
                return "%s > Google Search for \"%s\" returned no results. Please try again." % (
                    display_name, decoded_query)

            i = 0
            search_strings = []

            while i <= 1:
                if i < len(google_data):
                    url_title = google_data[i]["title"]
                    result_url = google_data[i]["url"].replace(
                        "https://", "").replace("http://", "").rstrip("/").strip()
                    search_strings.append(
                        "#%d: %s - %s" %
                        (i + 1, url_title, result_url))
                i += 1

            return "%s > Google Search for \"%s\" // %s" % (
                display_name, decoded_query, " // ".join(search_strings))
        return ""


    def get_youtubesearch_string(self, user, query=None):
        display_name = url_title = result_url = None

        user_data = ndf.channel_exists(user, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        display_name = str(user_data["users"][0]["display_name"])

        if query is None:
            return "You didn't provide a search query! Please try again. Usage - !google <search query>"
        else:
            decoded_query = parse.unquote(query).strip()
            disallowed_keywords = [
                "fuck",
                "dick",
                "porn",
                "pron",
                "fucking",
                "cock",
                "whore",
                "pussy",
                "cunt"]

            query_word_list = decoded_query.split(" ")

            for keyword in query_word_list:
                for bad_keyword in disallowed_keywords:
                    if keyword == bad_keyword:
                        return "Google Search Query contains disallowed keywords. Please try again."

            if display_name is None:
                display_name = user

            video_data = nyi.get_video_search(query)[0]

            if video_data["pageInfo"]["totalResults"] == 0:
                return "%s > YouTube Search for \"%s\" returned no results. Please try again." % (
                    display_name, decoded_query)

            i = 0
            search_strings = []

            while i <= 1:
                if i < len(video_data):
                    video_title = video_data["items"][i]["snippet"]["title"]
                    video_url = "https://youtu.be/%s" % video_data[
                        "items"][i]["id"]["videoId"]
                    youtube_channel = video_data["items"][
                        i]["snippet"]["channelTitle"]
                    search_strings.append(
                        "#%d: %s - %s - %s" %
                        (i + 1, video_title, video_url, youtube_channel))
                i += 1

            return "%s > YouTube Search for \"%s\" // %s" % (
                display_name, decoded_query, " // ".join(search_strings))
        return ""


    def get_googledefine_string(self, user, query=None, flag=False):
        display_name = url_title = result_url = None

        user_data = ndf.channel_exists(user, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        display_name = str(user_data["users"][0]["display_name"])

        if flag:
            return "%s > Sorry, this command needs fixing. Google changed their shit. FeelsBadMan" % display_name

        if query is None:
            return "You didn't provide a search query! Please try again. Usage - !google <search query>"
        else:
            decoded_query = parse.unquote(query)
            disallowed_keywords = [
                "fuck",
                "dick",
                "porn",
                "pron",
                "fucking",
                "cock",
                "whore",
                "pussy",
                "cunt"]

            query_word_list = decoded_query.split(" ")

            for keyword in query_word_list:
                for bad_keyword in disallowed_keywords:
                    if keyword == bad_keyword:
                        return "Dictionary query contains disallowed keywords. Please try again."

            if display_name is None:
                display_name = user

            google_data = nsi.get_googledefine_result(query)

            if google_data is None:
                return "%s > Dictionary query for \"%s\" returned no results. Please try again." % (
                    display_name, decoded_query)

            word = google_data["word"]
            word_type = google_data["type"]
            definition = google_data["definition"]

            return "%s > \"%s\" [%s] // %s" % (display_name,
                                               word, word_type, definition)
        return ""


    def get_googlecurrency_string(self, user, query=None):
        display_name = url_title = result_url = None

        user_data = ndf.channel_exists(user, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        display_name = str(user_data["users"][0]["display_name"])

        if query is None:
            return "You didn't provide currency conversion data! Please try again. Usage - !currency <amount> <source> in <destination> Example - 50 AUD in USD"
        else:
            if display_name is None:
                display_name = user

            currency_data = nsi.get_googlecurrency_result(query)

            if currency_data is None:
                return "%s > Something went wrong or the data provided is invalid. Please try again." % display_name

            error_thrown = "error" in currency_data.keys()

            if error_thrown:
                if currency_data["error"] == "Source currency invalid":
                    return "%s > The source currency \"%s\" is invalid. Please try again." % (
                        display_name, currency_data["input"])
                elif currency_data["error"] == "Destination currency invalid":
                    return "%s > The destination currency \"%s\" is invalid. Please try again." % (
                        display_name, currency_data["input"])
                else:
                    return "%s > Something went wrong or the data provided is invalid. Please try again." % display_name

            return "%s > %s %s equates to %s %s" % (display_name, currency_data["source_amount"], currency_data[
                                                    "source_name"], currency_data["dest_amount"], currency_data["dest_name"])
        return ""


    def get_urbandefine_string(self, user, query=None):
        display_name = url_title = result_url = None

        user_data = ndf.channel_exists(user, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        display_name = str(user_data["users"][0]["display_name"])

        if query is None:
            return "You didn't provide a search query! Please try again. Usage - !urban <search query>"
        else:

            if display_name is None:
                display_name = user

            urban_data = nui.get_definition_result(query)[0]["list"][0]

            if urban_data is None:
                return "%s > Urban Dictionary query for \"%s\" returned no results. Please try again." % (
                    display_name, decoded_query)

            word = ndf.ucwords(urban_data["word"])
            definition = urban_data["definition"]

            if len(definition) > 280:
                definition = "%s%s %s" % (
                    definition[:280], "...", urban_data["permalink"])

            return "%s > \"%s\" // %s" % (display_name, word, definition)
        return ""


    def get_googletime_string(self, user, query=None):
        display_name = None

        user_data = ndf.channel_exists(user, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        display_name = str(user_data["users"][0]["display_name"])

        if query is None:
            return "You didn't provide a search query! Please try again. Usage - !time <location>"
        else:
            if display_name is None:
                display_name = user

            googletime_data = nsi.get_googletime_result(query)

            if googletime_data is None:
                return "%s > Location \"%s\" is invalid or something wen't wrong. Please try again." % (
                    display_name, query)

            location = googletime_data["location"]
            date_timezone = googletime_data["date_timezone"].split("(")
            date = date_timezone[0].strip()
            timezone = date_timezone[1].replace("+", " +").rstrip(")")
            time = googletime_data["time"]

            return "%s > The time is %s [%s] on %s in %s!" % (
                display_name, time, timezone, date, location)
        return ""


    def get_googlespotify_string(self, user, query=None):
        display_name = url_title = result_url = None

        user_data = ndf.channel_exists(user, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        display_name = str(user_data["users"][0]["display_name"])

        if query is None:
            return "You didn't provide a Spotify track search query! Please try again. Usage - !spotify <search query>"
        else:
            decoded_query = parse.unquote(query)
            query = "%s+spotify+track" % query

            if display_name is None:
                display_name = user

            google_data = nsi.get_googlesearch_result(query)

            if google_data is None:
                return "%s > Spotify track search for \"%s\" returned no results. Please try again." % (
                    display_name, decoded_query)

            song_title = google_data[0]["title"].replace(
                " on Spotify", "").replace(
                ", a song by ", " - ")
            track_id = google_data[0]["url"].replace(
                "https://open.spotify.com/track/",
                "").replace(
                "http://open.spotify.com/track/",
                "").rstrip("/").strip()

            return "%s > %s // %s" % (display_name, song_title, track_id)
        return ""


    def get_poll_string(self, user, channel):
        display_name = None

        user_data = ndf.channel_exists(user, "id")
        channel_data = ndf.channel_exists(channel, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        user_display_name = str(user_data["users"][0]["display_name"])
        display_name = str(channel_data["users"][0]["display_name"])

        try:
            with open("./database/" + channel + "_currentpoll.txt", "r") as f:
                poll_id = f.read().strip()
        except:
            return "Poll for %s cannot be found!" % display_name

        if poll_id == "":
            return "Poll for %s cannot be found!" % display_name

        poll_data = npi.get_poll(poll_id)[0]

        results_list = []
        results_list_str = []
        options = poll_data["options"]
        votes = poll_data["votes"]

        for key, value in zip(options, votes):
            results_list.append({"name": key, "votes": value})

        results_list_sorted = sorted(
            results_list,
            key=itemgetter('votes'),
            reverse=True)

        for result in results_list_sorted:
            results_list_str.append("\"%s\" - %s" %
                                    (unescape(result["name"]), result["votes"]))

        results_str = " // ".join(results_list_str)

        title = unescape(poll_data["title"])

        return "%s > %s // %s // strawpoll.me/%s/r" % (
            user_display_name, title, results_str, poll_id)


    def get_poll_post(self, user, channel, message=None):
        display_name = None

        user_data = ndf.channel_exists(user, "id")
        channel_data = ndf.channel_exists(channel, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        user_display_name = str(user_data["users"][0]["display_name"])
        display_name = str(channel_data["users"][0]["display_name"])

        if message is not None:
            message = parse.unquote(message)

        if message is None or "|" not in message:
            return "You didn't provide poll data or it's in the wrong format! Please try again. Usage - !newpoll <poll title> | <option 1> | <option 2> etc."

        try:
            poll_data = npi.post_new_poll(message)
        except:
            return "%s > Failed to create Straw Poll! Please try again." % user_display_name

        poll_id = poll_data["id"]

        try:
            with open("./database/" + channel + "_currentpoll.txt", "w") as f:
                f.write(str(poll_id))
        except:
            return None

        poll_title = unescape(poll_data["title"])

        return "%s > Straw Poll // %s // Created successfully! // strawpoll.me/%s" % (
            user_display_name, poll_title, poll_id)


    def clear_current_poll(self, user, channel):
        display_name = None

        user_data = ndf.channel_exists(user, "id")
        channel_data = ndf.channel_exists(channel, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        user_display_name = str(user_data["users"][0]["display_name"])
        display_name = str(channel_data["users"][0]["display_name"])

        try:
            with open("./database/" + channel + "_currentpoll.txt", "w") as f:
                f.write(str(""))
        except:
            return None

        return "%s > Current Straw Poll was cleared!" % user_display_name


    def get_svnotes(self, user, channel, message=None):
        display_name = None

        user_data = ndf.channel_exists(user, "id")
        channel_data = ndf.channel_exists(channel, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        user_display_name = str(user_data["users"][0]["display_name"])
        display_name = str(channel_data["users"][0]["display_name"])

        file = channel + "_svnotes.json"
        json_data = ndi.get_json_data(file)
        if json_data is None:
            return "There is no notes in the database for the channel \"%s\"! Please contact @AuroraExLux on Twitter for more details!" % channel

        if message is None:
            return "You didn't provide or it's in the wrong format! Please try again."
        elif message.strip().lower() == "all":
            return "%s > All notes can be viewed here http://twi.exlux.co/sv/%s" % (
                user_display_name, channel)
        elif " " not in message:
            return "You didn't provide or it's in the wrong format! Please try again."

        message = message.lower()
        notes_list = message.split(" ")
        tone = notes_list[1].lower()
        tone_list = ["loves", "likes", "dislikes", "hates", "misc"]
        charname = notes_list[0]

        if charname not in json_data["charnames"]:
            return "%s > The character \"%s\" is not in the database! Please add them first!" % (
                user_display_name, charname)

        if tone not in tone_list:
            return "%s > The category \"%s\" is not recognized!" % (
                user_display_name, tone)

        charname_formatted = str(ndf.ucwords(charname))

        if tone == "loves":
            if not json_data[charname]["loves"]:
                return "%s > %s does not love anything! Add something they love!" % (
                    user_display_name, charname_formatted)
            else:
                loves_data = sorted(json_data[charname]["loves"])
                for i, love in enumerate(loves_data):
                    loves_data[i] = str(ndf.ucwords(loves_data[i]))
                if len(loves_data) is 1:
                    loves_str = loves_data[0]
                elif len(loves_data) > 1:
                    loves_str = "%s and %s" % (
                        ", ".join(loves_data[:-1]), loves_data[-1])
                return "%s > %s loves %s!" % (
                    user_display_name, charname_formatted, loves_str)
        elif tone == "likes":
            if not json_data[charname]["likes"]:
                return "%s > %s does not like anything! Add something they like!" % (
                    user_display_name, charname_formatted)
            else:
                likes_data = sorted(json_data[charname]["likes"])
                for i, like in enumerate(likes_data):
                    likes_data[i] = str(ndf.ucwords(likes_data[i]))
                if len(likes_data) is 1:
                    likes_str = likes_data[0]
                elif len(likes_data) > 1:
                    likes_str = "%s and %s" % (
                        ", ".join(likes_data[:-1]), likes_data[-1])
                return "%s > %s likes %s!" % (
                    user_display_name, charname_formatted, likes_str)
        elif tone == "dislikes":
            if not json_data[charname]["dislikes"]:
                return "%s > %s does not dislike anything! Add something they dislike!" % (
                    user_display_name, charname_formatted)
            else:
                dislikes_data = sorted(json_data[charname]["dislikes"])
                for i, dislike in enumerate(dislikes_data):
                    dislikes_data[i] = str(ndf.ucwords(dislikes_data[i]))
                if len(dislikes_data) is 1:
                    dislikes_str = dislikes_data[0]
                elif len(dislikes_data) > 1:
                    dislikes_str = "%s and %s" % (
                        ", ".join(dislikes_data[:-1]), dislikes_data[-1])
                return "%s > %s dislikes %s!" % (
                    user_display_name, charname_formatted, dislikes_str)
        elif tone == "hates":
            if not json_data[charname]["hates"]:
                return "%s > %s does not hate anything! Add something they hate!" % (
                    user_display_name, charname_formatted)
            else:
                hates_data = sorted(json_data[charname]["hates"])
                for i, hate in enumerate(hates_data):
                    hates_data[i] = str(ndf.ucwords(hates_data[i]))
                if len(hates_data) is 1:
                    hates_str = hates_data[0]
                elif len(hates_data) > 1:
                    hates_str = "%s and %s" % (
                        ", ".join(hates_data[:-1]), hates_data[-1])
                return "%s > %s hates %s!" % (
                    user_display_name, charname_formatted, hates_str)
        elif tone == "misc":
            if not json_data[charname]["misc"]:
                return "%s > No miscellaneous notes for %s! Add something they like!" % (
                    user_display_name, charname_formatted)
            else:
                misc_data = sorted(json_data[charname]["misc"])
                for i, misc in enumerate(misc_data):
                    misc_data[i] = str(ndf.ucwords(misc_data[i]))
                if len(misc_data) is 1:
                    misc_str = misc_data[0]
                elif len(misc_data) > 1:
                    misc_str = " // ".join(misc_data)
                return "%s > Miscellaneous notes for %s // %s" % (
                    user_display_name, charname_formatted, misc_str)
        else:
            return ""


    def put_svnotes(self, user, channel, message=None):
        display_name = None

        user_data = ndf.channel_exists(user, "id")
        channel_data = ndf.channel_exists(channel, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        user_display_name = str(user_data["users"][0]["display_name"])
        display_name = str(channel_data["users"][0]["display_name"])

        if message is None or " " not in message:
            return "You didn't provide notes data or it's in the wrong format! Please try again."

        file = channel + "_svnotes.json"
        json_data = ndi.get_json_data(file)
        if json_data is None:
            return "There is no notes in the database for the channel \"%s\"! Please contact @AuroraExLux on Twitter for more details!" % channel
        message = message.lower()
        notes_list = message.split(" ")
        action = notes_list[0]
        action_list = ["add", "delete"]
        charname = notes_list[1]
        tone = notes_list[2]
        tone_list = ["loves", "likes", "dislikes", "hates", "misc"]
        item = " ".join(notes_list[3:]).strip()

        if charname not in json_data["charnames"]:
            return "%s > The character \"%s\" is not in the database! Please add them first!" % (
                user_display_name, charname)

        if action not in action_list:
            return "%s > The action \"%s\" is not recognized!" % (
                user_display_name, action)

        if tone not in tone_list:
            return "%s > The category \"%s\" is not recognized!" % (
                user_display_name, tone)

        if item is None or item == "":
            return "%s > The subject item was not provided!" % user_display_name

        charname_formatted = str(ndf.ucwords(charname))
        item_formatted = str(ndf.ucwords(item))

        if action == "add":
            if tone == "loves":
                json_data[charname]["loves"].append(item)
                json_data[charname]["loves"] = list(
                    set(json_data[charname]["loves"]))
                output = "%s > %s now loves %s!" % (
                    user_display_name, charname_formatted, item_formatted)
            elif tone == "likes":
                json_data[charname]["likes"].append(item)
                json_data[charname]["likes"] = list(
                    set(json_data[charname]["likes"]))
                output = "%s > %s now likes %s!" % (
                    user_display_name, charname_formatted, item_formatted)
            elif tone == "dislikes":
                json_data[charname]["dislikes"].append(item)
                json_data[charname]["dislikes"] = list(
                    set(json_data[charname]["dislikes"]))
                output = "%s > %s now dislikes %s!" % (
                    user_display_name, charname_formatted, item_formatted)
            elif tone == "hates":
                json_data[charname]["hates"].append(item)
                json_data[charname]["hates"] = list(
                    set(json_data[charname]["hates"]))
                output = "%s > %s now hates %s!" % (
                    user_display_name, charname_formatted, item_formatted)
            elif tone == "misc":
                json_data[charname]["misc"].append(item)
                json_data[charname]["misc"] = list(
                    set(json_data[charname]["misc"]))
                output = "%s > Miscellaneous note for %s \"%s\" was added!" % (
                    user_display_name, charname_formatted, item_formatted)
        elif action == "delete":
            if tone == "loves":
                if item in json_data[charname]["loves"]:
                    json_data[charname]["loves"].remove(item)
                    output = "%s > %s no longer loves %s!" % (
                        user_display_name, charname_formatted, item_formatted)
                else:
                    output = "%s > %s didn't previously love %s!" % (
                        user_display_name, charname_formatted, item_formatted)
            elif tone == "likes":
                if item in json_data[charname]["likes"]:
                    json_data[charname]["likes"].remove(item)
                    output = "%s > %s no longer likes %s!" % (
                        user_display_name, charname_formatted, item_formatted)
                else:
                    output = "%s > %s didn't previously like %s!" % (
                        user_display_name, charname_formatted, item_formatted)
            elif tone == "dislikes":
                if item in json_data[charname]["dislikes"]:
                    json_data[charname]["dislikes"].remove(item)
                    output = "%s > %s no longer dislikes %s!" % (
                        user_display_name, charname_formatted, item_formatted)
                else:
                    output = "%s > %s didn't previously dislike %s!" % (
                        user_display_name, charname_formatted, item_formatted)
            elif tone == "hates":
                if item in json_data[charname]["hates"]:
                    json_data[charname]["hates"].remove(item)
                    output = "%s > %s no longer hates %s!" % (
                        user_display_name, charname_formatted, item_formatted)
                else:
                    output = "%s > %s didn't previously hate %s!" % (
                        user_display_name, charname_formatted, item_formatted)
            elif tone == "misc":
                if item in json_data[charname]["misc"]:
                    json_data[charname]["misc"].remove(item)
                    output = "%s > Miscellaneous note for %s \"%s\" was deleted!" % (
                        user_display_name, charname_formatted, item_formatted)
                else:
                    output = "%s > Miscellaneous note for %s \"%s\" didn't previously exist!" % (
                        user_display_name, charname_formatted, item_formatted)

        response = ndi.put_json_data(json_data, file)
        if response is None:
            return "%s > Failed to write to the database! Please try again shortly." % user_display_name
        else:
            return output


    def render_svnotes(self, channel):
        display_name = None
        html_table = ""

        channel_data = ndf.channel_exists(channel, "id")

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        display_name = str(channel_data["users"][0]["display_name"])

        file = channel + "_svnotes.json"
        json_data = ndi.get_json_data(file)
        if json_data is None:
            return "There is no notes in the database for the channel \"%s\"! Please contact @AuroraExLux on Twitter for more details!" % channel

        html_header = (
            "<html>"
            "    <head>"
            "        <meta charset=\"utf-8\">"
            "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">"
            "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
            "        <meta name=\"description\" content=\"\">"
            "        <meta name=\"author\" content=\"\">"
            "        <title>Stardew Valley Notes - %s</title>"
            "        <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" rel=\"stylesheet\">"
            "        <link href=\"../html/css/svnotes_custom.css\" rel=\"stylesheet\">"
            "        </head>"
            "    <body>"
            "        <div class=\"container-fluid\">"
            "            <div class=\"col-xs-12 table-responsive\">"
            "                <table class=\"table table-hover header-fixed\">"
            "                    <thead>"
            "                        <tr>"
            "                            <th class=\"name-col\">Name</th>"
            "                            <th class=\"content-col\">Loves</th>"
            "                            <th class=\"content-col\">Likes</th>"
            "                            <th class=\"content-col\">Dislikes</th>"
            "                            <th class=\"content-col\">Hates</th>"
            "                            <th class=\"content-col\">Miscellaneous</th>"
            "                        </tr>"
            "                    </thead>"
            "                    <tbody>" % display_name)

        for charname in sorted(json_data["charnames"]):
            charname_formatted = str(ndf.ucwords(charname))
            if json_data[charname]["loves"]:
                loves_data = sorted(json_data[charname]["loves"])
                for i, love in enumerate(loves_data):
                    loves_data[i] = str(ndf.ucwords(loves_data[i]))
                if len(loves_data) is 1:
                    loves_str = loves_data[0]
                elif len(loves_data) > 1:
                    loves_str = "<br>".join(loves_data)
            else:
                loves_str = ""
            if json_data[charname]["likes"]:
                likes_data = sorted(json_data[charname]["likes"])
                for i, like in enumerate(likes_data):
                    likes_data[i] = str(ndf.ucwords(likes_data[i]))
                if len(likes_data) is 1:
                    likes_str = likes_data[0]
                elif len(likes_data) > 1:
                    likes_str = "<br>".join(likes_data)
            else:
                likes_str = ""
            if json_data[charname]["dislikes"]:
                dislikes_data = sorted(json_data[charname]["dislikes"])
                for i, dislike in enumerate(dislikes_data):
                    dislikes_data[i] = str(ndf.ucwords(dislikes_data[i]))
                if len(dislikes_data) is 1:
                    dislikes_str = dislikes_data[0]
                elif len(dislikes_data) > 1:
                    dislikes_str = "<br>".join(dislikes_data)
            else:
                dislikes_str = ""
            if json_data[charname]["hates"]:
                hates_data = sorted(json_data[charname]["hates"])
                for i, hate in enumerate(hates_data):
                    hates_data[i] = str(ndf.ucwords(hates_data[i]))
                if len(hates_data) is 1:
                    hates_str = hates_data[0]
                elif len(hates_data) > 1:
                    hates_str = "<br>".join(hates_data)
            else:
                hates_str = ""
            if json_data[charname]["misc"]:
                misc_data = sorted(json_data[charname]["misc"])
                for i, misc in enumerate(misc_data):
                    misc_data[i] = str(ndf.ucwords(misc_data[i]))
                if len(misc_data) is 1:
                    misc_str = misc_data[0]
                elif len(misc_data) > 1:
                    misc_str = "<br>".join(misc_data)
            else:
                misc_str = ""
            html_table = html_table + (
                "        <tr>"
                "            <th>%s</th>"
                "            <td>%s</td>"
                "            <td>%s</td>"
                "            <td>%s</td>"
                "            <td>%s</td>"
                "            <td>%s</td>"
                "        </tr>"
                % (charname_formatted, loves_str, likes_str, dislikes_str, hates_str, misc_str))

        html_footer = (
            "                    </tbody>"
            "                </table>"
            "            </div>"
            "        </div>"
            "    </body>"
            "</html>"
        )

        return "%s%s%s" % (html_header, html_table, html_footer)


    def put_boss_info(self, user, channel, message=None):
        display_name = None
        active_boss_index = 0
        active_boss_name = ""
        last_def_boss = 0
        last_def_boss_name = ""
        def_boss = []
        undef_boss = []
        def_boss_names = []
        undef_boss_names = []
        boss_status = []

        user_data = ndf.channel_exists(user, "id")
        channel_data = ndf.channel_exists(channel, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        user_display_name = str(user_data["users"][0]["display_name"])
        display_name = str(channel_data["users"][0]["display_name"])

        boss_data = ndi.get_json_data("darksouls3_bossdata.json")

        file = channel + "_ds3_bossdata.json"
        json_data = ndi.get_json_data(file)
        if json_data is None:
            return "There is no data in the database for the channel \"%s\"! Please contact @AuroraExLux on Twitter for more details!" % channel

        for i, record in enumerate(json_data):
            if json_data[i]["defeated"] is True:
                last_def_boss = json_data[i]["index"]
                last_def_boss_name = boss_data[i]["name"]

        for i, record in enumerate(json_data):
            boss_status.append(json_data[i]["defeated"])
            if json_data[i]["defeated"] is True:
                def_boss.append(json_data[i]["index"])
                def_boss_names.append(boss_data[i]["name"])
            else:
                undef_boss.append(json_data[i]["index"])
                undef_boss_names.append(boss_data[i]["name"])
            if json_data[i]["active"] is True:
                active_boss_index = json_data[i]["index"]
                active_boss_name = boss_data[i]["name"]

        if message is None:
            return "You didn't specify an action! Please try again."

        message_list = message.split(" ")
        action_list = ["reset", "back", "skip", "defeat", "undo"]
        action = message_list[0].lower()
        if action == "skip":
            try:
                skip_count = int(message_list[1])
            except (IndexError, TypeError, ValueError):
                skip_count = 1

        if action not in action_list:
            return "%s > The action \"%s\" is not recognized!" % (
                user_display_name, action)

        if action == "reset":
            if active_boss_index < 18:
                json_data[active_boss_index]["active"] = False
                json_data[def_boss[-1] + 1]["active"] = True
                boss_name = boss_data[def_boss[-1] + 1]["name"]
                output = "%s > The streamers active boss was reset to %s!" % (
                    user_display_name, boss_name)
            else:
                output = "%s > Reset failed! Try using back instead." % user_display_name
        elif action == "back":
            json_data[active_boss_index]["active"] = False
            json_data[undef_boss[0]]["active"] = True
            boss_name = boss_data[undef_boss[0]]["name"]
            output = "%s > The streamers active boss was set back to %s!" % (
                user_display_name, boss_name)
        elif action == "skip":
            if active_boss_index < 18:
                json_data[active_boss_index]["active"] = False
                json_data[undef_boss[skip_count]]["active"] = True
                boss_name = boss_data[undef_boss[skip_count]]["name"]
                output = "%s > The streamers active boss was set to %s!" % (
                    user_display_name, boss_name)
            else:
                output = "%s > There are no more undefeated bosses after %s!" % (
                    user_display_name, active_boss_name)
        elif action == "defeat":
            json_data[active_boss_index]["defeated"] = True
            undef_boss.remove(active_boss_index)
            def_boss.append(active_boss_index)
            undef_boss_names.remove(active_boss_name)
            def_boss_names.append(active_boss_name)
            ndi.put_json_data({"index": active_boss_index,
                               "name": active_boss_name},
                              channel + "_ds3_lastboss.json")
            if active_boss_index < 18:
                json_data[active_boss_index]["active"] = False
                if active_boss_index + 1 in undef_boss:
                    json_data[active_boss_index + 1]["active"] = True
                else:
                    json_data[undef_boss[0]]["active"] = True
                output = "Congrats Streamer, you defeated %s! PogChamp" % active_boss_name
            elif active_boss_index == 18 and not undef_boss:
                output = "Congrats Streamer, you have defeated all bosses in Dark Souls III! PogChamp"
            elif active_boss_index == 18 and undef_boss:
                output = "Congrats Streamer, you defeated %s however you still haven't defeated %s!" % (
                    active_boss_name, ", ".join(undef_boss_names))
        elif action == "undo":
            lastboss_data = ndi.get_json_data(channel + "_ds3_lastboss.json")
            if lastboss_data is None:
                output = "%s > Failed to revert! Please try again after a boss defeat." % user_display_name
            if def_boss:
                json_data[active_boss_index]["active"] = False
                json_data[lastboss_data["index"]]["active"] = True
                json_data[lastboss_data["index"]]["defeated"] = False
                output = "%s > Defeat of %s was reverted!" % (
                    user_display_name, lastboss_data["name"])
            else:
                output = "%s > Streamer hasn't defeated any bosses yet!" % user_display_name

        response = ndi.put_json_data(json_data, file)
        if response is None:
            return "%s > Failed to write to the database! Please try again shortly." % user_display_name
        else:
            return output


    def get_boss_info(self, user, channel):
        display_name = None
        active_boss_name = ""
        def_boss_names = []
        undef_boss_names = []

        user_data = ndf.channel_exists(user, "id")
        channel_data = ndf.channel_exists(channel, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        user_display_name = str(user_data["users"][0]["display_name"])
        display_name = str(channel_data["users"][0]["display_name"])

        boss_data = ndi.get_json_data("darksouls3_bossdata.json")

        file = channel + "_ds3_bossdata.json"
        json_data = ndi.get_json_data(file)
        if json_data is None:
            return "There is no data in the database for the channel \"%s\"! Please contact @AuroraExLux on Twitter for more details!" % channel

        for i, record in enumerate(json_data):
            if json_data[i]["defeated"] is True:
                def_boss_names.append(boss_data[i]["name"])
            else:
                undef_boss_names.append(boss_data[i]["name"])
            if json_data[i]["active"] is True:
                active_boss_name = boss_data[i]["name"]

        if not undef_boss_names:
            return "%s > Streamer has defeated all bosses in Dark Souls III! %s! PogChamp" % (
                user_display_name, " // ".join(def_boss_names))
        elif not def_boss_names:
            return "%s > Streamer is currently up to %s!" % (
                user_display_name, active_boss_name)
        else:
            return "%s > Streamer is currently up to %s! They have defeated %s!" % (
                user_display_name, active_boss_name, " // ".join(def_boss_names))


    def put_system_info(self, channel, target=None):
        display_name = None

        channel_data = ndf.channel_exists(channel, "id")

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        display_name = str(channel_data["users"][0]["display_name"])

        if display_name is None:
            display_name = channel

        if target is None:
            return "You didn\'t specify a target system! Please try again!"
        else:
            target = target.lower().strip()

        if target in system_dicts["steam"]["inputs"]:
            system = system_dicts["steam"]["str"]

        elif target in system_dicts["origin"]["inputs"]:
            system = system_dicts["origin"]["str"]

        elif target in system_dicts["win10"]["inputs"]:
            system = system_dicts["win10"]["str"]

        elif target in system_dicts["pc"]["inputs"]:
            system = system_dicts["pc"]["str"]

        elif target in system_dicts["xb1"]["inputs"]:
            system = system_dicts["xb1"]["str"]

        elif target in system_dicts["ps3"]["inputs"]:
            system = system_dicts["ps3"]["str"]

        elif target in system_dicts["ps4"]["inputs"]:
            system = system_dicts["ps4"]["str"]

        elif target in system_dicts["wiiu"]["inputs"]:
            system = system_dicts["wiiu"]["str"]

        elif target in system_dicts["3ds"]["inputs"]:
            system = system_dicts["3ds"]["str"]

        elif target in system_dicts["switch"]["inputs"]:
            system = system_dicts["switch"]["str"]

        elif target in system_dicts["linux"]["inputs"]:
            system = system_dicts["linux"]["str"]

        else:
            return "Target system \"%s\" is not valid!" % target

        file = ndi.put_text_data(system, "%s_currentsystem.txt" % channel)
        if file is None:
            return "System data for %s cannot be found!" % display_name
        return "System successfully set to %s!" % system


    def get_system_info(self, nickname, channel):
        display_name = None
        stream_live = False

        nickname_active = nickname.lower() != "false"

        channel_data = ndf.channel_exists(channel, "data")

        if channel_data is None:
            return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % channel

        stream_data = nti.get_stream_data(channel)[0]

        if nickname_active is False:
            display_name = str(channel_data["display_name"])
        else:
            display_name = str(ndf.ucwords(nickname))

        if stream_data["stream"] is None:
            return "%s is not currently streaming!" % display_name
        else:
            current_game = str(stream_data["stream"]["channel"]["game"])
            stream_live = True

        if display_name is not None and stream_live is True:
            file = ndi.get_text_data("%s_currentsystem.txt" % channel)
            if file is None:
                return "System data for %s cannot be found!" % display_name
            elif current_game == "Creative" and file == "Ubuntu Linux 16.04 LTS":
                return "%s is currently coding under %s on %s!" % (
                    display_name, current_game, file)
            elif current_game == "Creative":
                return "%s is currently being %s on %s!" % (
                    display_name, current_game, file)
            elif current_game == "Talk Shows":
                return "%s is currently hosting %s on %s!" % (
                    display_name, current_game, file)
            elif current_game == "Music":
                return "%s is currently making %s on %s!" % (
                    display_name, current_game, file)
            else:
                return "%s is currently playing %s on %s!" % (
                    display_name, current_game, file)
        else:
            return ""


    def get_steamplaytime_string(self, host, target, query=None):
        steam_display_name = None

        steam_profile_data = nei.get_steamuser_profile_data(target)

        if steam_profile_data is None:
            return "Sorry, the Steam profile \"%s\" was not found or it's set to private! Please try again." % target

        steam_display_name = steam_profile_data[
            "response"]["players"][0]["personaname"]

        if query is None:
            host_channel_data = ndf.channel_exists(host, "data")

            if host_channel_data is None:
                return "Host variable is invalid. Please contact @AuroraExLux on Twitter for command configuration."
            else:
                host_game = host_channel_data["game"]
                twitch_games_exclusions = [
                    "Creative",
                    "Music",
                    "IRL",
                    "Casino",
                    "Poker",
                    "Talk Shows",
                    "Social Eating",
                    "Twitch Plays",
                    "Games + Demos"]

                if host_game not in twitch_games_exclusions:
                    query = parse.unquote(host_channel_data["game"])
                else:
                    return "You didn't provide a Steam game to search for! Please try again. Usage - !steamhours <search query>"

        try:
            steam_game_search = nsi.get_steamstore_result(query)[0]
        except IndexError:
            return "There are no steam games for the query \"%s\". Please try again!" % parse.unquote(
                query)

        steam_usergame_data = nei.get_usergame_data(
            steam_game_search["game_appid"], target)

        if steam_usergame_data["response"]["game_count"] == 0:
            return "%s does not own %s! FeelsBadMan" % (
                steam_display_name, steam_game_search["game_name"])
        elif steam_usergame_data["response"]["game_count"] != 1:
            return "There are no steam games for the query \"%s\". Please try again!" % parse.unquote(
                query)

        total_minutes_played = steam_usergame_data[
            "response"]["games"][0]["playtime_forever"]

        if total_minutes_played < 60:
            total_minutes_played = "%s minutes on record" % total_minutes_played
        else:
            total_hours = total_minutes_played / 60

            if (total_hours < 20):
                total_hours = "{0:.1f}".format(total_hours)
            else:
                total_hours = int(total_hours)

            total_minutes_played = "%s hours on record" % total_hours

        return "%s has clocked %s on %s!" % (
            steam_display_name, total_minutes_played, steam_game_search["game_name"])


    def get_seppuku_string(self, host, botname, user):
        user_display_name = None

        if host and user and botname:
            user_channel_data = ndf.channel_exists(user, "id")

            if user_channel_data is None:
                return "Sorry, the Twitch channel \"%s\" was not found! Please try again." % user

            user_display_name = str(
                user_channel_data["users"][0]["display_name"])
            viewer_list = nti.get_viewer_list(host)[0]
            moderators_list = viewer_list["chatters"]["moderators"]

        if user_display_name is not None:
            if user.lower() == host.lower():
                return "Nice try %s, but you can\'t seppuku in your own chat!" % user_display_name
            elif user.lower() == botname.lower():
                return "I am not allowed this honour!"
            elif user.lower() in moderators_list:
                return "Sorry, %s you are not allowed this honour!" % user_display_name
            else:
                return "/timeout %s 1" % user
        return ""


    def get_eightball_string(self, user, question=None):
        display_name = None

        user_data = ndf.channel_exists(user, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        display_name = str(user_data["users"][0]["display_name"])

        if display_name is None:
            return ""  # Something wen't wrong, die and return nothing

        if question is None:
            return "You didn\'t ask 8-ball a question! Please try again. Usage - !8ball <question>"
        else:
            positive_responses = [
                "It is certain!",
                "It is decidedly so.",
                "Without a doubt.",
                "Yes, definitely!",
                "You may rely on it.",
                "As I see it, yes!",
                "Most likely!",
                "Outlook good!",
                "Yes.",
                "Signs point to yes.",
                # Custom responses
                "100% positively, yes!",
                "The stars align today! Yes!",
                "If I say yes, will you go out with me?",
                "Yes, in due time.",
                "YAAAAS!",
                "If you are one with the force, then it is so.",
            ]
            neutral_responses = [
                "Reply hazy try again.",
                "Ask again later.",
                "Better not tell you now.",
                "Cannot predict now.",
                "Concentrate and ask again.",
                # Custom responses
                "Who knows?",
                "I couldn\'t be more uncertain!",
                "That\'s a matter of perspective.",
                "I\'ll leave that one up to you.",
                "I\'m too old for this shit!",
                "I haven't the foggiest idea!",
            ]
            negative_responses = [
                "Don\'t count on it.",
                "My reply is no.",
                "My sources say no.",
                "Outlook not so good.",
                "Very doubtful.",
                # Custom responses
                "No, Non, Nein, Bú shì!",
                "The stars do not align today. Bad luck!",
                "Luck is not on your side, unfortunate but true!",
                "I\'m sorry but I have to say... hell no!",
                "Never!",
                "I could tell you... but then I\'d have to kill you!",
                "I hate to let you down here but... nope!",
            ]

        tone_selector = randint(0, 2)
        if tone_selector is 0:
            response_list = positive_responses
        elif tone_selector is 1:
            response_list = neutral_responses
        elif tone_selector is 2:
            response_list = negative_responses
        else:
            response_list = positive_responses

        response_str = str(choice(response_list))
        question = question.strip()
        question_list = question.split(" ")
        if question_list:
            question_list[0] = ndf.ucwords(question_list[0])
            question = " ".join(question_list)

        if "%3F" not in question or "?" not in question:
            question = "%s?" % question

        return "%s asks \"%s\" ... Magic 8-ball says \"%s\"" % (
            display_name, question, response_str)


    def get_gamble_string(self, user, points=None):
        display_name = None

        user_data = ndf.channel_exists(user, "id")

        if user_data is None:
            return "Sorry, the Twitch username \"%s\" was not found! Please try again." % user

        display_name = str(user_data["users"][0]["display_name"])

        if points is None:
            return "You didn\'t define how much $currencyname you wish to gamble! Please try again. Usage - !gamble <amount>"
        else:
            extracted_points = points.strip().split(" ")[0]

        if extracted_points.isdigit() is False:
            return "Your $currencyname bet is not a valid number! Please try again. Usage - !gamble <amount>"
        else:
            points = extracted_points

        points = int(points)

        lose_roll = randint(0, 59)
        win_roll = randint(60, 99)
        jackpot_roll = randint(95, 99)

        low_bet_rand = randint(0, 1)
        high_bet_rand = randint(0, 5)
        jackpot_rand = randint(0, 19)

        if jackpot_rand == 0:
            gamble_rand = jackpot_roll
        elif points > 49999 and high_bet_rand == 0:
            gamble_rand = win_roll
        elif points < 50000 and low_bet_rand == 0:
            gamble_rand = win_roll
        else:
            gamble_rand = lose_roll

        points_awarded = points * 2
        points_bonus = points * 3
        points_bonus_awarded = points * 4

        lose_str = "$removepoints(\"{0}\",\"{1}\",\"{1}\",\"You lost {1} $currencyname you now have $newbalance({0}) $currencyname.\",\"You don't have {1} $currencyname to gamble with!\",\"false\")".format(
            display_name, points)
        no_points_str = "$removepoints(\"{0}\",\"{1}\",\"{1}\",\"\",\"You don't have {1} $currencyname to gamble with!\",\"false\")".format(
            display_name, points)
        win_str = "$addpoints(\"{0}\",\"{2}\",\"{2}\",\"You got your {1} bet back and won {1} $currencyname and now have $newbalance({0}) $currencyname.\",\"Failed to give {0} $currencyname\")".format(
            display_name, points, points_awarded)
        bonus_win_str = "$addpoints(\"{0}\",\"{3}\",\"{3}\",\"YOU HIT THE JACKPOT! You got your {1} bet back and won {2} $currencyname and now has $newbalance({0}) $currencyname.\",\"Failed to give {0} $currencyname\")".format(
            display_name, points, points_bonus, points_bonus_awarded)

        gamble_array = [
            "Rolled 1. %s" % lose_str,
            "Rolled 2. %s" % lose_str,
            "Rolled 3. %s" % lose_str,
            "Rolled 4. %s" % lose_str,
            "Rolled 5. %s" % lose_str,
            "Rolled 6. %s" % lose_str,
            "Rolled 7. %s" % lose_str,
            "Rolled 8. %s" % lose_str,
            "Rolled 9. %s" % lose_str,
            "Rolled 10. %s" % lose_str,
            "Rolled 11. %s" % lose_str,
            "Rolled 12. %s" % lose_str,
            "Rolled 13. %s" % lose_str,
            "Rolled 14. %s" % lose_str,
            "Rolled 15. %s" % lose_str,
            "Rolled 16. %s" % lose_str,
            "Rolled 17. %s" % lose_str,
            "Rolled 18. %s" % lose_str,
            "Rolled 19. %s" % lose_str,
            "Rolled 20. %s" % lose_str,
            "Rolled 21. %s" % lose_str,
            "Rolled 22. %s" % lose_str,
            "Rolled 23. %s" % lose_str,
            "Rolled 24. %s" % lose_str,
            "Rolled 25. %s" % lose_str,
            "Rolled 26. %s" % lose_str,
            "Rolled 27. %s" % lose_str,
            "Rolled 28. %s" % lose_str,
            "Rolled 29. %s" % lose_str,
            "Rolled 30. %s" % lose_str,
            "Rolled 31. %s" % lose_str,
            "Rolled 32. %s" % lose_str,
            "Rolled 33. %s" % lose_str,
            "Rolled 34. %s" % lose_str,
            "Rolled 35. %s" % lose_str,
            "Rolled 36. %s" % lose_str,
            "Rolled 37. %s" % lose_str,
            "Rolled 38. %s" % lose_str,
            "Rolled 39. %s" % lose_str,
            "Rolled 40. %s" % lose_str,
            "Rolled 41. %s" % lose_str,
            "Rolled 42. %s" % lose_str,
            "Rolled 43. %s" % lose_str,
            "Rolled 44. %s" % lose_str,
            "Rolled 45. %s" % lose_str,
            "Rolled 46. %s" % lose_str,
            "Rolled 47. %s" % lose_str,
            "Rolled 48. %s" % lose_str,
            "Rolled 49. %s" % lose_str,
            "Rolled 50. %s" % lose_str,
            "Rolled 51. %s" % lose_str,
            "Rolled 52. %s" % lose_str,
            "Rolled 53. %s" % lose_str,
            "Rolled 54. %s" % lose_str,
            "Rolled 55. %s" % lose_str,
            "Rolled 56. %s" % lose_str,
            "Rolled 57. %s" % lose_str,
            "Rolled 58. %s" % lose_str,
            "Rolled 59. %s" % lose_str,
            "Rolled 60. %s" % lose_str,
            "Rolled 61. %s %s" % (no_points_str, win_str),
            "Rolled 62. %s %s" % (no_points_str, win_str),
            "Rolled 63. %s %s" % (no_points_str, win_str),
            "Rolled 64. %s %s" % (no_points_str, win_str),
            "Rolled 65. %s %s" % (no_points_str, win_str),
            "Rolled 66. %s %s" % (no_points_str, win_str),
            "Rolled 67. %s %s" % (no_points_str, win_str),
            "Rolled 68. %s %s" % (no_points_str, win_str),
            "Rolled 69. %s %s" % (no_points_str, win_str),
            "Rolled 70. %s %s" % (no_points_str, win_str),
            "Rolled 71. %s %s" % (no_points_str, win_str),
            "Rolled 72. %s %s" % (no_points_str, win_str),
            "Rolled 73. %s %s" % (no_points_str, win_str),
            "Rolled 74. %s %s" % (no_points_str, win_str),
            "Rolled 75. %s %s" % (no_points_str, win_str),
            "Rolled 76. %s %s" % (no_points_str, win_str),
            "Rolled 77. %s %s" % (no_points_str, win_str),
            "Rolled 78. %s %s" % (no_points_str, win_str),
            "Rolled 79. %s %s" % (no_points_str, win_str),
            "Rolled 80. %s %s" % (no_points_str, win_str),
            "Rolled 81. %s %s" % (no_points_str, win_str),
            "Rolled 82. %s %s" % (no_points_str, win_str),
            "Rolled 83. %s %s" % (no_points_str, win_str),
            "Rolled 84. %s %s" % (no_points_str, win_str),
            "Rolled 85. %s %s" % (no_points_str, win_str),
            "Rolled 86. %s %s" % (no_points_str, win_str),
            "Rolled 87. %s %s" % (no_points_str, win_str),
            "Rolled 88. %s %s" % (no_points_str, win_str),
            "Rolled 89. %s %s" % (no_points_str, win_str),
            "Rolled 90. %s %s" % (no_points_str, win_str),
            "Rolled 91. %s %s" % (no_points_str, win_str),
            "Rolled 92. %s %s" % (no_points_str, win_str),
            "Rolled 93. %s %s" % (no_points_str, win_str),
            "Rolled 94. %s %s" % (no_points_str, win_str),
            "Rolled 95. %s %s" % (no_points_str, win_str),
            "Rolled 96. %s %s" % (no_points_str, bonus_win_str),
            "Rolled 97. %s %s" % (no_points_str, bonus_win_str),
            "Rolled 98. %s %s" % (no_points_str, bonus_win_str),
            "Rolled 99. %s %s" % (no_points_str, bonus_win_str),
            "Rolled 100. %s %s" % (no_points_str, bonus_win_str)
        ]

        if display_name is not None:
            return "%s > %s" % (display_name, gamble_array[gamble_rand])
