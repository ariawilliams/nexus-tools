# from classes.core.NexusCore import NexusCore
from urllib import parse
from flask import Flask
from flask import request
from flask import redirect
from flask import send_from_directory
from werkzeug.contrib.fixers import ProxyFix
import os
import json


# ncr = NexusCore()

app = Flask(__name__)


@app.errorhandler(404)
def page_not_found(e):
    print(e)
    return ""


@app.errorhandler(500)
def internal_server_error(e):
    print(e)
    return "I'm terribly sorry, something went wrong in my brain! Please try again."



# Serve Favicon
@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/html/<path:path>')
def send_html_files(path):
    return send_from_directory('html', path)


@app.route("/<code>")
def main(code):
    url = nur.get_target_url(code)
    return redirect(url)


app.wsgi_app = ProxyFix(app.wsgi_app)

if __name__ == "__main__":
    app.run(debug = False)
